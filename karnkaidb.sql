/*
 Navicat Premium Data Transfer

 Source Server         : Karnkai
 Source Server Type    : MariaDB
 Source Server Version : 100334
 Source Host           : 66.29.152.64:3306
 Source Schema         : karnkaidb

 Target Server Type    : MariaDB
 Target Server Version : 100334
 File Encoding         : 65001

 Date: 19/03/2022 19:42:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb2021
-- ----------------------------
DROP TABLE IF EXISTS `tb2021`;
CREATE TABLE `tb2021`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tb2022
-- ----------------------------
DROP TABLE IF EXISTS `tb2022`;
CREATE TABLE `tb2022`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tbadverse
-- ----------------------------
DROP TABLE IF EXISTS `tbadverse`;
CREATE TABLE `tbadverse`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbadverse
-- ----------------------------
INSERT INTO `tbadverse` VALUES (11, '1647691491624Banner-1.jpg', 'ທຳມະດາ', 1);
INSERT INTO `tbadverse` VALUES (12, '1647691717599Banner-2.jpg', 'ທຳມະດາ', 1);
INSERT INTO `tbadverse` VALUES (13, '1647691758279Banner-3.jpg', 'ຖ້າອົດທົນໄດ້ ກໍໃຫ້ອົດທົນໄປກ່ອນ. ', 1);
INSERT INTO `tbadverse` VALUES (14, '1647691837499Banner-4.jpg', 'ເປົ້າໝາຍຊັດເຈນ', 1);
INSERT INTO `tbadverse` VALUES (15, '1647691890419Banner-5.jpg', 'ເຮົາຕ່າງກໍເປັນແບບຢ່າງໃຫ້ແກ່ກັນ', 1);
INSERT INTO `tbadverse` VALUES (16, '1647691920789Banner-6.jpg', 'ແນມໃຫ້ເຫັນຄວາມເປັນໄປ', 1);
INSERT INTO `tbadverse` VALUES (17, '1647691938115Banner-7.jpg', 'ສຸດໃຈ', 1);
INSERT INTO `tbadverse` VALUES (18, '1647691962109Banner-8.jpg', 'ໃຫ້ຜົນງານນິຍາມໂຕເຮົາ', 1);
INSERT INTO `tbadverse` VALUES (19, '1647691996245Banner-9.jpg', 'ຄິດວ່າດີ ກໍເຮັດໄປ', 1);
INSERT INTO `tbadverse` VALUES (20, '1647692033123Banner-10.jpg', 'ໃຫ້ຮັກ ນຳທາງ', 1);
INSERT INTO `tbadverse` VALUES (21, '1647692058996Banner-11.jpg', 'ບໍ່ລີມວ່າ ເຮົາແມ່ນໃຜ', 1);
INSERT INTO `tbadverse` VALUES (22, '1647692109190Banner-12.jpg', 'ສິ່ງ ຄວນ ເຮັດ', 1);

-- ----------------------------
-- Table structure for tbbag
-- ----------------------------
DROP TABLE IF EXISTS `tbbag`;
CREATE TABLE `tbbag`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbbag
-- ----------------------------
INSERT INTO `tbbag` VALUES (8, '1647244997471Product-all.jpg', 'my bag ', 1);
INSERT INTO `tbbag` VALUES (9, '1647245941546262925-1600x1030-which-brands-designer-handbags-are-most-affordable.jpg', '90,000 LAK', 1);

-- ----------------------------
-- Table structure for tbcalendar
-- ----------------------------
DROP TABLE IF EXISTS `tbcalendar`;
CREATE TABLE `tbcalendar`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbcalendar
-- ----------------------------
INSERT INTO `tbcalendar` VALUES (5, '1647143719626BDB6BA6A-DC42-456A-9A92-7B238611F7F7.jpeg', 'Calendar design 1', 1);
INSERT INTO `tbcalendar` VALUES (6, '1647143814509DF93CD60-2FEA-4F50-AA67-60D6B1D3B870.jpeg', 'Calendar design 2', 1);
INSERT INTO `tbcalendar` VALUES (7, '16471438422385EE83181-8DA5-4EF1-800B-8AE5FF63F4A8.jpeg', 'Calendar design 3', 1);

-- ----------------------------
-- Table structure for tbcanvas
-- ----------------------------
DROP TABLE IF EXISTS `tbcanvas`;
CREATE TABLE `tbcanvas`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbcanvas
-- ----------------------------
INSERT INTO `tbcanvas` VALUES (7, '1647246497592jpeg-home.jpg', '700,000 LAK', 1);
INSERT INTO `tbcanvas` VALUES (8, '1647246515676canvas.jpg', '50,000 LAK', 1);
INSERT INTO `tbcanvas` VALUES (9, '1647246537525logo_design_elements_58954.jpg', '65,000 LAK', 1);

-- ----------------------------
-- Table structure for tbdownload
-- ----------------------------
DROP TABLE IF EXISTS `tbdownload`;
CREATE TABLE `tbdownload`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbdownload
-- ----------------------------
INSERT INTO `tbdownload` VALUES (22, '1647243293222Product-all.jpg', 'test', 1);
INSERT INTO `tbdownload` VALUES (23, '1647576465715maxresdefault (1).jpg', 'dsadasda', 1);

-- ----------------------------
-- Table structure for tbevent
-- ----------------------------
DROP TABLE IF EXISTS `tbevent`;
CREATE TABLE `tbevent`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT 1,
  `datetime` datetime(0) NULL DEFAULT current_timestamp,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbevent
-- ----------------------------
INSERT INTO `tbevent` VALUES (4, '1647421179838photo-1508098682722-e99c43a406b2.jpg', 'this event 2022', 1, '2022-03-16 08:59:39');
INSERT INTO `tbevent` VALUES (5, '1647423646177PicsArt_08-15-03.06.14_compress79.jpg', 'animation event 2022', 1, '2022-03-16 09:40:46');

-- ----------------------------
-- Table structure for tbeventprofile
-- ----------------------------
DROP TABLE IF EXISTS `tbeventprofile`;
CREATE TABLE `tbeventprofile`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `datetime` datetime(0) NULL DEFAULT current_timestamp,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbeventprofile
-- ----------------------------
INSERT INTO `tbeventprofile` VALUES (18, '1647340723381download (3).jpg', '2022-03-15 10:38:44');
INSERT INTO `tbeventprofile` VALUES (32, '1647418859103website.jpg', '2022-03-16 08:20:59');
INSERT INTO `tbeventprofile` VALUES (33, '1647418945598website.jpg', '2022-03-16 08:22:25');
INSERT INTO `tbeventprofile` VALUES (34, '1647422720318website.jpg', '2022-03-16 09:25:20');
INSERT INTO `tbeventprofile` VALUES (35, '1647423538005Moon.jpg', '2022-03-16 09:38:58');
INSERT INTO `tbeventprofile` VALUES (36, '1647423601118website.jpg', '2022-03-16 09:40:01');
INSERT INTO `tbeventprofile` VALUES (37, '1647423759885Moon.jpg', '2022-03-16 09:42:39');
INSERT INTO `tbeventprofile` VALUES (38, '1647423795931website.jpg', '2022-03-16 09:43:15');
INSERT INTO `tbeventprofile` VALUES (39, '1647506210067C48553A8AEC6AD28F0F6FC88188_C0517F26_17E29.jpg', '2022-03-17 08:36:50');

-- ----------------------------
-- Table structure for tblogotypedesign
-- ----------------------------
DROP TABLE IF EXISTS `tblogotypedesign`;
CREATE TABLE `tblogotypedesign`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tblogotypedesign
-- ----------------------------
INSERT INTO `tblogotypedesign` VALUES (5, '1647311887675minion.jpg', 'test', 1);

-- ----------------------------
-- Table structure for tblogs
-- ----------------------------
DROP TABLE IF EXISTS `tblogs`;
CREATE TABLE `tblogs`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `requestId` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `datetime` timestamp(0) NULL DEFAULT NULL,
  `method` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `serviceName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `message` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5772 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tblogs
-- ----------------------------
INSERT INTO `tblogs` VALUES (5770, 'maxky', '20220316133842', '2022-03-16 13:38:42', 'POST', '/karnkai/api/service/auth/login', '200', 'success', 'Login successfully', NULL);
INSERT INTO `tblogs` VALUES (5771, '589', '20220316134022', '2022-03-16 13:40:22', 'POST', '/karnkai/api/service/auth/register', '200', 'success', 'Register successfully', NULL);

-- ----------------------------
-- Table structure for tbpackage
-- ----------------------------
DROP TABLE IF EXISTS `tbpackage`;
CREATE TABLE `tbpackage`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `customerName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `customerEmail` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `customerTel` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `customerText` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `datetime` datetime(0) NULL DEFAULT current_timestamp,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbpackage
-- ----------------------------
INSERT INTO `tbpackage` VALUES (14, 'package: all product', 'NULL', 'test@gmail.com', 'NULL', '88f85fFE 1 ອັນ', 'waiting', '2022-03-18 04:18:44');
INSERT INTO `tbpackage` VALUES (15, 'package: 200,000, ຊື່ເຈົ້າບ່າວ: boy, ຊື່ເຈົ້າສາວ: girl ', 'test', 'test@gmail.com', '+58620 99885566', 'my wedding', 'accept', '2022-03-18 04:21:05');
INSERT INTO `tbpackage` VALUES (16, 'package: calendar', 'NULL', 'testcalendar@gmail.com', 'NULL', 'Calendar design 3\n\n', 'waiting', '2022-03-18 04:23:08');

-- ----------------------------
-- Table structure for tbpackageLogotypeDesign
-- ----------------------------
DROP TABLE IF EXISTS `tbpackageLogotypeDesign`;
CREATE TABLE `tbpackageLogotypeDesign`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `customerName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `customerEmail` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `customerTel` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `customerText` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbpackageLogotypeDesign
-- ----------------------------
INSERT INTO `tbpackageLogotypeDesign` VALUES (1, 'package: 200,000, ຊື່ເຈົ້າບ່າວ: jj, ຊື່ເຈົ້າສາວ: jj ', 'jj', 'jj@gmail.com', 'jj', 'jj', 'waiting');
INSERT INTO `tbpackageLogotypeDesign` VALUES (2, 'package: 800,000, ຊື່ເຈົ້າບ່າວ: Boy, ຊື່ເຈົ້າສາວ: Girl', 'Test', 'Test@gmail.com', '020 55663322', 'Test Title', 'waiting');
INSERT INTO `tbpackageLogotypeDesign` VALUES (3, 'package: 800,000, ຊື່ເຈົ້າບ່າວ: Boy, ຊື່ເຈົ້າສາວ: Girl', 'Test', 'Test@gmail.com', '020 55663322', 'Test Title', 'waiting');
INSERT INTO `tbpackageLogotypeDesign` VALUES (4, 'package: 800,000, ຊື່ເຈົ້າບ່າວ: boy, ຊື່ເຈົ້າສາວ: girl ', 'test', 'test@gmail.com', '020 88996655', 'my wedding', 'waiting');

-- ----------------------------
-- Table structure for tbphotobook
-- ----------------------------
DROP TABLE IF EXISTS `tbphotobook`;
CREATE TABLE `tbphotobook`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbphotobook
-- ----------------------------
INSERT INTO `tbphotobook` VALUES (5, '1647246959032og-design.jpg', 'design 1', 1);
INSERT INTO `tbphotobook` VALUES (6, '1647246984786book-cover-art-8d4c18ab49d692ec43edef04379bedbab7f13fc38efe488fb8841db28be3c93f.jpg', 'design 2', 1);

-- ----------------------------
-- Table structure for tbpreweddingprofile
-- ----------------------------
DROP TABLE IF EXISTS `tbpreweddingprofile`;
CREATE TABLE `tbpreweddingprofile`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `datetime` datetime(0) NULL DEFAULT current_timestamp,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbpreweddingprofile
-- ----------------------------
INSERT INTO `tbpreweddingprofile` VALUES (1, '16475716127121-designer-fall-bags-index-1604689902.jpg', 'Insert photo photobook', '2022-03-18 02:46:53');
INSERT INTO `tbpreweddingprofile` VALUES (2, '1647573252419Pre-Wedding-3526e-scaled.jpg', 'dsdadsadsadddsd', '2022-03-18 03:14:12');

-- ----------------------------
-- Table structure for tbproduct
-- ----------------------------
DROP TABLE IF EXISTS `tbproduct`;
CREATE TABLE `tbproduct`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `productName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `productPrice` int(11) NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT 1,
  `datetime` datetime(0) NULL DEFAULT current_timestamp,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbproduct
-- ----------------------------
INSERT INTO `tbproduct` VALUES (3, '16474994361021-designer-fall-bags-index-1604689902.jpg', 'Insert photo photobook', '626DG564 NIKE', 50000, 0, '2022-03-17 06:43:56');
INSERT INTO `tbproduct` VALUES (6, '1647500031757canvas.jpg', 'Insert photo photobook', '626DG564 NIKE', 50000, 1, '2022-03-17 06:53:51');
INSERT INTO `tbproduct` VALUES (7, '1647500044067canvas.jpg', 'Insert photo photobook', '626DG564 NIKE', 50000, 1, '2022-03-17 06:54:04');

-- ----------------------------
-- Table structure for tbworkshop
-- ----------------------------
DROP TABLE IF EXISTS `tbworkshop`;
CREATE TABLE `tbworkshop`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbworkshop
-- ----------------------------
INSERT INTO `tbworkshop` VALUES (5, '1647312394564C48553A8AEC6AD28F0F6FC88188_C0517F26_17E29.jpg', 'test', 1);
INSERT INTO `tbworkshop` VALUES (6, '1647312413627offset_comp_772626-opt.jpg', 'test photo', 1);

-- ----------------------------
-- Table structure for tbworkshopprofile
-- ----------------------------
DROP TABLE IF EXISTS `tbworkshopprofile`;
CREATE TABLE `tbworkshopprofile`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `datetime` datetime(0) NULL DEFAULT current_timestamp,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbworkshopprofile
-- ----------------------------
INSERT INTO `tbworkshopprofile` VALUES (1, '1647499277629Calendar-design-in-Photoshop-scaled-1.jpg', 'Insert photo workshop', '2022-03-17 06:41:17');
INSERT INTO `tbworkshopprofile` VALUES (2, '1647506313007exproler2021.jpg', 'fscfewrw ້ເກດ້້ດຫເ້', '2022-03-17 08:38:33');
INSERT INTO `tbworkshopprofile` VALUES (3, '16475733490950_fb5BzRT8qkzqpmYb.jpg', 'sddadasd', '2022-03-18 03:15:49');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `role` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'admin',
  `date` timestamp(0) NOT NULL DEFAULT current_timestamp,
  `status` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 706 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (598, 'maxky@gmail.com', 'maxky', '$2b$10$noPUM94TOcyHT3UIDDI.we/6b6J3ROkuvFDu0MaV/TfhfGcdkOLCe', 'admin', '2021-03-09 08:02:16', 'active');
INSERT INTO `users` VALUES (665, 'test@gmail.com', 'test', '$2b$10$glWT43p6kJaGJP6WVj42iO.t7dZx2BvpkXfAMj6VgzRkhUb02QKuC', 'general', '2022-03-03 07:47:24', 'active');
INSERT INTO `users` VALUES (705, 'test@gmail.com', 'test', '$2b$10$2FioduTr0vt7JuA7G54S8OiqabYgP9I2ed/fIQn4S1K0AscgWtBJ6', 'general', '2022-03-16 06:40:23', 'active');
INSERT INTO `users` VALUES (704, 'kaka@gmail.com', 'kaka', '$2b$10$WjhSWd0nVgsWo4j8T/mUmeD87S99tLuvxklOJX3MXMuvL6I5Tt.TG', 'admin', '2022-03-12 13:16:16', 'active');
INSERT INTO `users` VALUES (703, 'xxx@gmail.com', 'xxx', '$2b$10$WjhSWd0nVgsWo4j8T/mUmegyOQatxDXPVW8Y5WYpJMCthNhMdEOQ6', 'general', '2022-03-12 13:09:28', 'active');
INSERT INTO `users` VALUES (702, 'testkarnak@gmail.com', 'karnkai', '$2b$10$WjhSWd0nVgsWo4j8T/mUmeD87S99tLuvxklOJX3MXMuvL6I5Tt.TG', 'admin', '2022-03-12 12:26:06', 'active');

-- ----------------------------
-- Procedure structure for pcd_userDelete
-- ----------------------------
DROP PROCEDURE IF EXISTS `pcd_userDelete`;
delimiter ;;
CREATE PROCEDURE `pcd_userDelete`(IN `USER_ID` INT)
DELETE FROM users WHERE id=USER_ID
;;
delimiter ;

-- ----------------------------
-- Procedure structure for pcd_userId
-- ----------------------------
DROP PROCEDURE IF EXISTS `pcd_userId`;
delimiter ;;
CREATE PROCEDURE `pcd_userId`(IN `id` INT)
BEGIN
SELECT * FROM users WHERE users.id = id;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for pcd_userInsert
-- ----------------------------
DROP PROCEDURE IF EXISTS `pcd_userInsert`;
delimiter ;;
CREATE PROCEDURE `pcd_userInsert`(IN `email` VARCHAR(100), IN `username` VARCHAR(100), IN `password` VARCHAR(100))
BEGIN
INSERT INTO users (email, username, password) VALUES (`email`, `username`, `password`);
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for pcd_userLogin
-- ----------------------------
DROP PROCEDURE IF EXISTS `pcd_userLogin`;
delimiter ;;
CREATE PROCEDURE `pcd_userLogin`(IN `username` VARCHAR(100))
begin
select id, role, username from users where username = `username`;
end
;;
delimiter ;

-- ----------------------------
-- Procedure structure for pcd_users
-- ----------------------------
DROP PROCEDURE IF EXISTS `pcd_users`;
delimiter ;;
CREATE PROCEDURE `pcd_users`()
BEGIN
SELECT * FROM users;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for pcd_userUpdate
-- ----------------------------
DROP PROCEDURE IF EXISTS `pcd_userUpdate`;
delimiter ;;
CREATE PROCEDURE `pcd_userUpdate`(IN `USER_ID` INT, IN `EMAIL` VARCHAR(100), IN `USERNAME` VARCHAR(100), IN `PASSWORD` VARCHAR(100))
UPDATE users SET email=EMAIL, username=USERNAME, password=PASSWORD WHERE id=USER_ID
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
