/*eslint-disable*/
// method isNumber() ແມ່ນຕັ້ງໃຫ້ສາມາດປ້ອນໄດ້ແຕ່ໂຕເລກເທົ່ານັ້ນ
function isNumberInput(evt,pointer) {
  evt = evt ? evt : window.event;
  var charCode = evt.which ? evt.which : evt.keyCode;
  if (
    charCode > 31 &&
    (charCode < 48 || charCode > 57) &&
    charCode !== 46
  ) {
    pointer = false;
    evt.preventDefault();
  } else if (charCode == 46) {
    pointer = true;
  } else {
    return true;
  }
}

export default isNumberInput;