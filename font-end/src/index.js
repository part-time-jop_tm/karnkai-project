import React from "react";
import ReactDOM from "react-dom";
import {
  BrowserRouter,
  Router,
  Route,
  Redirect,
  Switch,
} from "react-router-dom";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import { createBrowserHistory } from "history";
// styles
import "bootstrap/scss/bootstrap.scss";
import "assets/scss/paper-kit.scss?v=1.3.0";
import "assets/demo/demo.css?v=1.3.0";
// pages
import Index from "views/Index.js";
import AdminPage from "views/examples/AdminPage.js";
import LoginPage from "views/examples/LoginPage.js";
import ErrorPage from "views/examples/ErrorPage";
import SectionProduct from "views/index-sections/SectionProduct";
import SectionPhotoBook from "views/index-sections/SectionPhotoBook";
import SectionCalendar from "views/index-sections/SectionCalendar";
import SectionEventDetail from "views/index-sections/SectionEventDetail";
import SectionLogotypeDesign from "views/index-sections/SectionLogotypeDesign";
import SectionWorkShop from "views/index-sections/SectionWorkShop";
import SectionAboutDetail from "views/index-sections/SectionAboutDetail";
import SectionFreevector from "views/index-sections/SectionFreevector";
// core components
import indexReducer from "./stores/index";

const hist = createBrowserHistory();
const store = createStore(indexReducer, applyMiddleware(thunk));

ReactDOM.render(
  <Provider store={store}>
    <Router history={hist}>
      <BrowserRouter>
        <Switch>
          <Route path="/index" render={(props) => <Index {...props} />} />
          <Route
            path="/product/bag-canvas"
            render={(props) => <SectionProduct {...props} />}
          />{" "}
          <Route
            path="/product/photobook"
            render={(props) => <SectionPhotoBook {...props} />}
          />{" "}
          <Route
            path="/product/calendar"
            render={(props) => <SectionCalendar {...props} />}
          />
          <Route
            path="/event"
            render={(props) => <SectionEventDetail {...props} />}
          />
          <Route
            path="/logotypedesign"
            render={(props) => <SectionLogotypeDesign {...props} />}
          />
          <Route
            path="/workshop"
            render={(props) => <SectionWorkShop {...props} />}
          />
          <Route
            path="/about"
            render={(props) => <SectionAboutDetail {...props} />}
          />
          <Route
            path="/freevector"
            render={(props) => <SectionFreevector {...props} />}
          />
          <Route
            path="/$P$BksB3.YrZwWMJMAIJ2rqWxV740p8yO/admin-page"
            render={(props) => <AdminPage {...props} />}
          />
          <Route
            path="/error-page"
            render={(props) => <ErrorPage {...props} />}
          />
          <Route
            path="/$P$BksB3.YrZwWMJMAIJ2rqWxV740p8yO/login"
            render={(props) => <LoginPage {...props} />}
          />
          <Redirect from="/" to="/index" />
        </Switch>
      </BrowserRouter>
    </Router>
  </Provider>,
  document.getElementById("root")
);
