/*eslint-disable*/
import React from "react";

// reactstrap components
import { Row, Col, Container } from "reactstrap";

function AdminFooter() {
  return (
    <footer>
      <Container>
        <Row>
          <Col className="text-center mb-3">
            <div className="credits ml-auto">
              <span className="copyright text-white">
                COPYRIGHT © {new Date().getFullYear()}, KARNKAIART.COM
              </span>
            </div>
          </Col>
        </Row>
      </Container>
    </footer>
  );
}

export default AdminFooter;
