/*eslint-disable*/
import React from "react";

// reactstrap components
import { Row, Col, Container } from "reactstrap";

function DemoFooter() {
  return (
    // <footer className="footer footer-black footer-white">
    //   <Container>

    //   </Container>
    // </footer>
    <>
      <div className="section section-dark">
        <Container>
          <Row>
            <Col className="text-white" xs="6">
              <h4 className="text-customer">
                <a href="/about">About</a>
              </h4>
            </Col>
            <Col className="text-right mt-4" xs="6">
              <div>
                <span>
                  <a href="https://www.facebook.com/kidkarnkai" target="_blank">
                    <i className="fa fa-facebook-square" />
                    <p className="d-lg-none  ">Facebook</p>
                  </a>
                  <a
                    href="https://www.instagram.com/kidkarnkai/"
                    target="_blank"
                  >
                    <i className="fa fa-instagram  " />
                    <p className="d-lg-none ">Instagram</p>
                  </a>
                </span>
              </div>
            </Col>
          </Row>
          <hr />
          <Row>
            <Col className="text-right">
              <div className="credits ml-auto">
                <h6>
                  <span className="copyright text-customer">
                    COPYRIGHT © {new Date().getFullYear()}, KARNKAIART.COM
                  </span>
                </h6>
              </div>
            </Col>
          </Row>
          <hr />
        </Container>
      </div>
    </>
  );
}

export default DemoFooter;
