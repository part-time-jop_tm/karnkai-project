import { combineReducers } from "redux";
import instagram from "./reducures/instagram.reducure";
import login from "./reducures/login.reducer";
import user from "./reducures/user.reducer";
import download from "./reducures/download.reducer";
import adverse from "./reducures/adverse.reducer";
import bag from "./reducures/bag.reducer";
import canvas from "./reducures/canvas.reducer";
import photobook from "./reducures/photobook.reducer";
import calendar from "./reducures/calendar.reducer";
import twentyone from "./reducures/twentyone.reducer";
import twentytwo from "./reducures/twentytwo.reducer";
import workshop from "./reducures/workshop.reducer";
import logotypedesign from "./reducures/logotypedesign.reducer";
import packagelogotypedesign from "./reducures/packagelogotypedesign.reducer";
import packageData from "./reducures/package.reducer";
import event from "./reducures/event.reducer";
import eventprof from "./reducures/eventprofile.reducer";
import product from "./reducures/product.reducer";
import workshopprof from "./reducures/workshopprofile.reducer";
import preweddingprof from "./reducures/preweddingprofile.reducer";

export default combineReducers({
  instagram,
  login,
  user,
  download,
  adverse,
  bag,
  canvas,
  photobook,
  calendar,
  twentyone,
  twentytwo,
  workshop,
  logotypedesign,
  packagelogotypedesign,
  packageData,
  event,
  eventprof,
  product,
  workshopprof,
  preweddingprof
});
