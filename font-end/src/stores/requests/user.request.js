import { pendingData, errorData, get_users } from "../actions/user.action";
import header from "../header";
import config from "../../config/config";
import service from "../../config/service";

export function getUsers() {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(
      config.URL_API + service.apiGetUsers,
      header.apiPost(
        JSON.stringify({
          userId: localStorage.getItem("userId"),
        })
      )
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          dispatch(get_users(result.data.users));
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

export function insertUser(data) {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(
      config.URL_API + service.apiInsertUser,
      header.apiPost(JSON.stringify(data))
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          alert("ບັນທຶກຂໍ້ມູນສຳເລັດ");
          window.location.reload(true);
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

export function deleteUser(id) {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(
      config.URL_API + service.apiDeleteUser + id,
      header.apiPost(
        JSON.stringify({
          userId: localStorage.getItem("userId"),
        })
      )
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          alert("ລົບຂໍ້ມູນສຳເລັດ");
          window.location.reload(true);
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

export function lockUser(id) {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(
      config.URL_API + service.apiLockUser + id,
      header.apiPut(
        JSON.stringify({
          userId: localStorage.getItem("userId"),
        })
      )
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          alert("ປິດນຳໃຊ້ຂໍ້ມູນສຳເລັດ");
          window.location.reload(true);
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

export function activeUser(id) {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(
      config.URL_API + service.apiActiveUser + id,
      header.apiPut(
        JSON.stringify({
          userId: localStorage.getItem("userId"),
        })
      )
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          alert("ເປີດນຳໃຊ້ຂໍ້ມູນສຳເລັດ");
          window.location.reload(true);
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

export function changePasswordUser(data) {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(
      config.URL_API +
        service.apiChangePasswordUser +
        localStorage.getItem("userId"),
      header.apiPut(JSON.stringify(data))
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          alert("ປ່ຽນລະຫັດໃໝ່ສຳເລັດ");
          header.logout();
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

export function setPasswordUser(id, data) {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(
      config.URL_API + service.apiChangePasswordUser + id,
      header.apiPut(JSON.stringify(data))
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => { 
        if (result.success === true) {
          alert("ຕັ້ງລະຫັດໃໝ່ສຳເລັດ");
          window.location.reload(true);
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

const userRequest = {
  getUsers,
  insertUser,
  lockUser,
  activeUser,
  changePasswordUser,
  setPasswordUser,
};

export default userRequest;
