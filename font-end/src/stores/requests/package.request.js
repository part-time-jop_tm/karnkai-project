import {
  pendingData,
  errorData,
  get_packages,
} from "../actions/package.action";
import header from "../header";
import config from "../../config/config";
import service from "../../config/service";

export function getDatas() {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(
      config.URL_API + service.apiGetPackage,
      header.apiPost(
        JSON.stringify({
          userId: localStorage.getItem("userId"),
        })
      )
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          dispatch(get_packages(result.data.customers));
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

export function insertData(data) {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(
      config.URL_API + service.apiInsertPackage,
      header.apiPost(JSON.stringify(data))
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          alert("ບັນທຶກຂໍ້ມູນສຳເລັດ");
          window.location.reload(true);
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

export function acceptData(id) {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(
      config.URL_API + service.apieUpdatePackag + id,
      header.apiPut(
        JSON.stringify({
          userId: localStorage.getItem("userId"),
        })
      )
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          alert("ຍືນຍັນຂໍ້ມູນສຳເລັດ");
          window.location.reload(true);
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

const packageRequest = {
  getDatas,
  insertData,
  acceptData,
};

export default packageRequest;
