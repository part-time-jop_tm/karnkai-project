import {
    pendingData,
    errorData,
    get_twentytwos,
  } from "../actions/twentytwo.action";
  import header from "../header";
  import config from "../../config/config";
  import service from "../../config/service";
  
  export function getDatas() {
    return (dispatch) => {
      dispatch(pendingData());
      return fetch(
        config.URL_API + service.apiGet2022,
        header.apiPost(
          JSON.stringify({
            userId: localStorage.getItem("userId"),
          })
        )
      )
        .then(header.authorization)
        .then((res) => res.json())
        .then((result) => {
          if (result.success === true) {
            dispatch(get_twentytwos(result.data.images));
          }
          return result;
        })
        .catch((error) => dispatch(errorData(error)));
    };
  }
  
  export function insertData(data) {
    return (dispatch) => {
      dispatch(pendingData());
      return fetch(
        config.URL_API + service.apiInsert2022,
        header.apiPostPicture(data)
      )
        .then(header.authorization)
        .then((res) => res.json())
        .then((result) => {
          if (result.success === true) {
            alert("ບັນທຶກຂໍ້ມູນສຳເລັດ");
            window.location.reload(true);
          }
          return result;
        })
        .catch((error) => dispatch(errorData(error)));
    };
  }
  
  export function deleteData(id) {
    return (dispatch) => {
      dispatch(pendingData());
      return fetch(
        config.URL_API + service.apiDelete2022 + id,
        header.apiPost(
          JSON.stringify({
            userId: localStorage.getItem("userId"),
          })
        )
      )
        .then(header.authorization)
        .then((res) => res.json())
        .then((result) => {
          if (result.success === true) {
            alert("ລົບຂໍ້ມູນສຳເລັດ");
            window.location.reload(true);
          }
          return result;
        })
        .catch((error) => dispatch(errorData(error)));
    };
  }
  
  export function disabledData(id) {
    return (dispatch) => {
      dispatch(pendingData());
      return fetch(
        config.URL_API + service.apiDisabled2022 + id,
        header.apiPut(
          JSON.stringify({
            userId: localStorage.getItem("userId"),
          })
        )
      )
        .then(header.authorization)
        .then((res) => res.json())
        .then((result) => {
          if (result.success === true) {
            alert("ປິດນຳໃຊ້ຂໍ້ມູນສຳເລັດ");
            window.location.reload(true);
          }
          return result;
        })
        .catch((error) => dispatch(errorData(error)));
    };
  }
  
  export function enabledData(id) {
    return (dispatch) => {
      dispatch(pendingData());
      return fetch(
        config.URL_API + service.apiEnabled2022 + id,
        header.apiPut(
          JSON.stringify({
            userId: localStorage.getItem("userId"),
          })
        )
      )
        .then(header.authorization)
        .then((res) => res.json())
        .then((result) => {
          if (result.success === true) {
            alert("ເປີດນຳໃຊ້ຂໍ້ມູນສຳເລັດ");
            window.location.reload(true);
          }
          return result;
        })
        .catch((error) => dispatch(errorData(error)));
    };
  }
  
  const twentytwoRequest = {
    getDatas,
    insertData,
    deleteData,
    disabledData,
    enabledData,
  };
  
  export default twentytwoRequest;