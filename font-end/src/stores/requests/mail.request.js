import header from "../header";
import config from "../../config/config";
import service from "../../config/service";

export function sendMail(data) {
  return () => {
    return fetch(
      config.URL_API + service.apiSendMail,
      header.apiPost(JSON.stringify(data))
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => { 
        return result;
      })
      .catch((error) => {
        return error;
      });
  };
}

const sendmailRequest = {
  sendMail,
};

export default sendmailRequest;
