import {
    pendingData,
    errorData,
    get_products,
    getall_products
  } from "../actions/product.action";
  import header from "../header";
  import config from "../../config/config";
  import service from "../../config/service";
  
  export function getDatas() {
    return (dispatch) => {
      dispatch(pendingData());
      return fetch(
        config.URL_API + service.apiGetProducts,
        header.apiPost(
          JSON.stringify({
            userId: localStorage.getItem("userId"),
          })
        )
      )
        .then(header.authorization)
        .then((res) => res.json())
        .then((result) => {
          if (result.success === true) {
            dispatch(get_products(result.data.images));
          }
          return result;
        })
        .catch((error) => dispatch(errorData(error)));
    };
  }
  
  export function insertData(data) {
    return (dispatch) => {
      dispatch(pendingData());
      return fetch(
        config.URL_API + service.apiInsertProduct,
        header.apiPostPicture(data)
      )
        .then(header.authorization)
        .then((res) => res.json())
        .then((result) => {
          if (result.success === true) {
            alert("ບັນທຶກຂໍ້ມູນສຳເລັດ");
            window.location.reload(true);
          }
          return result;
        })
        .catch((error) => dispatch(errorData(error)));
    };
  }
  
  export function deleteData(id) {
    return (dispatch) => {
      dispatch(pendingData());
      return fetch(
        config.URL_API + service.apiDeleteProduct + id,
        header.apiPost(
          JSON.stringify({
            userId: localStorage.getItem("userId"),
          })
        )
      )
        .then(header.authorization)
        .then((res) => res.json())
        .then((result) => {
          if (result.success === true) {
            alert("ລົບຂໍ້ມູນສຳເລັດ");
            window.location.reload(true);
          }
          return result;
        })
        .catch((error) => dispatch(errorData(error)));
    };
  }
  
  export function disabledData(id) {
    return (dispatch) => {
      dispatch(pendingData());
      return fetch(
        config.URL_API + service.apiDisabledProduct + id,
        header.apiPut(
          JSON.stringify({
            userId: localStorage.getItem("userId"),
          })
        )
      )
        .then(header.authorization)
        .then((res) => res.json())
        .then((result) => {
          if (result.success === true) {
            alert("ປິດນຳໃຊ້ຂໍ້ມູນສຳເລັດ");
            window.location.reload(true);
          }
          return result;
        })
        .catch((error) => dispatch(errorData(error)));
    };
  }
  
  export function enabledData(id) {
    return (dispatch) => {
      dispatch(pendingData());
      return fetch(
        config.URL_API + service.apiEnabledProduct + id,
        header.apiPut(
          JSON.stringify({
            userId: localStorage.getItem("userId"),
          })
        )
      )
        .then(header.authorization)
        .then((res) => res.json())
        .then((result) => {
          if (result.success === true) {
            alert("ເປີດນຳໃຊ້ຂໍ້ມູນສຳເລັດ");
            window.location.reload(true);
          }
          return result;
        })
        .catch((error) => dispatch(errorData(error)));
    };
  }
  
  //
  export function getAllDatas() {
    return (dispatch) => {
      dispatch(pendingData());
      return fetch(config.URL_API + service.apiGetImagesAllProduct, header.apiGet(null))
        .then(header.authorization)
        .then((res) => res.json())
        .then((result) => {
          if (result.success === true) {
            dispatch(getall_products(result.data.images));
          }
          return result;
        })
        .catch((error) => dispatch(errorData(error)));
    };
  }
  
  const productRequest = {
    getDatas,
    insertData,
    deleteData,
    disabledData,
    enabledData,
    //
    getAllDatas
  };
  
  export default productRequest;
  