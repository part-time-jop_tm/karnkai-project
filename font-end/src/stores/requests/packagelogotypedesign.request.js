import {
  pendingData,
  errorData,
  // get_packagelogotypedesigns
} from "../actions/packagelogotypedesign.action";
import header from "../header";
import config from "../../config/config";
import service from "../../config/service";

export function insertData(data) {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(
      config.URL_API + service.apiInsertPackageLogotypeDesign,
      header.apiPost(JSON.stringify(data))
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          alert("ບັນທຶກຂໍ້ມູນສຳເລັດ");
          window.location.reload(true);
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

const packagelogotypedesignRequest = {
  insertData,
};

export default packagelogotypedesignRequest;
