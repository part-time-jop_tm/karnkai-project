import {
  pendingData,
  errorData,
  get_bags,
  getAll_bags,
} from "../actions/bag.action";
import header from "../header";
import config from "../../config/config";
import service from "../../config/service";

export function getDatas() {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(
      config.URL_API + service.apiGetBags,
      header.apiPost(
        JSON.stringify({
          userId: localStorage.getItem("userId"),
        })
      )
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          dispatch(get_bags(result.data.images));
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

export function insertData(data) {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(
      config.URL_API + service.apiInsertBag,
      header.apiPostPicture(data)
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          alert("ບັນທຶກຂໍ້ມູນສຳເລັດ");
          window.location.reload(true);
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

export function deleteData(id) {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(
      config.URL_API + service.apiDeleteBag + id,
      header.apiPost(
        JSON.stringify({
          userId: localStorage.getItem("userId"),
        })
      )
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          alert("ລົບຂໍ້ມູນສຳເລັດ");
          window.location.reload(true);
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

export function disabledData(id) {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(
      config.URL_API + service.apiDisabledBag + id,
      header.apiPut(
        JSON.stringify({
          userId: localStorage.getItem("userId"),
        })
      )
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          alert("ປິດນຳໃຊ້ຂໍ້ມູນສຳເລັດ");
          window.location.reload(true);
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

export function enabledData(id) {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(
      config.URL_API + service.apiEnabledBag + id,
      header.apiPut(
        JSON.stringify({
          userId: localStorage.getItem("userId"),
        })
      )
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          alert("ເປີດນຳໃຊ້ຂໍ້ມູນສຳເລັດ");
          window.location.reload(true);
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

//
export function getImagesDatas() {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(config.URL_API + service.apiGetImageBag, header.apiGet(null))
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          dispatch(getAll_bags(result.data.images));
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

const bagRequest = {
  getDatas,
  insertData,
  deleteData,
  disabledData,
  enabledData,
  //
  getImagesDatas,
};

export default bagRequest;
