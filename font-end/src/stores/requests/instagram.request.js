import {
  pendingData,
  errorData,
  getInstagram,
} from "../actions/instagram.action";
import config from "../../config/config";

const instagramPath = "instagram/picture";

export function getInstagramFt() {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(config.URL_API + instagramPath, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    })
      .then(handleError)
      .then((res) => res.json())
      .then((result) => {  
        dispatch(getInstagram(result.data.images));
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

function handleError(response) {
  if (!response.ok) {
    throw Error(response.statucText);
  }
  return response;
}

const Instagram = {
  getInstagramFt,
};

export default Instagram;
