import {
    pendingData,
    errorData, 
    get_events,
    getImages_events
  } from "../actions/event.action";
  import header from "../header";
  import config from "../../config/config";
  import service from "../../config/service";
  
  export function getDatas() {
    return (dispatch) => {
      dispatch(pendingData());
      return fetch(
        config.URL_API + service.apiGetEvents,
        header.apiPost(
          JSON.stringify({
            userId: localStorage.getItem("userId"),
          })
        )
      )
        .then(header.authorization)
        .then((res) => res.json())
        .then((result) => {
          if (result.success === true) {
            dispatch(get_events(result.data.images));
          }
          return result;
        })
        .catch((error) => dispatch(errorData(error)));
    };
  }
  
  export function insertData(data) {
    return (dispatch) => {
      dispatch(pendingData());
      return fetch(
        config.URL_API + service.apiInsertEvent,
        header.apiPostPicture(data)
      )
        .then(header.authorization)
        .then((res) => res.json())
        .then((result) => {
          if (result.success === true) {
            alert("ບັນທຶກຂໍ້ມູນສຳເລັດ");
            window.location.reload(true);
          }
          return result;
        })
        .catch((error) => dispatch(errorData(error)));
    };
  }
  
  export function deleteData(id) {
    return (dispatch) => {
      dispatch(pendingData());
      return fetch(
        config.URL_API + service.apiDeleteEvent + id,
        header.apiPost(
          JSON.stringify({
            userId: localStorage.getItem("userId"),
          })
        )
      )
        .then(header.authorization)
        .then((res) => res.json())
        .then((result) => {
          if (result.success === true) {
            alert("ລົບຂໍ້ມູນສຳເລັດ");
            window.location.reload(true);
          }
          return result;
        })
        .catch((error) => dispatch(errorData(error)));
    };
  }
  
  export function disabledData(id) {
    return (dispatch) => {
      dispatch(pendingData());
      return fetch(
        config.URL_API + service.apiDisabledEvent + id,
        header.apiPut(
          JSON.stringify({
            userId: localStorage.getItem("userId"),
          })
        )
      )
        .then(header.authorization)
        .then((res) => res.json())
        .then((result) => {
          if (result.success === true) {
            alert("ປິດນຳໃຊ້ຂໍ້ມູນສຳເລັດ");
            window.location.reload(true);
          }
          return result;
        })
        .catch((error) => dispatch(errorData(error)));
    };
  }
  
  export function enabledData(id) {
    return (dispatch) => {
      dispatch(pendingData());
      return fetch(
        config.URL_API + service.apiEnabledEvent + id,
        header.apiPut(
          JSON.stringify({
            userId: localStorage.getItem("userId"),
          })
        )
      )
        .then(header.authorization)
        .then((res) => res.json())
        .then((result) => {
          if (result.success === true) {
            alert("ເປີດນຳໃຊ້ຂໍ້ມູນສຳເລັດ");
            window.location.reload(true);
          }
          return result;
        })
        .catch((error) => dispatch(errorData(error)));
    };
  }
  
  //
  export function getImagesDatas() {
    return (dispatch) => {
      dispatch(pendingData());
      return fetch(config.URL_API + service.apiGetImageEvent, header.apiGet(null))
        .then(header.authorization)
        .then((res) => res.json())
        .then((result) => {
          if (result.success === true) {
            dispatch(getImages_events(result.data.images));
          }
          return result;
        })
        .catch((error) => dispatch(errorData(error)));
    };
  }
  
  const eventRequest = {
    getDatas,
    insertData,
    deleteData,
    disabledData,
    enabledData,
    //
    getImagesDatas,
  };
  
  export default eventRequest;
  