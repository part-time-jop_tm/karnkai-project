import {
  pendingData,
  errorData,
  get_photobooks,
  getAll_photobooks,
} from "../actions/photobook.action";
import header from "../header";
import config from "../../config/config";
import service from "../../config/service";

export function getDatas() {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(
      config.URL_API + service.apiGetPhotoBooks,
      header.apiPost(
        JSON.stringify({
          userId: localStorage.getItem("userId"),
        })
      )
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          dispatch(get_photobooks(result.data.images));
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

export function insertData(data) {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(
      config.URL_API + service.apiInsertPhotoBook,
      header.apiPostPicture(data)
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          alert("ບັນທຶກຂໍ້ມູນສຳເລັດ");
          window.location.reload(true);
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

export function deleteData(id) {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(
      config.URL_API + service.apiDeletePhotoBook + id,
      header.apiPost(
        JSON.stringify({
          userId: localStorage.getItem("userId"),
        })
      )
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          alert("ລົບຂໍ້ມູນສຳເລັດ");
          window.location.reload(true);
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

export function disabledData(id) {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(
      config.URL_API + service.apiDisabledPhotoBook + id,
      header.apiPut(
        JSON.stringify({
          userId: localStorage.getItem("userId"),
        })
      )
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          alert("ປິດນຳໃຊ້ຂໍ້ມູນສຳເລັດ");
          window.location.reload(true);
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

export function enabledData(id) {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(
      config.URL_API + service.apiEnabledPhotoBook + id,
      header.apiPut(
        JSON.stringify({
          userId: localStorage.getItem("userId"),
        })
      )
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          alert("ເປີດນຳໃຊ້ຂໍ້ມູນສຳເລັດ");
          window.location.reload(true);
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

//
export function getAllDatas() {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(config.URL_API + service.apiGetImagesAllPhotobook, header.apiGet(null))
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          dispatch(getAll_photobooks(result.data.images));
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

const photobookRequest = {
  getDatas,
  insertData,
  deleteData,
  disabledData,
  enabledData,
  getAllDatas
};

export default photobookRequest;
