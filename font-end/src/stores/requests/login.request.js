import { pendingData, errorData, loginData } from "../actions/login.action";
import header from "../header";
import service from "../../config/service";
import config from "../../config/config";

export function login(login) {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(config.URL_API + service.apiLogin, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(login),
    })
      .then(header.authorizationLogin)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) { 
          if (result.data.user === "lock") {
            alert("ຊື່ຜູ້ໃຊ້ນີ້ຖືກລ໋ອກ ກະລຸນາຕິດຕໍ່ແອັດມີນ");
          } else {
            dispatch(loginData(result.data));
            localStorage.setItem("isLoggedIn", "true");
            localStorage.setItem("tap", "1");
            localStorage.setItem("session", result.data);
            localStorage.setItem("userId", result.data.user.id);
            localStorage.setItem("username", result.data.user.username);
            localStorage.setItem("userRole", result.data.user.role);
            localStorage.setItem("userStatus", result.data.user.status);
            localStorage.setItem("token", "Bearer " + result.data.token);
          }
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

const loginRequest = {
  login,
};

export default loginRequest;
