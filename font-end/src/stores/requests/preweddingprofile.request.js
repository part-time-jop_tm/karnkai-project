import {
    pendingData,
    errorData,
    get_preweddingprofiles,
    getImage_preweddingprofile
  } from "../actions/preweddingprofile.action";
  import header from "../header";
  import config from "../../config/config";
  import service from "../../config/service";
  
  export function getDatas() {
    return (dispatch) => {
      dispatch(pendingData());
      return fetch(
        config.URL_API + service.apiGetPreWeddingProfile,
        header.apiPost(
          JSON.stringify({
            userId: localStorage.getItem("userId"),
          })
        )
      )
        .then(header.authorization)
        .then((res) => res.json())
        .then((result) => {
          if (result.success === true) {
            dispatch(get_preweddingprofiles(result.data.images));
          }
          return result;
        })
        .catch((error) => dispatch(errorData(error)));
    };
  }
  
  export function insertData(data) {
    return (dispatch) => {
      dispatch(pendingData());
      return fetch(
        config.URL_API + service.apiInsertPreWeddingProfile,
        header.apiPostPicture(data)
      )
        .then(header.authorization)
        .then((res) => res.json())
        .then((result) => {
          if (result.success === true) {
            alert("ບັນທຶກຂໍ້ມູນສຳເລັດ");
            window.location.reload(true);
          }
          return result;
        })
        .catch((error) => dispatch(errorData(error)));
    };
  }
  
  //
  export function getImageDatas() {
    return (dispatch) => {
      dispatch(pendingData());
      return fetch(config.URL_API + service.apiGetImagePreWeddingProfile, header.apiGet(null))
        .then(header.authorization)
        .then((res) => res.json())
        .then((result) => {
          if (result.success === true) {
            dispatch(getImage_preweddingprofile(result.data.images));
          }
          return result;
        })
        .catch((error) => dispatch(errorData(error)));
    };
  }
  
  const workshopprofileRequest = {
    getDatas,
    insertData,
    //
    getImageDatas,
  };
  
  export default workshopprofileRequest;
  