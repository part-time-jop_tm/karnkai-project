import {
  pendingData,
  errorData,
  get_calendars,
  getImages_calendars,
  getAll_calendars
} from "../actions/calendar.action";
import header from "../header";
import config from "../../config/config";
import service from "../../config/service";

export function getDatas() {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(
      config.URL_API + service.apiGetCalendars,
      header.apiPost(
        JSON.stringify({
          userId: localStorage.getItem("userId"),
        })
      )
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          dispatch(get_calendars(result.data.images));
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

export function insertData(data) {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(
      config.URL_API + service.apiInsertCalendar,
      header.apiPostPicture(data)
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          alert("ບັນທຶກຂໍ້ມູນສຳເລັດ");
          window.location.reload(true);
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

export function deleteData(id) {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(
      config.URL_API + service.apiDeleteCalendar + id,
      header.apiPost(
        JSON.stringify({
          userId: localStorage.getItem("userId"),
        })
      )
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          alert("ລົບຂໍ້ມູນສຳເລັດ");
          window.location.reload(true);
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

export function disabledData(id) {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(
      config.URL_API + service.apiDisabledCalendar + id,
      header.apiPut(
        JSON.stringify({
          userId: localStorage.getItem("userId"),
        })
      )
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          alert("ປິດນຳໃຊ້ຂໍ້ມູນສຳເລັດ");
          window.location.reload(true);
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

export function enabledData(id) {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(
      config.URL_API + service.apiEnabledCalendar + id,
      header.apiPut(
        JSON.stringify({
          userId: localStorage.getItem("userId"),
        })
      )
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          alert("ເປີດນຳໃຊ້ຂໍ້ມູນສຳເລັດ");
          window.location.reload(true);
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

//
export function getImageDatas() {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(
      config.URL_API + service.apiGetImageCalendar,
      header.apiGet(null)
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          dispatch(getImages_calendars(result.data.images));
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

export function getAllDatas() {
  return (dispatch) => {
    dispatch(pendingData());
    return fetch(
      config.URL_API + service.apiGetImagesAllCalendar,
      header.apiGet(null)
    )
      .then(header.authorization)
      .then((res) => res.json())
      .then((result) => {
        if (result.success === true) {
          dispatch(getAll_calendars(result.data.images));
        }
        return result;
      })
      .catch((error) => dispatch(errorData(error)));
  };
}

const calendarRequest = {
  getDatas,
  insertData,
  deleteData,
  disabledData,
  enabledData,
  //
  getImageDatas,
  getAllDatas
};

export default calendarRequest;
