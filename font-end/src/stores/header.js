const apiPost = (data) => {
  return {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: localStorage.getItem("token"),
    },
    body: data,
  };
};

const apiPostPicture = (data) => {
  return {
    method: "POST",
    headers: {
      Authorization: localStorage.getItem("token"),
    },
    body: data,
  };
};

const apiGet = (data) => {
  return {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: localStorage.getItem("token"),
    },
    body: data,
  };
};

const apiPut = (data) => {
  return {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: localStorage.getItem("token"),
    },
    body: data,
  };
};

const authorization = (response) => {
  if (!response.ok) {
    // check error code 401
    if (response.status === 401) {
      alert("Token Expired");
      logout();
    }
    throw Error(response.statucText);
  }
  return response;
};

const authorizationLogin = (response) => {
  if (!response.ok) {
    // check error code 401
    if (response.status === 401) {
      alert("ລະຫັດບໍ່ຖືກຕ້ອງ.");
      window.location.reload(true);
    }
    throw Error(response.statucText);
  }
  return response;
};

const logout = () => {
  localStorage.setItem("isLoggedIn", "false");
  localStorage.removeItem("tap");
  localStorage.removeItem("session");
  localStorage.removeItem("userId");
  localStorage.removeItem("username");
  localStorage.removeItem("userRole");
  localStorage.removeItem("userStatus");
  localStorage.removeItem("token");
  window.location.assign("/$P$BksB3.YrZwWMJMAIJ2rqWxV740p8yO/login");
};

const header = {
  apiPost,
  apiPostPicture,
  apiGet,
  apiPut,
  authorization,
  authorizationLogin,
  logout,
};

export default header;
