import {
  PENDING,
  ERROR,
  GET_EVENTPROFILE,
  GETIMAGE_EVENTPROFILE,
} from "../actions/eventprofile.action";

const initialState = {
  eventprofile: [],
  imageeventprofile: [],
  pending: false,
  error: null,
};

export default function eventprof(state = initialState, action) {
  switch (action.type) {
    case PENDING:
      return {
        ...state,
        pending: true,
        error: null,
      };
    case GET_EVENTPROFILE:
      return {
        ...state,
        pending: false,
        eventprofile: action.payload.eventprofile,
      };
    case GETIMAGE_EVENTPROFILE:
      return {
        ...state,
        pending: false,
        imageeventprofile: action.payload.imageeventprofile,
      };
    case ERROR:
      return {
        ...state,
        pending: false,
        error: action.payload.error,
        eventprofile: [],
      };
    default:
      return state;
  }
}
