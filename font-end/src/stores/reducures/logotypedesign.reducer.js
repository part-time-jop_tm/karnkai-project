import { PENDING, ERROR, GET_LOGOTYPEDESIGNS, GETALL_LOGOTYPEDESIGNS } from "../actions/logotypedesign.action";

const initialState = {
  logotypedesigns: [],
  alllogotypedesigns: [],
  pending: false,
  error: null,
};

export default function logotypedesign(state = initialState, action) {
  switch (action.type) {
    case PENDING:
      return {
        ...state,
        pending: true,
        error: null,
      };
    case GET_LOGOTYPEDESIGNS:
      return {
        ...state,
        pending: false,
        logotypedesigns: action.payload.logotypedesigns,
      };
    case GETALL_LOGOTYPEDESIGNS:
      return {
        ...state,
        pending: false,
        alllogotypedesigns: action.payload.alllogotypedesigns,
      };
    case ERROR:
      return {
        ...state,
        pending: false,
        error: action.payload.error,
        logotypedesigns: [],
      };
    default:
      return state;
  }
}
