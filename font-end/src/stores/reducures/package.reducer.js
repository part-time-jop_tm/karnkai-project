import { PENDING, ERROR, GET_PACKAGES } from "../actions/package.action";

const initialState = {
  packages: [],
  pending: false,
  error: null,
};

export default function packageData(state = initialState, action) {
  switch (action.type) {
    case PENDING:
      return {
        ...state,
        pending: true,
        error: null,
      };
    case GET_PACKAGES:
      return {
        ...state,
        pending: false,
        packages: action.payload.packages,
      };
    case ERROR:
      return {
        ...state,
        pending: false,
        error: action.payload.error,
        packages: [],
      };
    default:
      return state;
  }
}
