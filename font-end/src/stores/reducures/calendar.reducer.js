import {
  PENDING,
  ERROR,
  GET_CALENDARS,
  GETIMAGES_CALENDARS,
  GETALL_CALENDARS
} from "../actions/calendar.action";

const initialState = {
  calendars: [],
  imagescalendars: [],
  allcalendars: [],
  pending: false,
  error: null,
};

export default function calendar(state = initialState, action) {
  switch (action.type) {
    case PENDING:
      return {
        ...state,
        pending: true,
        error: null,
      };
    case GET_CALENDARS:
      return {
        ...state,
        pending: false,
        calendars: action.payload.calendars,
      };
    case GETIMAGES_CALENDARS:
      return {
        ...state,
        pending: false,
        imagescalendars: action.payload.imagescalendars,
      };
    case GETALL_CALENDARS:
      return {
        ...state,
        pending: false,
        allcalendars: action.payload.allcalendars,
      };
    case ERROR:
      return {
        ...state,
        pending: false,
        error: action.payload.error,
        calendars: [],
      };
    default:
      return state;
  }
}
