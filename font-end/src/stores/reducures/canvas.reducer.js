import {
  PENDING,
  ERROR,
  GET_CANVASES,
  GETIMAGES_CANVASES,
} from "../actions/canvas.action";

const initialState = {
  canvases: [],
  imagesCanvases: [],
  pending: false,
  error: null,
};

export default function canvas(state = initialState, action) {
  switch (action.type) {
    case PENDING:
      return {
        ...state,
        pending: true,
        error: null,
      };
    case GET_CANVASES:
      return {
        ...state,
        pending: false,
        canvases: action.payload.canvases,
      };
    case GETIMAGES_CANVASES:
      return {
        ...state,
        pending: false,
        imagesCanvases: action.payload.imagesCanvases,
      };
    case ERROR:
      return {
        ...state,
        pending: false,
        error: action.payload.error,
        canvases: [],
      };
    default:
      return state;
  }
}
