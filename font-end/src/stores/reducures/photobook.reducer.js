import {
  PENDING,
  ERROR,
  GET_PHOTOBOOKS,
  GETALL_PHOTOBOOKS,
} from "../actions/photobook.action";

const initialState = {
  photobooks: [],
  Allphotobooks: [],
  pending: false,
  error: null,
};

export default function photobook(state = initialState, action) {
  switch (action.type) {
    case PENDING:
      return {
        ...state,
        pending: true,
        error: null,
      };
    case GET_PHOTOBOOKS:
      return {
        ...state,
        pending: false,
        photobooks: action.payload.photobooks,
      };
    case GETALL_PHOTOBOOKS:
      return {
        ...state,
        pending: false,
        Allphotobooks: action.payload.Allphotobooks,
      };
    case ERROR:
      return {
        ...state,
        pending: false,
        error: action.payload.error,
        photobooks: [],
      };
    default:
      return state;
  }
}
