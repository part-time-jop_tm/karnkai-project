import {
  PENDING,
  ERROR,
  GET_PACKAGELOGOTYPEDESIGNS,
} from "../actions/packagelogotypedesign.action";

const initialState = {
  packagelogotypedesigns: [],
  pending: false,
  error: null,
};

export default function packagelogotypedesign(state = initialState, action) {
  switch (action.type) {
    case PENDING:
      return {
        ...state,
        pending: true,
        error: null,
      };
    case GET_PACKAGELOGOTYPEDESIGNS:
      return {
        ...state,
        pending: false,
        packagelogotypedesigns: action.payload.packagelogotypedesigns,
      };
    case ERROR:
      return {
        ...state,
        pending: false,
        error: action.payload.error,
        packagelogotypedesigns: [],
      };
    default:
      return state;
  }
}
