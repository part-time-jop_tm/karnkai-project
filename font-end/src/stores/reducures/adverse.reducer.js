import {
    PENDING,
    ERROR,
    GET_ADVERSES,
    GETALL_ADVERSES
  } from "../actions/adverse.action";
  
  const initialState = {
    adverses: [],
    Alladverses: [],
    pending: false,
    error: null,
  };
  
  export default function adverse(state = initialState, action) {
    switch (action.type) {
      case PENDING:
        return {
          ...state,
          pending: true,
          error: null,
        };
      case GET_ADVERSES:
        return {
          ...state,
          pending: false,
          adverses: action.payload.adverses,
        }; 
      case GETALL_ADVERSES:
        return {
          ...state,
          pending: false,
          Alladverses: action.payload.Alladverses,
        }; 
      case ERROR:
        return {
          ...state,
          pending: false,
          error: action.payload.error,
          adverses: [],
        };
      default:
        return state;
    }
  }
  