import { PENDING, ERROR, GET_TWENTYTWOS } from "../actions/twentytwo.action";

const initialState = {
  twentytwos: [],
  pending: false,
  error: null,
};

export default function twentytwo(state = initialState, action) {
  switch (action.type) {
    case PENDING:
      return {
        ...state,
        pending: true,
        error: null,
      };
    case GET_TWENTYTWOS:
      return {
        ...state,
        pending: false,
        twentytwos: action.payload.twentytwos,
      };
    case ERROR:
      return {
        ...state,
        pending: false,
        error: action.payload.error,
        twentytwos: [],
      };
    default:
      return state;
  }
}
