import {
    PENDING,
    ERROR,
    GET_PREWEDDINGPROFILES,
    GETIMAGE_PREWEDDINGPROFILE
  } from "../actions/preweddingprofile.action";
  
  const initialState = {
    preweddingprofiles: [],
    imagespreweddingprofile: [],
    pending: false,
    error: null,
  };
  
  export default function preweddingprof(state = initialState, action) {
    switch (action.type) {
      case PENDING:
        return {
          ...state,
          pending: true,
          error: null,
        };
      case GET_PREWEDDINGPROFILES:
        return {
          ...state,
          pending: false,
          preweddingprofiles: action.payload.preweddingprofiles,
        };
      case GETIMAGE_PREWEDDINGPROFILE:
        return {
          ...state,
          pending: false,
          imagespreweddingprofile: action.payload.imagespreweddingprofile,
        };
      case ERROR:
        return {
          ...state,
          pending: false,
          error: action.payload.error,
          preweddingprofiles: [],
        };
      default:
        return state;
    }
  }
  