import {
  PENDING,
  ERROR,
  GET_WORKSHOPS,
  GETALL_WORKSHOPS,
  GETIMAGES_WORKSHOPS,
} from "../actions/workshop.action";

const initialState = {
  workshops: [],
  Allworkshops: [],
  imagesworkshops: [],
  pending: false,
  error: null,
};

export default function workshop(state = initialState, action) {
  switch (action.type) {
    case PENDING:
      return {
        ...state,
        pending: true,
        error: null,
      };
    case GET_WORKSHOPS:
      return {
        ...state,
        pending: false,
        workshops: action.payload.workshops,
      };
    case GETALL_WORKSHOPS:
      return {
        ...state,
        pending: false,
        Allworkshops: action.payload.Allworkshops,
      };
    case GETIMAGES_WORKSHOPS:
      return {
        ...state,
        pending: false,
        imagesworkshops: action.payload.imagesworkshops,
      };
    case ERROR:
      return {
        ...state,
        pending: false,
        error: action.payload.error,
        workshops: [],
      };
    default:
      return state;
  }
}
