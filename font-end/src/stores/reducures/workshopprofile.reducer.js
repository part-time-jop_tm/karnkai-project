import {
    PENDING,
    ERROR,
    GET_WORKSHOPPROFILES,
    GETIMAGE_WORKSHOPPROFILE
  } from "../actions/workshopprofile.action";
  
  const initialState = {
    workshopprofiles: [],
    imagesworkshopprofile: [],
    pending: false,
    error: null,
  };
  
  export default function workshopprof(state = initialState, action) {
    switch (action.type) {
      case PENDING:
        return {
          ...state,
          pending: true,
          error: null,
        };
      case GET_WORKSHOPPROFILES:
        return {
          ...state,
          pending: false,
          workshopprofiles: action.payload.workshopprofiles,
        };
      case GETIMAGE_WORKSHOPPROFILE:
        return {
          ...state,
          pending: false,
          imagesworkshopprofile: action.payload.imagesworkshopprofile,
        };
      case ERROR:
        return {
          ...state,
          pending: false,
          error: action.payload.error,
          workshopprofiles: [],
        };
      default:
        return state;
    }
  }
  