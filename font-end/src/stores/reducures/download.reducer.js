import {
    PENDING,
    ERROR,
    GET_DOWNLOAD,
    GETALL_DOWNLOAD
  } from "../actions/download.action";
  
  const initialState = {
    downloads: [],
    Alldownloads: [],
    pending: false,
    error: null,
  };
  
  export default function download(state = initialState, action) {
    switch (action.type) {
      case PENDING:
        return {
          ...state,
          pending: true,
          error: null,
        };
      case GET_DOWNLOAD:
        return {
          ...state,
          pending: false,
          downloads: action.payload.downloads,
        }; 
      case GETALL_DOWNLOAD:
        return {
          ...state,
          pending: false,
          Alldownloads: action.payload.Alldownloads,
        }; 
      case ERROR:
        return {
          ...state,
          pending: false,
          error: action.payload.error,
          downloads: [],
        };
      default:
        return state;
    }
  }
  