import {
    PENDING,
    ERROR,
    GET_PRODUCTS,
    GETALL_PRODUCTS
  } from "../actions/product.action";
  
  const initialState = {
    products: [],
    allproducts: [],
    pending: false,
    error: null,
  };
  
  export default function product(state = initialState, action) {
    switch (action.type) {
      case PENDING:
        return {
          ...state,
          pending: true,
          error: null,
        };
      case GET_PRODUCTS:
        return {
          ...state,
          pending: false,
          products: action.payload.products,
        };
      case GETALL_PRODUCTS:
        return {
          ...state,
          pending: false,
          allproducts: action.payload.allproducts,
        };
      case ERROR:
        return {
          ...state,
          pending: false,
          error: action.payload.error,
          products: [],
        };
      default:
        return state;
    }
  }
  