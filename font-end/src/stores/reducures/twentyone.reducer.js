import { PENDING, ERROR, GET_TWENTYONES, GETIMAGES_TWENTYONES } from "../actions/twentyone.action";

const initialState = {
  twentyones: [],
  imagestwentyones: [],
  pending: false,
  error: null,
};

export default function twentyone(state = initialState, action) {
  switch (action.type) {
    case PENDING:
      return {
        ...state,
        pending: true,
        error: null,
      };
    case GET_TWENTYONES:
      return {
        ...state,
        pending: false,
        twentyones: action.payload.twentyones,
      };
    case GETIMAGES_TWENTYONES:
      return {
        ...state,
        pending: false,
        imagestwentyones: action.payload.imagestwentyones,
      };
    case ERROR:
      return {
        ...state,
        pending: false,
        error: action.payload.error,
        twentyones: [],
      };
    default:
      return state;
  }
}
