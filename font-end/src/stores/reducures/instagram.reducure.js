import { PENDING, ERROR, GET_INSTAGRAM } from "../actions/instagram.action";

const initialState = {
  instagram: [],
  pending: false,
  error: null,
};

export default function user(state = initialState, action) {
  switch (action.type) {
    case PENDING:
      return {
        ...state,
        pending: true,
        error: null,
      };
    case GET_INSTAGRAM:
      return {
        ...state,
        pending: false,
        instagram: action.payload.instagram,
      }; 
    case ERROR:
      return {
        ...state,
        pending: false,
        error: action.payload.error,
        instagram: [],
      }; 
    default:
      return state;
  }
}
