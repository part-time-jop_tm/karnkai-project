import {
  PENDING,
  ERROR,
  GET_USERS, 
} from "../actions/user.action";

const initialState = {
  users: [],
  pending: false,
  error: null,
};

export default function user(state = initialState, action) {
  switch (action.type) {
    case PENDING:
      return {
        ...state,
        pending: true,
        error: null,
      };
    case GET_USERS:
      return {
        ...state,
        pending: false,
        users: action.payload.users,
      }; 
    case ERROR:
      return {
        ...state,
        pending: false,
        error: action.payload.error,
        users: [],
      };
    default:
      return state;
  }
}
