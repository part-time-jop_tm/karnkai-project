import { PENDING, ERROR, GET_BAGS, GETALL_BAGS } from "../actions/bag.action";

const initialState = {
  bags: [],
  Allbags: [],
  pending: false,
  error: null,
};

export default function bag(state = initialState, action) {
  switch (action.type) {
    case PENDING:
      return {
        ...state,
        pending: true,
        error: null,
      };
    case GET_BAGS:
      return {
        ...state,
        pending: false,
        bags: action.payload.bags,
      };
    case GETALL_BAGS:
      return {
        ...state,
        pending: false,
        Allbags: action.payload.Allbags,
      };
    case ERROR:
      return {
        ...state,
        pending: false,
        error: action.payload.error,
        bags: [],
      };
    default:
      return state;
  }
}
