import {
  PENDING,
  ERROR,
  GET_EVENTS,
  GETIMAGES_EVENTS,
} from "../actions/event.action";

const initialState = {
  events: [],
  imageevents: [],
  pending: false,
  error: null,
};

export default function event(state = initialState, action) {
  switch (action.type) {
    case PENDING:
      return {
        ...state,
        pending: true,
        error: null,
      };
    case GET_EVENTS:
      return {
        ...state,
        pending: false,
        events: action.payload.events,
      };
    case GETIMAGES_EVENTS:
      return {
        ...state,
        pending: false,
        imageevents: action.payload.imageevents,
      };
    case ERROR:
      return {
        ...state,
        pending: false,
        error: action.payload.error,
        events: [],
      };
    default:
      return state;
  }
}
