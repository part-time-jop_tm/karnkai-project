import { PENDING, ERROR, LOGIN } from "../actions/login.action";

const initialState = {
  user: [],
  pending: false,
  error: null,
};

export default function login(state = initialState, action) {
  switch (action.type) {
    case PENDING:
      return {
        ...state,
        pending: true,
        error: null,
      };
    case LOGIN:
      return {
        ...state,
        pending: false,
        user: action.payload.user,
      }; 
    case ERROR:
      return {
        ...state,
        pending: false,
        error: action.payload.error,
        instagram: [],
      }; 
    default:
      return state;
  }
}
