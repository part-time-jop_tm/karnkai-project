export const PENDING = "PENDING";
export const GET_DOWNLOAD = "GET_DOWNLOAD";  
export const GETALL_DOWNLOAD = "GETALL_DOWNLOAD";  
export const ERROR = "ERROR"; 

export const pendingData = () => ({
  type: PENDING,
});

export const get_downloads = (downloads) => ({
  type: GET_DOWNLOAD,
  payload: { downloads },
});  

export const getAll_downloads = (Alldownloads) => ({
  type: GETALL_DOWNLOAD,
  payload: { Alldownloads },
});  

export const errorData = (error) => ({
  type: ERROR,
  payload: { error },
});