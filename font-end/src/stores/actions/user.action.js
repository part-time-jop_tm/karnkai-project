export const PENDING = "PENDING";
export const GET_USERS = "GET_USERS";  
export const ERROR = "ERROR"; 

export const pendingData = () => ({
  type: PENDING,
});

export const get_users = (users) => ({
  type: GET_USERS,
  payload: { users },
});  

export const errorData = (error) => ({
  type: ERROR,
  payload: { error },
});