export const PENDING = "PENDING";
export const GET_TWENTYTWOS = "GET_TWENTYTWOS";  
export const ERROR = "ERROR"; 

export const pendingData = () => ({
  type: PENDING,
});

export const get_twentytwos = (twentytwos) => ({
  type: GET_TWENTYTWOS,
  payload: { twentytwos },
});  

export const errorData = (error) => ({
  type: ERROR,
  payload: { error },
});