export const PENDING = "PENDING";
export const GET_CALENDARS = "GET_CALENDARS";  
export const GETIMAGES_CALENDARS = "GETIMAGES_CALENDARS";  
export const GETALL_CALENDARS = "GETALL_CALENDARS";  
export const ERROR = "ERROR"; 

export const pendingData = () => ({
  type: PENDING,
});

export const get_calendars = (calendars) => ({
  type: GET_CALENDARS,
  payload: { calendars },
});  

export const getImages_calendars = (imagescalendars) => ({
  type: GETIMAGES_CALENDARS,
  payload: { imagescalendars },
});  

export const getAll_calendars = (allcalendars) => ({
  type: GETALL_CALENDARS,
  payload: { allcalendars },
});  

export const errorData = (error) => ({
  type: ERROR,
  payload: { error },
});