export const PENDING = "PENDING";
export const GET_PRODUCTS = "GET_PRODUCTS";  
export const GETALL_PRODUCTS = "GETALL_PRODUCTS";  
export const ERROR = "ERROR"; 

export const pendingData = () => ({
  type: PENDING,
});

export const get_products = (products) => ({
  type: GET_PRODUCTS,
  payload: { products },
}); 

export const getall_products = (allproducts) => ({
  type: GETALL_PRODUCTS,
  payload: { allproducts },
});  

export const errorData = (error) => ({
  type: ERROR,
  payload: { error },
});