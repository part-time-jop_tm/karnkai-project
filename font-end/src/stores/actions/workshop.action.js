export const PENDING = "PENDING";
export const GET_WORKSHOPS = "GET_WORKSHOPS";  
export const GETALL_WORKSHOPS = "GETALL_WORKSHOPS";  
export const GETIMAGES_WORKSHOPS = "GETIMAGES_WORKSHOPS";  
export const ERROR = "ERROR"; 

export const pendingData = () => ({
  type: PENDING,
});

export const get_workshops = (workshops) => ({
  type: GET_WORKSHOPS,
  payload: { workshops },
});  

export const getAll_workshops = (Allworkshops) => ({
  type: GETALL_WORKSHOPS,
  payload: { Allworkshops },
}); 

export const getImages_workshops = (imagesworkshops) => ({
  type: GETIMAGES_WORKSHOPS,
  payload: { imagesworkshops },
});

export const errorData = (error) => ({
  type: ERROR,
  payload: { error },
});