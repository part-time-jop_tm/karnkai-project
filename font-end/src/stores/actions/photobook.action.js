export const PENDING = "PENDING";
export const GET_PHOTOBOOKS = "GET_PHOTOBOOKS";  
export const GETALL_PHOTOBOOKS = "GETALL_PHOTOBOOKS";  
export const ERROR = "ERROR"; 

export const pendingData = () => ({
  type: PENDING,
});

export const get_photobooks = (photobooks) => ({
  type: GET_PHOTOBOOKS,
  payload: { photobooks },
}); 

export const getAll_photobooks = (Allphotobooks) => ({
  type: GETALL_PHOTOBOOKS,
  payload: { Allphotobooks },
});  

export const errorData = (error) => ({
  type: ERROR,
  payload: { error },
});