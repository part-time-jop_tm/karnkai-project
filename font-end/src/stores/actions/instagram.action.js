export const PENDING = "PENDING";
export const GET_INSTAGRAM = "GET_INSTAGRAM"; 
export const ERROR = "ERROR"; 

export const pendingData = () => ({
  type: PENDING,
});

export const getInstagram = (instagram) => ({
  type: GET_INSTAGRAM,
  payload: { instagram },
}); 

export const errorData = (error) => ({
  type: ERROR,
  payload: { error },
});
