export const PENDING = "PENDING";
export const LOGIN = "LOGIN"; 
export const ERROR = "ERROR"; 

export const pendingData = () => ({
  type: PENDING,
});

export const loginData = (login) => ({
  type: LOGIN,
  payload: { login },
}); 

export const errorData = (error) => ({
  type: ERROR,
  payload: { error },
});
