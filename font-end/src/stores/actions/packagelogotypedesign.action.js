export const PENDING = "PENDING";
export const GET_PACKAGELOGOTYPEDESIGNS = "GET_PACKAGELOGOTYPEDESIGNS";   
export const ERROR = "ERROR"; 

export const pendingData = () => ({
  type: PENDING,
});

export const get_packagelogotypedesigns = (packagelogotypedesigns) => ({
  type: GET_PACKAGELOGOTYPEDESIGNS,
  payload: { packagelogotypedesigns },
});  

export const errorData = (error) => ({
  type: ERROR,
  payload: { error },
});