export const PENDING = "PENDING";
export const GET_ADVERSES = "GET_ADVERSES";
export const GETALL_ADVERSES = "GETALL_ADVERSES";    
export const ERROR = "ERROR"; 

export const pendingData = () => ({
  type: PENDING,
});

export const get_adverses = (adverses) => ({
  type: GET_ADVERSES,
  payload: { adverses },
});  

export const getAll_adverses = (Alladverses) => ({
  type: GETALL_ADVERSES,
  payload: { Alladverses },
}); 

export const errorData = (error) => ({
  type: ERROR,
  payload: { error },
});