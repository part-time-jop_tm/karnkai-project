export const PENDING = "PENDING";
export const GET_EVENTS = "GET_EVENTS";
export const GETIMAGES_EVENTS = "GETIMAGES_EVENTS";
export const ERROR = "ERROR";

export const pendingData = () => ({
  type: PENDING,
});

export const get_events = (events) => ({
  type: GET_EVENTS,
  payload: { events },
});

export const getImages_events = (imageevents) => ({
  type: GETIMAGES_EVENTS,
  payload: { imageevents },
});

export const errorData = (error) => ({
  type: ERROR,
  payload: { error },
});
