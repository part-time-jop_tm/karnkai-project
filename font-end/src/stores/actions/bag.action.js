export const PENDING = "PENDING";
export const GET_BAGS = "GET_BAGS";  
export const GETALL_BAGS = "GETALL_BAGS";  
export const ERROR = "ERROR"; 

export const pendingData = () => ({
  type: PENDING,
});

export const get_bags = (bags) => ({
  type: GET_BAGS,
  payload: { bags },
});  

export const getAll_bags = (Allbags) => ({
  type: GETALL_BAGS,
  payload: { Allbags },
});  

export const errorData = (error) => ({
  type: ERROR,
  payload: { error },
});