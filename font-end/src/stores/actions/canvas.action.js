export const PENDING = "PENDING";
export const GET_CANVASES = "GET_CANVASES";  
export const GETIMAGES_CANVASES = "GETIMAGES_CANVASES";  
export const ERROR = "ERROR"; 

export const pendingData = () => ({
  type: PENDING,
});

export const get_canvases = (canvases) => ({
  type: GET_CANVASES,
  payload: { canvases },
});  

export const getImages_canvases = (imagesCanvases) => ({
  type: GETIMAGES_CANVASES,
  payload: { imagesCanvases },
});

export const errorData = (error) => ({
  type: ERROR,
  payload: { error },
});