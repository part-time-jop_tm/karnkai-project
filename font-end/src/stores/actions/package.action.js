export const PENDING = "PENDING";
export const GET_PACKAGES = "GET_PACKAGES";   
export const ERROR = "ERROR"; 

export const pendingData = () => ({
  type: PENDING,
});

export const get_packages = (packages) => ({
  type: GET_PACKAGES,
  payload: { packages },
});  

export const errorData = (error) => ({
  type: ERROR,
  payload: { error },
});