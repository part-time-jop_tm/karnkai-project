export const PENDING = "PENDING";
export const GET_WORKSHOPPROFILES = "GET_WORKSHOPPROFILES";   
export const GETIMAGE_WORKSHOPPROFILE = "GETIMAGE_WORKSHOPPROFILE";  
export const ERROR = "ERROR"; 

export const pendingData = () => ({
  type: PENDING,
});

export const get_workshopprofiles = (workshopprofiles) => ({
  type: GET_WORKSHOPPROFILES,
  payload: { workshopprofiles },
});   

export const getImage_workshopprofile = (imagesworkshopprofile) => ({
  type: GETIMAGE_WORKSHOPPROFILE,
  payload: { imagesworkshopprofile },
});

export const errorData = (error) => ({
  type: ERROR,
  payload: { error },
});