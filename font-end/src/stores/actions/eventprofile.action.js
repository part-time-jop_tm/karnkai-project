export const PENDING = "PENDING";
export const GET_EVENTPROFILE = "GET_EVENTPROFILE";   
export const GETIMAGE_EVENTPROFILE = "GETIMAGE_EVENTPROFILE";   
export const ERROR = "ERROR"; 

export const pendingData = () => ({
  type: PENDING,
});

export const get_eventprofile = (eventprofile) => ({
  type: GET_EVENTPROFILE,
  payload: { eventprofile },
});   

export const getImage_eventprofile = (imageeventprofile) => ({
  type: GETIMAGE_EVENTPROFILE,
  payload: { imageeventprofile },
});   


export const errorData = (error) => ({
  type: ERROR,
  payload: { error },
});