export const PENDING = "PENDING";
export const GET_LOGOTYPEDESIGNS = "GET_LOGOTYPEDESIGNS";  
export const GETALL_LOGOTYPEDESIGNS = "GETALL_LOGOTYPEDESIGNS";  
export const ERROR = "ERROR"; 

export const pendingData = () => ({
  type: PENDING,
});

export const get_logotypedesigns = (logotypedesigns) => ({
  type: GET_LOGOTYPEDESIGNS,
  payload: { logotypedesigns },
}); 

export const getAll_logotypedesigns = (alllogotypedesigns) => ({
  type: GETALL_LOGOTYPEDESIGNS,
  payload: { alllogotypedesigns },
});  

export const errorData = (error) => ({
  type: ERROR,
  payload: { error },
});