export const PENDING = "PENDING";
export const GET_TWENTYONES = "GET_TWENTYONES";  
export const GETIMAGES_TWENTYONES = "GETIMAGES_TWENTYONES";  
export const ERROR = "ERROR"; 

export const pendingData = () => ({
  type: PENDING,
});

export const get_twentyones = (twentyones) => ({
  type: GET_TWENTYONES,
  payload: { twentyones },
});  

export const getImages_twentyones = (imagestwentyones) => ({
  type: GETIMAGES_TWENTYONES,
  payload: { imagestwentyones },
}); 

export const errorData = (error) => ({
  type: ERROR,
  payload: { error },
});