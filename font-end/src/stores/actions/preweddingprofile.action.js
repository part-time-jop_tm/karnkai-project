export const PENDING = "PENDING";
export const GET_PREWEDDINGPROFILES = "GET_PREWEDDINGPROFILES";   
export const GETIMAGE_PREWEDDINGPROFILE = "GETIMAGE_PREWEDDINGPROFILE";  
export const ERROR = "ERROR"; 

export const pendingData = () => ({
  type: PENDING,
});

export const get_preweddingprofiles = (preweddingprofiles) => ({
  type: GET_PREWEDDINGPROFILES,
  payload: { preweddingprofiles },
});   

export const getImage_preweddingprofile = (imagespreweddingprofile) => ({
  type: GETIMAGE_PREWEDDINGPROFILE,
  payload: { imagespreweddingprofile },
});

export const errorData = (error) => ({
  type: ERROR,
  payload: { error },
});