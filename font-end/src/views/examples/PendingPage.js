/*eslint-disable*/
import React from "react"; 

// reactstrap components
import { 
  Container,
  Row,
  Col, 
  Spinner,
} from "reactstrap";

function PendingPage() {
  return (
    <>
      <div className="section section-dark" >
        <Container style={{ marginTop: '20%', marginBottom: '20%' }}>
          <Row>
            <Col className="ml-auto mr-auto text-center" md="12">
              <Spinner color="danger">Loading...</Spinner>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

export default PendingPage;
