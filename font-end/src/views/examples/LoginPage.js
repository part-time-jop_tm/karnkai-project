/*eslint-disable*/
import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
// reactstrap components
import { Button, Card, Form, Input, Container, Row, Col } from "reactstrap";


// core components
import Loading from "views/customize/Loading";

// api connection
import { login } from "../../stores/requests/login.request";

const RegisterPage = (props) => {
  // login session
  if (localStorage.getItem("isLoggedIn") === "true") {
    window.location.assign("/$P$BksB3.YrZwWMJMAIJ2rqWxV740p8yO/admin-page");
  }
  document.documentElement.classList.remove("nav-open");
  React.useEffect(() => {
    document.body.classList.add("register-page");
    return function cleanup() {
      document.body.classList.remove("register-page");
    };
  });

  const {
    username,
    password,
    handleChangeUsername,
    handleChangePassword,
    loginSubmit,
  } = props;

  return (
    <>
      <div
        className="page-header"
        style={{
          backgroundImage:
            "url(" +
            require("assets/img/karnkai/That-Luang-Festival.jpg").default +
            ")",
        }}
      >
        <div className="filter" />
        <Container className="fixed-top">
          <Row>
            <Col className="ml-auto mr-auto" lg="4">
              <Card className="card-register ml-auto mr-auto">
                <h6 className="title mx-auto">Welcome to Karnkai for admin</h6>
                <Form>
                  <label>Username</label>
                  <Input
                    placeholder="Username"
                    type="text"
                    onChange={handleChangeUsername}
                    value={username}
                  />
                  <label>Password</label>
                  <Input
                    placeholder="Password"
                    type="password"
                    onChange={handleChangePassword}
                    value={password}
                  />
                  <Button
                    block
                    className="btn-round"
                    color="danger"
                    onClick={loginSubmit}
                    data-action='submit'
                  >
                    Login
                  </Button>
                </Form>
              </Card>
            </Col>
          </Row>
        </Container>
        <div className="footer register-footer text-center">
          <h6>
            COPYRIGHT © {new Date().getFullYear()}, KARNKAI.la{" "}
            <i className="fa fa-heart heart" /> ALL RIGHTS RESE.
          </h6>
        </div>
      </div>
    </>
  );
};

class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      loading: false,
      openEmpty: false,
      openError: false,
    };
    this.handleChangeUsername = this.handleChangeUsername.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
    this.loginSubmit = this.loginSubmit.bind(this);
  }


  // this is comment for handleChange function
  handleChangeUsername(event) {
    this.setState({
      username: event.target.value,
    });
  }

  handleChangePassword(event) {
    this.setState({
      password: event.target.value,
    });
  }

  // function poppup card loding
  HandleLoding = () => {
    this.setState({ loading: true });
  };

  // this is comment for login function
  loginSubmit(event) {
    event.preventDefault();

    window.grecaptcha.ready(function() {
      window.grecaptcha.execute('6LexUtUeAAAAAKiIId7lkm7ASwJJ1s51E5lDHwOW', {action: 'submit'}).then(function(token) {
        // Send form value as well as token to the server
      });
    });


    if (this.state.username === "" || this.state.password === "") {
      alert("ກະລຸນາປ້ອນຂໍ້ມູນໃຫ້ຄົບ.");
    } else {
      this.HandleLoding();
      const data = {
        username: this.state.username,
        password: this.state.password,
      };
      this.props.dispatch(login(data)).then((result) => {
        setTimeout(() => {
          this.setState({ loading: false });
          if (result.success === true) {
            window.location.assign("/$P$BksB3.YrZwWMJMAIJ2rqWxV740p8yO/admin-page");
          }
        }, 2000);
      });
    }
  }

  render() {
    const { username, password } = this.state;
    return (
      <>
        <RegisterPage
          username={username}
          password={password}
          loginSubmit={this.loginSubmit}
          handleChangeUsername={(event) => this.handleChangeUsername(event)}
          handleChangePassword={(event) => this.handleChangePassword(event)}
        />
        <Loading open={this.state.loading} />
      </>
    );
  }
}

const mapStateToProps = () => ({});

export default compose(connect(mapStateToProps, null))(LoginPage);
