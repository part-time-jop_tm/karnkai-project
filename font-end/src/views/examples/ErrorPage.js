/*eslint-disable*/
import React from "react";

// reactstrap components
import { Container, Row, Col, NavbarBrand } from "reactstrap";

// core components

const ErrorPage = () => {
  return (
    <> 
      <div className="section section-dark">
        <Container style={{ marginTop: "20%", marginBottom: "20%" }}>
          <Row>
            <Col className="ml-auto mr-auto text-center" md="12">
              <span className="copyright text-danger">Error this page.</span>
            </Col>
          </Row>
          <Row>
            <Col className="ml-auto mr-auto text-center" md="12">
              {/* <NavbarBrand data-placement="bottom" href="/$P$BksB3.YrZwWMJMAIJ2rqWxV740p8yO/admin-page">
                <i className="nc-icon nc-refresh-69" />
              </NavbarBrand> */}
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};

export default ErrorPage;
