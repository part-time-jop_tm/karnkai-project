/*eslint-disable*/
import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";

// reactstrap components
import {
  Container,
  Row,
  Col,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  Button,
} from "reactstrap";

// core components
import AdminFooter from "components/Footers/AdminFooter";
import ChangePassword from "views/Modal/Users/ChangePassword";
// core tap component
import GetUser from "views/customize/user/GetUser";
import GetDownload from "views/customize/download/GetDownload";
import GetAdverse from "views/customize/adverse/GetAdverse";
import GetProduct from "views/customize/product/GetProduct";
import GetPhotobook from "views/customize/photobook/GetPhotobook";
import GetCalendar from "views/customize/calendar/GetCalendar";
import GetEvent from "views/customize/event/GetEvent";
import GetWorkshop from "views/customize/workshop/GetWorkshop";
import GetLogotypeDesign from "views/customize/logotypedesign/GetLogotypeDesign";
import Getpackage from "views/customize/package/Getpackage";

// core api
import { changePasswordUser } from "../../stores/requests/user.request";

// cor header
import header from "../../stores/header";

function AdminPageComponent(props) {
  // login session
  if (localStorage.getItem("isLoggedIn") !== "true") {
    window.location.assign("/$P$BksB3.YrZwWMJMAIJ2rqWxV740p8yO/login");
  }

  setInterval(() => {
    header.logout();
  }, 3600000);

  const [activeTab, setActiveTab] = React.useState(localStorage.getItem("tap"));
  const toggle = (tab) => {
    if (activeTab !== tab) {
      localStorage.setItem("tap", tab);
      setActiveTab(localStorage.getItem("tap"));
    }
  };

  // logout
  const logout = () => {
    header.logout();
  };

  const { HandleOpenChangepassword } = props;

  let pageHeader = React.createRef();

  React.useEffect(() => {
    if (window.innerWidth < 991) {
      const updateScroll = () => {
        let windowScrollTop = window.pageYOffset / 3;
        pageHeader.current.style.transform =
          "translate3d(0," + windowScrollTop + "px,0)";
      };
      window.addEventListener("scroll", updateScroll);
      return function cleanup() {
        window.removeEventListener("scroll", updateScroll);
      };
    }
  });

  return (
    <>
      <div
        style={{
          backgroundImage:
            "url(" +
            require("assets/img/karnkai/That-Luang-Festival.jpg").default +
            ")",
          width: "2048 px",
          height: "1633 px",
        }}
        // className="page-header page-header-xs"
        // data-parallax={true}
        // ref={pageHeader}
      >
        <div className="filter" />
        <Container>
          <Row>
            <Col className="text-right mt-2 mb-2" md="12">
              <p className="text-danger">
                ຜູ້ໃຊ້ປັດຈຸບັນ: {localStorage.getItem("username")}{" "}
              </p>
              <Button color="danger" outline onClick={HandleOpenChangepassword}>
                <i className="nc-icon nc-key-25" /> ປ່ຽນລະຫັດຜູ້ໃຊ້ລະບົບ
              </Button>{" "}
              <Button color="danger" outline onClick={() => logout()}>
                <i className="nc-icon nc-lock-circle-open" /> ອອກຈາກລະບົບ
              </Button>
            </Col>
          </Row>
          <hr />
          <Row>
            <Col md="12">
              <div className="nav-tabs-navigation">
                <div className="nav-tabs-wrapper">
                  <Nav id="tabs" role="tablist" tabs>
                    <NavItem>
                      <NavLink
                        className={activeTab === "1" ? "active" : ""}
                        onClick={() => {
                          toggle("1");
                        }}
                      >
                        Adverse
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={activeTab === "2" ? "active" : ""}
                        onClick={() => {
                          toggle("2");
                        }}
                      >
                        Product
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={activeTab === "3" ? "active" : ""}
                        onClick={() => {
                          toggle("3");
                        }}
                      >
                        Photo Book
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={activeTab === "4" ? "active" : ""}
                        onClick={() => {
                          toggle("4");
                        }}
                      >
                        Calendar
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={activeTab === "5" ? "active" : ""}
                        onClick={() => {
                          toggle("5");
                        }}
                      >
                        Event
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={activeTab === "6" ? "active" : ""}
                        onClick={() => {
                          toggle("6");
                        }}
                      >
                        Logotype Design
                      </NavLink>
                    </NavItem>{" "}
                    <NavItem>
                      <NavLink
                        className={activeTab === "7" ? "active" : ""}
                        onClick={() => {
                          toggle("7");
                        }}
                      >
                        Workshop
                      </NavLink>
                    </NavItem>
                    {/* <NavItem>
                      <NavLink
                        className={activeTab === "8" ? "active" : ""}
                        onClick={() => {
                          toggle("8");
                        }}
                      >
                        Free vector
                      </NavLink>
                    </NavItem> */}
                    <NavItem>
                      <NavLink
                        className={activeTab === "8" ? "active" : ""}
                        onClick={() => {
                          toggle("8");
                        }}
                      >
                        Package
                      </NavLink>
                    </NavItem>
                    {localStorage.getItem("userRole") === "admin" ? (
                      <NavItem>
                        <NavLink
                          className={activeTab === "9" ? "active" : ""}
                          onClick={() => {
                            toggle("9");
                          }}
                        >
                          User
                        </NavLink>
                      </NavItem>
                    ) : null}
                  </Nav>
                </div>
              </div>
              <TabContent activeTab={activeTab} className="text-center">
                <TabPane tabId="1">
                  <GetAdverse />
                </TabPane>
                <TabPane tabId="2">
                  <GetProduct />
                </TabPane>
                <TabPane tabId="3">
                  <GetPhotobook />
                </TabPane>
                <TabPane tabId="4">
                  <GetCalendar />
                </TabPane>
                <TabPane tabId="5">
                  <GetEvent />
                </TabPane>
                <TabPane tabId="6">
                  <GetLogotypeDesign />
                </TabPane>
                <TabPane tabId="7">
                  {" "}
                  <GetWorkshop />
                </TabPane>
                {/* <TabPane tabId="8">
                  {" "}
                  <GetDownload />
                </TabPane> */}
                <TabPane tabId="8">
                  <Getpackage />
                </TabPane>
                <TabPane tabId="9">
                  {localStorage.getItem("userRole") === "admin" ? (
                    <GetUser />
                  ) : null}
                </TabPane>
              </TabContent>
            </Col>
          </Row>
        </Container>
        <AdminFooter />
      </div>{" "}
    </>
  );
}

class AdminPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // modal
      openChangePassword: false,
      // change password
      new_password: "",
      conifrm_password: "",
    };
    this.handleChangeNewPassword = this.handleChangeNewPassword.bind(this);
    this.handleChangeConfirmPassword =
      this.handleChangeConfirmPassword.bind(this);
    this.handleChangepasswordtData = this.handleChangepasswordtData.bind(this);
  }

  // this is comment for handleChange function change password
  handleChangeNewPassword(event) {
    this.setState({
      new_password: event.target.value,
    });
  }

  handleChangeConfirmPassword(event) {
    this.setState({
      conifrm_password: event.target.value,
    });
  }

  // function modal open insert user
  HandleOpenChangepassword = () => {
    this.setState({ openChangePassword: true });
  };

  // function modal close
  handleCloseChangepassword = () => {
    this.setState({ openChangePassword: false });
    this.setState({ new_password: "" });
    this.setState({ conifrm_password: "" });
  };

  handleChangepasswordtData(event) {
    event.preventDefault();
    if (!this.state.new_password || !this.state.conifrm_password)
      return alert("ກະລຸນາຕື່ມຂໍ້ມູນໃຫ້ຄົບ");

    if (this.state.new_password !== this.state.conifrm_password)
      return alert("ກະລຸນາຕື່ມຂໍ້ມູນໃຫ້ຖືກຕ້ອງ");
    if (!confirm("Are you sure?")) return;
    const data = {
      password: this.state.conifrm_password,
      userId: localStorage.getItem("userId"),
    };
    this.props.dispatch(changePasswordUser(data));
    this.handleCloseChangepassword();
  }

  render() {
    const { new_password, conifrm_password } = this.state;
    return (
      <>
        <AdminPageComponent
          HandleOpenChangepassword={this.HandleOpenChangepassword}
        />
        {/* <------------------------ Modal ---------------------------> */}
        {/* change password user */}
        <ChangePassword
          open={this.state.openChangePassword}
          handleCloseChangepassword={this.handleCloseChangepassword}
          handleChangepasswordtData={this.handleChangepasswordtData}
          handleChangeNewPassword={(event) =>
            this.handleChangeNewPassword(event)
          }
          handleChangeConfirmPassword={(event) =>
            this.handleChangeConfirmPassword(event)
          }
          new_password={new_password}
          conifrm_password={conifrm_password}
        />
      </>
    );
  }
}

const mapStateToProps = (state) => ({});

export default compose(connect(mapStateToProps, null))(AdminPage);
