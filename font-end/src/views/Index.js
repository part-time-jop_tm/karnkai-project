import React from "react";
// core components
// import IndexNavbar from "components/Navbars/IndexNavbar.js";
import IndexHeader from "components/Headers/IndexHeader.js";
// import DemoFooter from "components/Footers/DemoFooter.js";
// index sections
import SectionCarousel from "views/index-sections/SectionCarousel.js";
import DemoFooter from "components/Footers/DemoFooter";
// import SectionDownload from "views/index-sections/SectionDownload.js";
// import SectionAbout from "views/index-sections/SectionAbout.js";
// import SectionSouvenir from "views/index-sections/SectionSouvenir.js";
// import SectionExhibition from "views/index-sections/SectionExhibition.js";
// import SectionService from "views/index-sections/SectionService.js";
// import Section2021 from "./index-sections/Section2021";
// import Section2022 from "./index-sections/Section2022";
import SectionProduction from "./index-sections/SectionProduction";
import SectionEvent from "./index-sections/SectionEvent";
import SectionPreWedding from "./index-sections/SectionPreWedding";
import SectionWorkShopBanner from "./index-sections/SectionWorkShopBanner";
import SectionContact from "./index-sections/SectionContact";
// import SectionBag from "./index-sections/SectionBag";
// import SectionCanvas from "./index-sections/SectionCanvas";
// import SectionPhotoBook from "./index-sections/SectionPhotoBook";
// import SectionCalendar from "./index-sections/SectionCalendar";
// import SectionEventDetail from "./index-sections/SectionEventDetail";
import "../assets/style/index.css";

function Index() {
  document.documentElement.classList.remove("nav-open");
  React.useEffect(() => {
    document.body.classList.add("index");
    return function cleanup() {
      document.body.classList.remove("index");
    };
  });
  return (
    <>
      <IndexHeader />
      <div className="main">
        {/* <SectionAbout /> */}
        <SectionCarousel />
        <SectionProduction />
        <SectionEvent />
        <SectionPreWedding />
        <SectionWorkShopBanner />
        {/* <SectionDownload /> */}
        <SectionContact />
        <DemoFooter />
        {/* <SectionBag />
        <SectionCanvas />
        <SectionPhotoBook />
        <SectionCalendar />
        <SectionEventDetail /> */}
        {/* <SectionSouvenir /> */}
        {/* <SectionExhibition />
        <Section2021 />
        <Section2022 />
        <SectionService /> */}
      </div>
    </>
  );
}

export default Index;
