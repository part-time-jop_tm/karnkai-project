/*eslint-disable*/
import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";

// react component used to create sweet alerts
import ReactBSAlert from "react-bootstrap-sweetalert";

// reactstrap components
import { Row, Col, Button, InputGroup, Input } from "reactstrap";

// core api
import { insertData } from "../../../stores/requests/workshopprofile.request";

class ChangeProfileModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // insert download
      picturefile: "",
      description: "",
    };
    this.handleChangePicturefile = this.handleChangePicturefile.bind(this);
    this.handleChangeDescription = this.handleChangeDescription.bind(this);
    this.handleInsertData = this.handleInsertData.bind(this);
  }

  // this is comment for handleChange function
  handleChangePicturefile(event) {
    let picturefile = event.target.files[0];
    this.setState({ picturefile: picturefile });
  }

  handleChangeDescription(event) {
    this.setState({
      description: event.target.value,
    });
  }

  // insert data
  handleInsertData(event) {
    event.preventDefault();
    if (!this.state.picturefile || !this.state.description)
      return alert("ກະລຸນາເລຶອກຮູບ ຫຼື ຕື່ມຂໍ້ມູນໃຫ້ຄົບ");
    if (!confirm("Are you sure?")) return;
    let formdata = new FormData();
    formdata.append("picturefile", this.state.picturefile);
    formdata.append("description", this.state.description);
    formdata.append("userId", localStorage.getItem("userId"));
    this.props.dispatch(insertData(formdata));
    this.props.handleCloseViewImage;
  }

  render() {
    const { open, handleCloseViewImage, image } = this.props;
    return (
      <>
        {open === true ? (
          <ReactBSAlert
            style={{ display: "block", marginTop: "100px" }}
            title=""
            onConfirm={handleCloseViewImage}
            showConfirm={false}
          >
            <div>
              <Row>
                <Col sm="12">
                  <label>ປ່ຽນຮູບໃໝ່</label>
                  <InputGroup>
                    <Input
                      placeholder="picturefile"
                      type="file"
                      accept="image/png, image/jpeg"
                      onChange={(event) => this.handleChangePicturefile(event)}
                    />
                  </InputGroup>
                </Col>
              </Row>
              <Row className="mt-2">
                <Col sm="12">
                  <Input
                    placeholder="ລາຍລະອຽດ"
                    type="textarea"
                    value={this.state.description}
                    onChange={(event) => this.handleChangeDescription(event)}
                  />
                </Col>
              </Row>
              <Row className="mt-2">
                {image.map((item) => {
                  return (
                    <Col key={item.id}>
                      <div>
                        <img
                          alt="..."
                          src={`https://api.karnkaiart.com/api/upload/workshopprofile/${item.image}`}
                          width="100%"
                        />
                      </div>
                    </Col>
                  );
                })}
              </Row>
              <Row className="mt-2">
                <Col>
                  <Button color="danger" outline onClick={handleCloseViewImage}>
                    <i className="nc-icon nc-simple-remove" /> ປິດ
                  </Button>{" "}
                  <Button
                    color="primary"
                    outline
                    onClick={this.handleInsertData}
                  >
                    <i className="nc-icon nc-single-copy-04" /> ບັນທຶກ
                  </Button>
                </Col>
              </Row>
            </div>
          </ReactBSAlert>
        ) : null}
      </>
    );
  }
}

const mapStateToProps = (state) => ({});

export default compose(connect(mapStateToProps, null))(ChangeProfileModal);
