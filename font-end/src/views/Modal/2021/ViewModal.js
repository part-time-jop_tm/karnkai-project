import React from "react";
// react component used to create sweet alerts
import ReactBSAlert from "react-bootstrap-sweetalert";

// reactstrap components
import { Row, Col, Button } from "reactstrap";

class ViewModal extends React.Component {
  render() {
    const { open, handleCloseViewImage, image } = this.props;
    return (
      <>
        {open === true ? (
          <ReactBSAlert
            style={{ display: "block", marginTop: "100px" }}
            title=""
            onConfirm={handleCloseViewImage}
            showConfirm={false}
          >
            <div>
              <Row>
                <Col>
                  {" "}
                  <img
                    alt="..."
                    src={`https://api.karnkaiart.com/api/upload/2021/${image}`}
                    width="100%"
                  />{" "}
                </Col>
              </Row>
              <Row className="mt-2">
                <Col>
                  <Button
                    color="danger"
                    outline
                    onClick={handleCloseViewImage}
                  >
                    <i className="nc-icon nc-simple-remove" />{" "}
                    ປິດ
                  </Button>
                </Col>
              </Row>
            </div>
          </ReactBSAlert>
        ) : null}
      </>
    );
  }
}

export default ViewModal;
