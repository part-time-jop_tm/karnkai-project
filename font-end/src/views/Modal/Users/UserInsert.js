import React from "react";
// react component used to create sweet alerts
import ReactBSAlert from "react-bootstrap-sweetalert";

// reactstrap components
import {
  Button,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from "reactstrap";

class UserInsert extends React.Component {
  render() {
    const {
      open,
      handleClose,
      handleInsertData,
      handleChangePassword,
      handleChangeEmail,
      handleChangeUsername,
      handleChangeRole,
      username,
      email,
      password, 
    } = this.props;
    return (
      <>
        {open === true ? (
          <ReactBSAlert
            style={{ display: "block", marginTop: "100px" }}
            title=""
            onConfirm={handleClose}
            showConfirm={false}
          >
            <div className="section landing-section">
              <Container>
                <Row>
                  <Col className="ml-auto mr-auto" md="8">
                    <h5 className="text-center">ເພີ່ມຂໍ້ມູນຜູ້ໃຊ້ລະບົບ</h5>
                    <Form className="contact-form">
                      <Row className="mb-2">
                        <Col md="12">
                          <InputGroup>
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="nc-icon nc-single-02" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              placeholder="username"
                              type="text"
                              value={username}
                              onChange={handleChangeUsername}
                            />
                          </InputGroup>
                        </Col>
                      </Row>
                      <Row className="mb-2">
                        <Col md="12">
                          <InputGroup>
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="nc-icon nc-email-85" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              placeholder="email@mail.com"
                              type="email"
                              value={email}
                              onChange={handleChangeEmail}
                            />
                          </InputGroup>
                        </Col>
                      </Row>
                      <Row className="mb-2">
                        <Col md="12">
                          <InputGroup>
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="nc-icon nc-key-25" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              placeholder="password"
                              type="password"
                              value={password}
                              onChange={handleChangePassword}
                            />
                          </InputGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col md="12">
                          <Row>
                            <Col className="text-left" md="6">
                              ລະດັບການນຳໃຊ້:
                            </Col>
                            <Col className="text-left" md="6">
                              <div onChange={handleChangeRole}>
                                <Input  
                                  defaultValue="admin"
                                  id="exampleRadios1"
                                  name="exampleRadios"
                                  type="radio"
                                />
                                admin
                                <br />
                                <Input
                                  defaultValue="general"
                                  id="exampleRadios2"
                                  name="exampleRadios"
                                  type="radio"
                                />
                                general
                              </div>
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                      <Row>
                        <Col className="text-center" md="12">
                          <Button color="danger" outline onClick={handleClose}>
                            <i className="nc-icon nc-simple-remove" /> ຍົກເລີກ
                          </Button>{" "}
                          <Button
                            color="primary"
                            outline
                            onClick={handleInsertData}
                          >
                            <i className="nc-icon nc-single-copy-04" /> ບັນທຶກ
                          </Button>
                        </Col>
                      </Row>
                    </Form>
                  </Col>
                </Row>
              </Container>
            </div>
          </ReactBSAlert>
        ) : null}
      </>
    );
  }
}

export default UserInsert;
