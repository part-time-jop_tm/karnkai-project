import React from "react";
// react component used to create sweet alerts
import ReactBSAlert from "react-bootstrap-sweetalert";

// reactstrap components
import {
  Button,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from "reactstrap";

class SetPassword extends React.Component {
  render() {
    const {
      open,
      handleCloseSetpassword,
      handleSetpasswordtData,
      handleChangeSetNewPassword,
      set_password,
      username_set_password,
    } = this.props;
    return (
      <>
        {open === true ? (
          <ReactBSAlert
            style={{ display: "block", marginTop: "100px" }}
            title=""
            onConfirm={handleCloseSetpassword}
            showConfirm={false}
          >
            <div className="section landing-section">
              <Container>
                <Row>
                  <Col className="ml-auto mr-auto" md="8">
                    <h5 className="text-center">ຕັ້ງລະຫັດຜູ້ໃຊ້ລະບົບໃໝ່</h5>
                    <Form className="contact-form">
                      <Row className="mb-2">
                        <Col md="12">
                          <InputGroup>
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="nc-icon nc-single-02" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              disabled
                              placeholder="username"
                              type="text"
                              value={username_set_password}
                            />
                          </InputGroup>
                        </Col>
                      </Row>
                      <Row className="mb-2">
                        <Col md="12">
                          <InputGroup>
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="nc-icon nc-key-25" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              placeholder="ລະຫັດໃໝ່"
                              type="password"
                              value={set_password}
                              onChange={handleChangeSetNewPassword}
                            />
                          </InputGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col className="text-center" md="12">
                          <Button
                            color="danger"
                            outline
                            onClick={handleCloseSetpassword}
                          >
                            <i className="nc-icon nc-simple-remove" /> ຍົກເລີກ
                          </Button>{" "}
                          <Button
                            color="primary"
                            outline
                            onClick={handleSetpasswordtData}
                          >
                            <i className="nc-icon nc-single-copy-04" /> ບັນທຶກ
                          </Button>
                        </Col>
                      </Row>
                    </Form>
                  </Col>
                </Row>
              </Container>
            </div>
          </ReactBSAlert>
        ) : null}
      </>
    );
  }
}

export default SetPassword;
