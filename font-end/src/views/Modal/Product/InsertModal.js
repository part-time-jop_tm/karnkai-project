import React from "react";
// react component used to create sweet alerts
import ReactBSAlert from "react-bootstrap-sweetalert";

// reactstrap components
import {
  Button,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from "reactstrap";

class InsertModal extends React.Component {
  render() {
    const {
      open,
      handleClose,
      handleInsertData,
      handleChangePicturefile,
      handleChangeDescription,
      handleChangeProductName,
      handleChangeProductPrice,
      description,
      productName,
      productPrice,
    } = this.props;
    return (
      <>
        {open === true ? (
          <ReactBSAlert
            style={{ display: "block", marginTop: "100px" }}
            title=""
            onConfirm={handleClose}
            showConfirm={false}
          >
            <div className="section landing-section">
              <Container>
                <Row>
                  <Col className="ml-auto mr-auto" md="8">
                    <h5 className="text-center">ເພີ່ມຂໍ້ມູນຮູບ</h5>
                    <Form className="contact-form">
                      <Row className="mb-2">
                        <Col md="12">
                          <InputGroup>
                            <Input
                              placeholder="picturefile"
                              type="file"
                              accept="image/png, image/jpeg"
                              onChange={handleChangePicturefile}
                            />
                          </InputGroup>
                        </Col>
                      </Row>
                      <Row className="mb-2">
                        <Col md="12">
                          <InputGroup>
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="nc-icon nc-bullet-list-67" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              placeholder="description"
                              type="text"
                              value={description}
                              onChange={handleChangeDescription}
                            />
                          </InputGroup>
                        </Col>
                      </Row>
                      <Row className="mb-2">
                        <Col md="12">
                          <InputGroup>
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="nc-icon nc-box" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              placeholder="ລະຫັດສີນຄ້າ"
                              type="text"
                              value={productName}
                              onChange={handleChangeProductName}
                            />
                          </InputGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col md="12">
                          <InputGroup>
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="nc-icon nc-money-coins" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              placeholder="ລາຄາ"
                              type="number"
                              value={productPrice}
                              onChange={handleChangeProductPrice}
                            />
                          </InputGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col className="text-center" md="12">
                          <Button color="danger" outline onClick={handleClose}>
                            <i className="nc-icon nc-simple-remove" /> ຍົກເລີກ
                          </Button>{" "}
                          <Button
                            color="primary"
                            outline
                            onClick={handleInsertData}
                          >
                            <i className="nc-icon nc-single-copy-04" /> ບັນທຶກ
                          </Button>
                        </Col>
                      </Row>
                    </Form>
                  </Col>
                </Row>
              </Container>
            </div>
          </ReactBSAlert>
        ) : null}
      </>
    );
  }
}

export default InsertModal;
