/*eslint-disable*/
import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";

// meterial ui components
import TablePagination from "@material-ui/core/TablePagination";

// reactstrap components
import {
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
  Table,
  Button,
  Row,
  Col,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Form,
  Input,
} from "reactstrap";

// core api
import {
  getUsers,
  insertUser,
  deleteUser,
  lockUser,
  activeUser,
  setPasswordUser,
} from "../../../stores/requests/user.request";

// core component
import PendingPage from "views/examples/PendingPage";
import ErrorPage from "views/examples/ErrorPage";
import UserInsert from "views/Modal/Users/UserInsert";
import SetPassword from "views/Modal/Users/SetPassword";

class GetUser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      // modal
      openInsertUser: false,
      openSetPassword: false,
      // next page
      page: 0,
      rowsPerPage: 10,
      // get infomation
      info: [],
      // search
      search: [],
      seacrhText: "",
      // insert user
      username: "",
      email: "",
      password: "",
      role: "",
      // set password
      set_password: "",
      id_set_password: "",
      username_set_password: "",
    };
    this.handleChangeUsername = this.handleChangeUsername.bind(this);
    this.handleChangeEmail = this.handleChangeEmail.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
    this.handleChangeRole = this.handleChangeRole.bind(this);
    this.handleInsertData = this.handleInsertData.bind(this);
    this.handleDeleteData = this.handleDeleteData.bind(this);
    this.handleLockData = this.handleLockData.bind(this);
    this.handleActiveData = this.handleActiveData.bind(this);
    this.handleChangeSetNewPassword =
      this.handleChangeSetNewPassword.bind(this);
    this.handleSetpasswordtData = this.handleSetpasswordtData.bind(this);
  }

  // this is comment for handleChange function
  handleChangeSetNewPassword(event) {
    this.setState({
      set_password: event.target.value,
    });
  }

  // this is comment for handleChange function
  handleChangeUsername(event) {
    this.setState({
      username: event.target.value,
    });
  }

  handleChangeEmail(event) {
    this.setState({
      email: event.target.value,
    });
  }

  handleChangePassword(event) {
    this.setState({
      password: event.target.value,
    });
  }

  handleChangeRole(event) {
    this.setState({
      role: event.target.value,
    });
  }

  // function modal open insert user
  HandleOpenInsertUser = () => {
    this.setState({ openInsertUser: true });
  };

  HandleOpenSetpassword = (id, username) => {
    this.setState({ openSetPassword: true });
    this.setState({ id_set_password: id });
    this.setState({ username_set_password: username });
  };

  // function modal close
  handleClose = () => {
    this.setState({ openInsertUser: false });
    this.setState({ username: "" });
    this.setState({ email: "" });
    this.setState({ password: "" });
    this.setState({ role: "" });
  };

  handleCloseSetpassword = () => {
    this.setState({ openSetPassword: false });
    this.setState({ set_password: "" });
    this.setState({ id_set_password: "" });
    this.setState({ username_set_password: "" });
  };

  // overload data function
  overloadData() {
    this.props.dispatch(getUsers());
  }

  // method overload
  componentDidMount() {
    this.overloadData();
  }

  // method overload after
  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({
      info: nextProps.users,
      search: nextProps.users,
    });
  }

  // next page
  handleChangePage = (event, newPage) => {
    this.setState({ page: newPage });
  };

  // select page
  handleChangeRowsPerPage = (event) => {
    this.setState({ rowsPerPage: +event.target.value });
    this.setState({ page: 0 });
  };

  // search
  handleSearch = (event) => {
    this.setState({ seacrhText: event.target.value }, () => {
      this.globalSearch();
    });
  };

  // search data
  globalSearch = () => {
    let { search, seacrhText } = this.state;
    var filterData = search;
    filterData = filterData.filter((value) => {
      return (
        value.email.toString().includes(seacrhText.toLowerCase()) ||
        value.username.toString().includes(seacrhText.toLowerCase()) ||
        value.role.toString().includes(seacrhText.toLowerCase()) ||
        value.status.toString().includes(seacrhText.toLowerCase())
      );
    });
    this.setState({ info: filterData });
  };

  // insert data
  handleInsertData(event) {
    event.preventDefault();
    if (
      !this.state.email ||
      !this.state.username ||
      !this.state.password ||
      !this.state.role
    )
      return alert("ກະລຸນາຕື່ມຂໍ້ມູນໃຫ້ຄົບ");
    if (!confirm("Are you sure?")) return;
    const data = {
      email: this.state.email,
      username: this.state.username,
      password: this.state.password,
      role: this.state.role,
      status: "active",
      userId: localStorage.getItem("userId"),
    };
    this.props.dispatch(insertUser(data));
    this.handleClose();
  }

  // delete data id
  handleDeleteData(id) {
    event.preventDefault();
    if (!confirm("Are you sure?")) return;
    this.props.dispatch(deleteUser(id));
  }

  // lock data id
  handleLockData(id) {
    event.preventDefault();
    if (!confirm("Are you sure?")) return;
    this.props.dispatch(lockUser(id));
  }

  // disabled data id
  handleActiveData(id) {
    event.preventDefault();
    if (!confirm("Are you sure?")) return;
    this.props.dispatch(activeUser(id));
  }

  // set password data id
  handleSetpasswordtData(event) {
    event.preventDefault();
    if (!this.state.set_password) return alert("ກະລຸນາຕື່ມຂໍ້ມູນໃຫ້ຄົບ");
    if (!confirm("Are you sure?")) return;
    const data = {
      password: this.state.set_password,
      userId: localStorage.getItem("userId"),
    };
    this.props.dispatch(setPasswordUser(this.state.id_set_password, data));
    this.handleCloseSetpassword();
  }
  render() {
    const {
      page,
      rowsPerPage,
      info,
      seacrhText,
      username,
      email,
      password,
      set_password,
      username_set_password,
    } = this.state;
    const { error, pending } = this.props;
    // store action
    if (pending) {
      return <PendingPage />;
    } else if (error) {
      return <ErrorPage />;
    } else {
      return (
        <>
          <div>
            <Card>
              <CardBody>
                <CardTitle className="text-left" tag="h5">
                  ການຈັດການຂໍ້ມູນຜູ້ໃຊ້ລະບົບ
                </CardTitle>
                <br />
                <CardSubtitle className="text-muted text-left" tag="h6">
                  <Button
                    color="primary"
                    outline
                    onClick={this.HandleOpenInsertUser}
                  >
                    <i className="nc-icon nc-single-02" /> ເພື່ອຜູ້ໃຊ້
                  </Button>
                  <Form className="contact-form">
                    <Row>
                      <Col md="12">
                        <InputGroup>
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="nc-icon nc-zoom-split" />
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            placeholder="ຄົ້ນຫາ"
                            type="text"
                            value={seacrhText || ""}
                            onChange={this.handleSearch}
                          />
                        </InputGroup>
                      </Col>
                    </Row>
                  </Form>
                </CardSubtitle>
              </CardBody>
              <CardBody>
                <TablePagination
                  rowsPerPageOptions={[10, 25, 50]}
                  component="div"
                  count={info.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  onPageChange={this.handleChangePage}
                  onRowsPerPageChange={this.handleChangeRowsPerPage}
                />
                <Table hover responsive size="" striped>
                  <thead>
                    <tr>
                      <th>ລຳດັບ</th>
                      <th>ຊື່ເຂົ້າໃຊ້ລະບົບ</th>
                      <th>ອີເມວ</th>
                      <th>ລະດັບການນຳໃຊ້</th>
                      <th>ສະຖານະ</th>
                      <th>ອື່ນໆ</th>
                    </tr>
                  </thead>
                  <tbody>
                    {info
                      .slice(
                        page * rowsPerPage,
                        page * rowsPerPage + rowsPerPage
                      )
                      .map((row, index) => {
                        return (
                          <tr key={index}>
                            <th scope="row">{index + 1}</th>
                            <td>{row.username}</td>
                            <td>{row.email}</td>
                            <td
                              className={
                                row.role === "admin"
                                  ? "text-primary"
                                  : "text-warning"
                              }
                            >
                              {row.role}
                            </td>
                            <td
                              className={
                                row.status === "active"
                                  ? "text-success"
                                  : "text-danger"
                              }
                            >
                              {row.status}
                            </td>
                            <td>
                              <Button
                                size="sm"
                                color="danger"
                                outline
                                onClick={(e) => this.handleDeleteData(row.id)}
                              >
                                ລົບ
                              </Button>{" "}
                              <Button
                                size="sm"
                                color="default"
                                outline
                                onClick={(e) =>
                                  this.HandleOpenSetpassword(
                                    row.id,
                                    row.username
                                  )
                                }
                              >
                                ຕັ້ງລະຫັດໃໝ່
                              </Button>{" "}
                              {row.status === "active" ? (
                                <Button
                                  size="sm"
                                  color="primary"
                                  outline
                                  onClick={(e) => this.handleLockData(row.id)}
                                >
                                  ລ໋ອກ
                                </Button>
                              ) : (
                                <Button
                                  size="sm"
                                  color="success"
                                  outline
                                  onClick={(e) => this.handleActiveData(row.id)}
                                >
                                  ປົດລ໋ອກ
                                </Button>
                              )}
                            </td>
                          </tr>
                        );
                      })}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </div>
          {/* <------------------------ Modal ---------------------------> */}
          {/* insert user */}
          <UserInsert
            open={this.state.openInsertUser}
            handleClose={this.handleClose}
            handleInsertData={this.handleInsertData}
            handleChangeUsername={(event) => this.handleChangeUsername(event)}
            handleChangeEmail={(event) => this.handleChangeEmail(event)}
            handleChangePassword={(event) => this.handleChangePassword(event)}
            handleChangeRole={(event) => this.handleChangeRole(event)}
            username={username}
            email={email}
            password={password}
          />
          {/* set password user */}
          <SetPassword
            open={this.state.openSetPassword}
            handleCloseSetpassword={this.handleCloseSetpassword}
            handleSetpasswordtData={this.handleSetpasswordtData}
            handleChangeSetNewPassword={(event) =>
              this.handleChangeSetNewPassword(event)
            }
            set_password={set_password}
            username_set_password={username_set_password}
          />
        </>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  users: state.user.users,
  pending: state.user.pending,
  error: state.user.error,
});

export default compose(connect(mapStateToProps, null))(GetUser);
