/*eslint-disable*/
import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";

// meterial ui components
import TablePagination from "@material-ui/core/TablePagination";

// reactstrap components
import {
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
  Table,
  Button,
  Row,
  Col,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Form,
  Input,
} from "reactstrap";

// core api
import {
  getDatas,
  insertData,
  deleteData,
  disabledData,
  enabledData,
} from "../../../stores/requests/twentyone.request";

// core component
import PendingPage from "views/examples/PendingPage";
import ErrorPage from "views/examples/ErrorPage";
// core modal
import InsertModal from "views/Modal/2021/InsertModal";
import ViewModal from "views/Modal/2021/ViewModal";

class GetTwentyone extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      // modal
      openInsert: false,
      openViewImage: false,
      // next page
      page: 0,
      rowsPerPage: 10,
      // get infomation
      info: [],
      // search
      search: [],
      seacrhText: "",
      // insert download
      picturefile: "",
      description: "",
      viewimage: "",
    };
    this.handleChangePicturefile = this.handleChangePicturefile.bind(this);
    this.handleChangeDescription = this.handleChangeDescription.bind(this);
    this.handleInsertData = this.handleInsertData.bind(this);
  }

  // this is comment for handleChange function
  handleChangePicturefile(event) {
    let picturefile = event.target.files[0];
    this.setState({ picturefile: picturefile });
  }

  handleChangeDescription(event) {
    this.setState({
      description: event.target.value,
    });
  }

  // function modal open insert user
  HandleOpenInsert = () => {
    this.setState({ openInsert: true });
  };

  HandleOpenViewImage = (image) => {
    this.setState({ openViewImage: true });
    this.setState({ viewimage: image });
  };

  // function modal close
  handleClose = () => {
    this.setState({ openInsert: false });
    this.setState({ picturefile: "" });
    this.setState({ description: "" });
  };

  handleCloseViewImage = () => {
    this.setState({ openViewImage: false });
  };

  // overload data function
  overloadData() {
    this.props.dispatch(getDatas());
  }

  // method overload
  componentDidMount() {
    this.overloadData();
  }

  // method overload after
  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({
      info: nextProps.twentyones,
      search: nextProps.twentyones,
    });
  }

  // next page
  handleChangePage = (event, newPage) => {
    this.setState({ page: newPage });
  };

  // select page
  handleChangeRowsPerPage = (event) => {
    this.setState({ rowsPerPage: +event.target.value });
    this.setState({ page: 0 });
  };

  // search
  handleSearch = (event) => {
    this.setState({ seacrhText: event.target.value }, () => {
      this.globalSearch();
    });
  };

  // search data
  globalSearch = () => {
    let { search, seacrhText } = this.state;
    var filterData = search;
    filterData = filterData.filter((value) => {
      return (
        value.description
          .toString()
          .toLowerCase()
          .indexOf(seacrhText.toLowerCase()) > -1 ||
        value.status
          .toString()
          .toLowerCase()
          .indexOf(seacrhText.toLowerCase()) > -1
      );
    });
    this.setState({ info: filterData });
  };

  // insert data
  handleInsertData(event) {
    event.preventDefault();
    if (!this.state.picturefile || !this.state.description)
      return alert("ກະລຸນາຕື່ມຂໍ້ມູນໃຫ້ຄົບ");
    if (!confirm("Are you sure?")) return;
    let formdata = new FormData();
    formdata.append("picturefile", this.state.picturefile);
    formdata.append("description", this.state.description);
    formdata.append("userId", localStorage.getItem("userId"));
    this.props.dispatch(insertData(formdata));
    this.handleClose();
  }

  // delete data id
  handleDeleteData(id) {
    event.preventDefault();
    if (!confirm("Are you sure?")) return;
    this.props.dispatch(deleteData(id));
  }

  // disabled data id
  handleDisabledData(id) {
    event.preventDefault();
    if (!confirm("Are you sure?")) return;
    this.props.dispatch(disabledData(id));
  }

  // enabled data id
  handleEnabledData(id) {
    event.preventDefault();
    if (!confirm("Are you sure?")) return;
    this.props.dispatch(enabledData(id));
  }

  render() {
    const {
      page,
      rowsPerPage,
      info,
      seacrhText,
      // picturefile,
      description,
      viewimage,
    } = this.state;
    const { error, pending } = this.props;
    // store action
    if (pending) {
      return <PendingPage />;
    } else if (error) {
      return <ErrorPage />;
    } else {
      return (
        <>
          <div>
            <Card>
              <CardBody>
                <CardTitle className="text-left" tag="h5">
                  ການຈັດການຂໍ້ມູນຮູບປີ 2021
                </CardTitle>
                <br />
                <CardSubtitle className=" text-muted text-left" tag="h6">
                  <Button
                    color="primary"
                    outline
                    onClick={this.HandleOpenInsert}
                  >
                    <i className="nc-icon nc-tap-01" /> ເພື່ອຂໍ້ມູນ
                  </Button>
                  <Form className="contact-form">
                    <Row>
                      <Col md="12">
                        <InputGroup>
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="nc-icon nc-zoom-split" />
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            placeholder="ຄົ້ນຫາ"
                            type="text"
                            value={seacrhText || ""}
                            onChange={(event) => this.handleSearch(event)}
                          />
                        </InputGroup>
                      </Col>
                    </Row>
                  </Form>
                </CardSubtitle>
              </CardBody>
              <CardBody>
                <TablePagination
                  rowsPerPageOptions={[10, 25, 50]}
                  component="div"
                  count={info.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  onPageChange={this.handleChangePage}
                  onRowsPerPageChange={this.handleChangeRowsPerPage}
                />
                <Table hover responsive size="" striped>
                  <thead>
                    <tr>
                      <th>ລຳດັບ</th>
                      <th>ລາຍລະອຽດ</th>
                      <th>ສະຖານະ</th>
                      <th>ອື່ນໆ</th>
                    </tr>
                  </thead>
                  <tbody>
                    {info
                      .slice(
                        page * rowsPerPage,
                        page * rowsPerPage + rowsPerPage
                      )
                      .map((row, index) => {
                        return (
                          <tr key={index}>
                            <th scope="row">{index + 1}</th>
                            <td>{row.description}</td>
                            <td
                              className={
                                row.status === 1
                                  ? "text-success"
                                  : "text-danger"
                              }
                            >
                              {row.status === 1 ? "ໂຄສະນາ" : "ປິດໂຄສະນາ"}
                            </td>
                            <td>
                              <Button
                                size="sm"
                                color="danger"
                                outline
                                onClick={(e) => this.handleDeleteData(row.id)}
                              >
                                ລົບ
                              </Button>{" "}
                              {row.status === 1 ? (
                                <Button
                                  size="sm"
                                  color="default"
                                  outline
                                  onClick={(e) =>
                                    this.handleDisabledData(row.id)
                                  }
                                >
                                  ປິດການນຳໃຊ້
                                </Button>
                              ) : (
                                <Button
                                  size="sm"
                                  color="success"
                                  outline
                                  onClick={(e) =>
                                    this.handleEnabledData(row.id)
                                  }
                                >
                                  ເປີດການນຳໃຊ້
                                </Button>
                              )}{" "}
                              <Button
                                size="sm"
                                color="warning"
                                outline
                                onClick={(e) =>
                                  this.HandleOpenViewImage(row.image)
                                }
                              >
                                ເບີ່ງຮູບ
                              </Button>
                            </td>
                          </tr>
                        );
                      })}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </div>
          {/* <------------------------ Modal ---------------------------> */}
          {/* insert download */}
          <InsertModal
            open={this.state.openInsert}
            handleClose={this.handleClose}
            handleInsertData={this.handleInsertData}
            handleChangePicturefile={(event) =>
              this.handleChangePicturefile(event)
            }
            handleChangeDescription={(event) =>
              this.handleChangeDescription(event)
            }
            // picturefile={picturefile}
            description={description}
          />
          {/* view image */}
          <ViewModal
            open={this.state.openViewImage}
            handleCloseViewImage={this.handleCloseViewImage}
            image={viewimage}
          />
        </>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  twentyones: state.twentyone.twentyones,
  pending: state.twentyone.pending,
  error: state.twentyone.error,
});

export default compose(connect(mapStateToProps, null))(GetTwentyone);
