/*eslint-disable*/
import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";

// meterial ui components
import TablePagination from "@material-ui/core/TablePagination";

// reactstrap components
import {
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
  Table,
  Button,
  Row,
  Col,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Form,
  Input,
} from "reactstrap";

// core api
import { getDatas, acceptData } from "../../../stores/requests/package.request";

// core component
import PendingPage from "views/examples/PendingPage";
import ErrorPage from "views/examples/ErrorPage";

class Getpackage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      // modal
      openInsert: false,
      openViewImage: false,
      openChangeProfile: false,
      // next page
      page: 0,
      rowsPerPage: 10,
      // get infomation
      info: [],
      // search
      search: [],
      seacrhText: "",
    };
  }

  // overload data function
  overloadData() {
    this.props.dispatch(getDatas());
  }

  // method overload
  componentDidMount() {
    this.overloadData();
  }

  // method overload after
  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({
      info: nextProps.packages,
      search: nextProps.packages,
    });
  }

  // next page
  handleChangePage = (event, newPage) => {
    this.setState({ page: newPage });
  };

  // select page
  handleChangeRowsPerPage = (event) => {
    this.setState({ rowsPerPage: +event.target.value });
    this.setState({ page: 0 });
  };

  // search
  handleSearch = (event) => {
    this.setState({ seacrhText: event.target.value }, () => {
      this.globalSearch();
    });
  };

  // search data
  globalSearch = () => {
    let { search, seacrhText } = this.state;
    var filterData = search;
    filterData = filterData.filter((value) => {
      return (
        value.package
          .toString()
          .toLowerCase()
          .indexOf(seacrhText.toLowerCase()) > -1 ||
        value.customerEmail
          .toString()
          .toLowerCase()
          .indexOf(seacrhText.toLowerCase()) > -1
      );
    });
    this.setState({ info: filterData });
  };

  // Accept data id
  handleAcceptData(id) {
    event.preventDefault();
    if (!confirm("Are you sure?")) return;
    this.props.dispatch(acceptData(id));
  }

  render() {
    const { page, rowsPerPage, info, seacrhText } = this.state;
    const { error, pending } = this.props;
    // store action
    if (pending) {
      return <PendingPage />;
    } else if (error) {
      return <ErrorPage />;
    } else {
      return (
        <>
          <div>
            <Card>
              <CardBody>
                <CardTitle className="text-left" tag="h5">
                  ການຈັດການຂໍ້ມູນລູກຄ້າ
                </CardTitle>
                <br />
                <CardSubtitle className=" text-muted text-left" tag="h6"> 
                  <Form className="contact-form">
                    <Row>
                      <Col md="12">
                        <InputGroup>
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="nc-icon nc-zoom-split" />
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            placeholder="ຄົ້ນຫາ"
                            type="text"
                            value={seacrhText || ""}
                            onChange={(event) => this.handleSearch(event)}
                          />
                        </InputGroup>
                      </Col>
                    </Row>
                  </Form>
                </CardSubtitle>
              </CardBody>
              <CardBody>
                <TablePagination
                  rowsPerPageOptions={[10, 25, 50]}
                  component="div"
                  count={info.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  onPageChange={this.handleChangePage}
                  onRowsPerPageChange={this.handleChangeRowsPerPage}
                />
                <Table hover responsive size="" striped>
                  <thead>
                    <tr>
                      <th>ລຳດັບ</th>
                      <th>ເພັກເກັດ</th>
                      <th>ຊື່ລູກຄ້າ</th>
                      <th>ອີເມວ</th>
                      <th>ເບີໂທລະສັບ</th>
                      <th>ຂໍ້ຄວາມ</th>
                      <th>ສະຖານະ</th>
                      <th>ອື່ນໆ</th>
                    </tr>
                  </thead>
                  <tbody>
                    {info
                      .slice(
                        page * rowsPerPage,
                        page * rowsPerPage + rowsPerPage
                      )
                      .map((row, index) => {
                        return (
                          <tr key={index}>
                            <th scope="row">{index + 1}</th>
                            <td>{row.package}</td>
                            <td>{row.customerName}</td>
                            <td>{row.customerEmail}</td>
                            <td>{row.customerTel}</td>
                            <td>{row.customerText}</td>
                            <td
                              className={
                                row.status === "waiting"
                                  ? "text-warning"
                                  : "text-success"
                              }
                            >
                              {row.status}
                            </td>
                            <td>
                              {row.status === "waiting" ? (
                                <Button
                                  size="sm"
                                  color="success"
                                  outline
                                  onClick={(e) => this.handleAcceptData(row.id)}
                                >
                                  accept
                                </Button>
                              ) : (
                                <i className="nc-icon nc-check-2" />
                              )}
                            </td>
                          </tr>
                        );
                      })}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </div>
        </>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  packages: state.packageData.packages,
  pending: state.packageData.pending,
  error: state.packageData.error,
});

export default compose(connect(mapStateToProps, null))(Getpackage);
