/*eslint-disable*/
import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";

// reactstrap components
import {
  Button,
  Card,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  Carousel,
  CarouselItem,
  CarouselIndicators,
  CarouselCaption,
} from "reactstrap";

// core components
import PendingPage from "views/examples/PendingPage";
import ErrorPage from "views/examples/ErrorPage";
import DemoFooter from "components/Footers/DemoFooter";

// core api
import {
  getImageDatas,
  getAllDatas,
} from "../../stores/requests/calendar.request";
import { insertData } from "../../stores/requests/package.request";
import { sendMail } from "../../stores/requests/mail.request"; 

function SectionCalendarComponent(props) {
  // props data
  const { data, allcalendars, handleSubmitData, handleSendmail } = props;

  const [activeIndex, setActiveIndex] = React.useState(0);
  const [animating, setAnimating] = React.useState(false);

  const onExiting = () => {
    setAnimating(true);
  };
  const onExited = () => {
    setAnimating(false);
  };
  const next = () => {
    if (animating) return;
    const nextIndex =
      activeIndex === allcalendars.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  };
  const previous = () => {
    if (animating) return;
    const nextIndex =
      activeIndex === 0 ? allcalendars.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  };
  const goToIndex = (newIndex) => {
    if (animating) return;
    setActiveIndex(newIndex);
  };

  // set value
  const [openCustomerEmail, setOpenCustomerEmail] = React.useState("");
  const [openCustomerText, setOpenCustomerText] = React.useState("");

  function handleChangeCustomerEmail(event) {
    setOpenCustomerEmail(event.target.value);
  }

  function handleChangeCustomerText(event) {
    setOpenCustomerText(event.target.value);
  }

  // submit
  function SubmitData(event) {
    event.preventDefault();
    if (!openCustomerEmail || !openCustomerText)
      return alert("ກະລຸນາຕື່ມຂໍ້ມູນໃຫ້ຄົບ");
    if (!confirm("Are you sure?")) return;
    const data = {
      package: "package: calendar",
      customerName: "NULL",
      customerEmail: openCustomerEmail,
      customerTel: "NULL",
      customerText: openCustomerText,
      status: "waiting",
    };
    handleSubmitData(data);
    // send mail
    const sendmailCust = {
      from: "karnkai.2022@gmail.com",
      to: openCustomerEmail,
      subject: "From karnkai",
      title: `Dear ${openCustomerEmail} <br /> you submit data successfully.<br />package: calendar, ${openCustomerText} <br /><br /><br />`,
    };
    const sendmailAdmin = {
      from: "karnkai.2022@gmail.com",
      to: "kidkarnkai@gmail.com",
      subject: "notification mail from customer",
      title: `Dear karnkai <br /> the customer submit data for you successfully.<br />Email:  ${openCustomerEmail}<br />package: calendar, ${openCustomerText} <br /><br /><br />`,
    };
    handleSendmail(sendmailCust);
    handleSendmail(sendmailAdmin);
  }

  return (
    <>
      <div className="section section-dark">
        <Container className="text-center">
          <div className="title text-center">
            {" "}
            <u>
              <a href="/index">
                <h5 className="text-customer">- ກັບຄືນສູ່ໜ້າຫຼັກ -</h5>
              </a>
            </u>
            <h3 className="text-customer">Calendar</h3>
          </div>
          <Row>
            <Col className="ml-auto mr-auto" md="8">
              <Card className="page-carousel">
                <Carousel
                  activeIndex={activeIndex}
                  next={next}
                  previous={previous}
                >
                  <CarouselIndicators
                    items={allcalendars}
                    activeIndex={activeIndex}
                    onClickHandler={goToIndex}
                  />
                  {allcalendars.map((item) => {
                    return (
                      <CarouselItem
                        onExiting={onExiting}
                        onExited={onExited}
                        key={item.id}
                      >
                        <img
                          src={`https://api.karnkaiart.com/api/upload/calendar/${item.image}`}
                          alt={item.image}
                        />
                        <CarouselCaption
                          captionText={item.description}
                          captionHeader=""
                        />
                      </CarouselItem>
                    );
                  })}
                  <a
                    className="left carousel-control carousel-control-prev"
                    data-slide="prev"
                    href="#pablo"
                    onClick={(e) => {
                      e.preventDefault();
                      previous();
                    }}
                    role="button"
                  >
                    <span className="fa fa-angle-left" />
                    <span className="sr-only">Previous</span>
                  </a>
                  <a
                    className="right carousel-control carousel-control-next"
                    data-slide="next"
                    href="#pablo"
                    onClick={(e) => {
                      e.preventDefault();
                      next();
                    }}
                    role="button"
                  >
                    <span className="fa fa-angle-right" />
                    <span className="sr-only">Next</span>
                  </a>
                </Carousel>
              </Card>
            </Col>
          </Row>
          <div id="images">
            <Row className="mb-5">
              {" "}
              {data.map((item) => {
                return (
                  <Col key={item.id} md="4" sm="4">
                    <Card>
                      <img
                        alt={item.description}
                        className="img-thumbnail img-responsive"
                        src={`https://api.karnkaiart.com/api/upload/calendar/${item.image}`}
                      />
                      <p className="card-description mt-2 mb-2">
                        {item.description}
                      </p>
                    </Card>
                  </Col>
                );
              })}
            </Row>
          </div>
          <Row>
            <Col>
              <h4 className="text-customer text-center mb-2">ຕິດຕໍ່ສັ່ງຈອງ</h4>
              <Form className="text-left">
                <div>
                  <label>ອີເມວ</label>
                  <InputGroup>
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="nc-icon nc-email-85" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      placeholder="ອີເມວ"
                      type="email"
                      value={openCustomerEmail}
                      onChange={handleChangeCustomerEmail}
                    />
                  </InputGroup>
                </div>
                <div className="mt-3">
                  <label>ຂໍ້ຄວາມ</label>
                  <Input
                    placeholder="ຂໍ້ຄວາມ"
                    type="textarea"
                    value={openCustomerText}
                    onChange={handleChangeCustomerText}
                  />
                </div>
                <div className="mt-3">
                  <Button outline color="success" onClick={SubmitData}>
                    <i className="nc-icon nc-send mr-2" />
                    ສົ່ງ
                  </Button>
                </div>
              </Form>
            </Col>
          </Row>
        </Container>
      </div>
      <DemoFooter />
    </>
  );
}

class SectionCalendar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.handleSubmitData = this.handleSubmitData.bind(this);
    this.handleSendmail = this.handleSendmail.bind(this);
  }

  // submit customer data
  handleSubmitData(data) {
    this.props.dispatch(insertData(data));
  }

  // submit send mail
  handleSendmail(data) {
    this.props.dispatch(sendMail(data));
  }

  // overload data
  componentDidMount = () => {
    this.props.dispatch(getImageDatas());
    this.props.dispatch(getAllDatas());
  };

  render() {
    const { error, pending, imagescalendars, allcalendars } = this.props;
    if (pending) {
      return <PendingPage />;
    } else if (error) {
      return <ErrorPage />;
    } else {
      return (
        <>
          <SectionCalendarComponent
            data={imagescalendars}
            allcalendars={allcalendars}
            handleSubmitData={this.handleSubmitData}
            handleSendmail={this.handleSendmail}
          />
        </>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  imagescalendars: state.calendar.imagescalendars,
  allcalendars: state.calendar.allcalendars,
  pending: state.calendar.pending,
  error: state.calendar.error,
});

export default compose(connect(mapStateToProps, null))(SectionCalendar);
