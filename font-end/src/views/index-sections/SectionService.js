/*eslint-disable*/
import React from "react";

// reactstrap components
import { Row, Col, Container } from "reactstrap";

// core components
import SectionWorkShop from "./SectionWorkShop";
import SectionLogotypeDesign from "./SectionLogotypeDesign";

function SectionService() {
  return (
    <>
      <div className="section section-dark">
        <Container> 
          <Row>
            <Col>
              <SectionWorkShop />
            </Col>
          </Row>
          <Row>
            <Col>
              <SectionLogotypeDesign />
            </Col>
          </Row> 
        </Container>
      </div>
    </>
  );
}

export default SectionService;
