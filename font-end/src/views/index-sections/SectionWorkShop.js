/*eslint-disable*/
import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";

// reactstrap components
import {
  Card,
  Carousel,
  CarouselCaption,
  CarouselIndicators,
  CarouselItem,
  Col,
  Container,
  Row,
} from "reactstrap";

// core components
import PendingPage from "views/examples/PendingPage";
import ErrorPage from "views/examples/ErrorPage";
import DemoFooter from "components/Footers/DemoFooter";

// core api
import { getAllDatas } from "../../stores/requests/workshop.request";
import { getImageDatas } from "../../stores/requests/workshopprofile.request";

function SectionWorkShopComponent(props) {
  // props data
  const { imagesworkshopprofile, data } = props;

  const [activeIndex, setActiveIndex] = React.useState(0);
  const [animating, setAnimating] = React.useState(false);

  const onExiting = () => {
    setAnimating(true);
  };
  const onExited = () => {
    setAnimating(false);
  };
  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === data.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  };
  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? data.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  };
  const goToIndex = (newIndex) => {
    if (animating) return;
    setActiveIndex(newIndex);
  };

  return (
    <>
      <div className="section section-dark">
        <Container className="text-center">
          <div className="title">
            <h3 className="text-customer">Work Shop</h3>
          </div>
          <Row className="mb-5">
            <Col className="ml-auto mr-auto" md="10">
              <Card className="page-carousel">
                <Carousel
                  activeIndex={activeIndex}
                  next={next}
                  previous={previous}
                >
                  <CarouselIndicators
                    items={data}
                    activeIndex={activeIndex}
                    onClickHandler={goToIndex}
                  />
                  {data.map((item) => {
                    return (
                      <CarouselItem
                        onExiting={onExiting}
                        onExited={onExited}
                        key={item.id}
                      >
                        <img
                          src={`https://api.karnkaiart.com/api/upload/workshop/${item.image}`}
                          alt={item.image}
                        />
                        <CarouselCaption
                          captionText={item.description}
                          captionHeader=""
                        />
                      </CarouselItem>
                    );
                  })}
                  <a
                    className="left carousel-control carousel-control-prev"
                    data-slide="prev"
                    href="#pablo"
                    onClick={(e) => {
                      e.preventDefault();
                      previous();
                    }}
                    role="button"
                  >
                    <span className="fa fa-angle-left" />
                    <span className="sr-only">Previous</span>
                  </a>
                  <a
                    className="right carousel-control carousel-control-next"
                    data-slide="next"
                    href="#pablo"
                    onClick={(e) => {
                      e.preventDefault();
                      next();
                    }}
                    role="button"
                  >
                    <span className="fa fa-angle-right" />
                    <span className="sr-only">Next</span>
                  </a>
                </Carousel>
              </Card>
            </Col>
          </Row>
          <Row>
            {imagesworkshopprofile.map((item) => {
              return (
                <Col key={item.id} sm="6" className="mt-2">
                  <div>
                    <img
                      alt="..."
                      src={`https://api.karnkaiart.com/api/upload/workshopprofile/${item.image}`}
                      width="100%"
                    />
                  </div>
                </Col>
              );
            })}
            <Col sm="6" className="text-left text-customer">
              <h5>ລາຍລະອຽດ</h5>
              <br></br>
              <h6>
                ( Calligraphy Workshop with Karnkai ) ຫົວຂໍ້ “ການຂຽນ ກັບ ການໄກ”
                ຊຸດທີ 3.{" "}
              </h6>
              <p>
                ລາຍລະອຽດ - ຮຽນຂຽນອັກສອນບັນຈົງ ຫຼື Calligraphy ນຳໃຊ້ປາກກາ
                PentelTouch ແລະ Apple Pencil<br></br>- ຮຽນຮູ້
                ຄວາມຮູ້ພື້ນຖານກ່ຽວກັບ Calligraphy. <br></br>- ຮຽນຂຽນ ແລະ ອອກແບບ
                ອັກສອນລາວບັນຈົງເປັນຫຼັກ 70% ແລະ ອັກສອນອັງກິດ30% (Modern
                Calligraphy). <br></br>- ຮຽນອອກແບບ ໂຕອັກສອນ Lettering
                ເພື່ອປະຍຸກເຮັດໂລໂກLOGOTYPE. <br></br>- ສາທິດ
                ອຸປະກອນທີ່ໃຊ້ໃນການຂຽນCalligraphy ຮູບແບບຕ່າງໆ.<br></br>-
                ແບ່ງປັນປະສົບການ ເບື້ອງຫຼັງ ຂັ້ນຕອນ ແລະ ເທັກໃນໃນການຂຽນອັກສອນ.{" "}
                <br></br>- ສາທິດການເພີ່ມeffectໃຫ້ໂຕອັກສອນ ເພື່ອເພີ່ມມິຕິ ແລະ
                ສ້າງຄວາມໜ້າສົນໃຈໃຫ້ຜົນງານ. <br></br>- ຮັບແຮງບັນດານໃຈໃນການຄິດ ແລະ
                ງານອອກແບບທີ່ມີຕົວອັກສອນເປັນອົງປະກອບຫຼັກ.<br></br>-
                ຮັບຂອງທີ່ລະນຶກ.
                <br></br> <br></br>
                ບັນຍາຍ ແລະ ນຳພາworkshop ໂດຍ: ໄກຍະສິດ ສິຣິວົງສາ (ການໄກ) <br></br>
                ຊົມຜົນງານບາງສ່ວນ: <br></br>
                Instagram: https://www.instagram.com/kidkarnkai/ <br></br>{" "}
                <br></br>| ເໝາະກັບໃຜ | <br></br>
                ເໝາະກັບທຸກທ່ານທີ່ສົນໃຈກ່ຽວກັບສີລະປະ ໂດຍສະເພາະປະເພດໂຕອັກສອນ
                ທັງຜູ້ທີ່ມີພື້ນຖານ ຫຼື ບໍ່ທັນມີພື້ນຖານມາກ່ອນ
                ກໍສາມາດເຂົ້າຮ່ວມຮຽນໄດ້.
                <br></br> <br></br>| ອຸປະກອນໃນການຮຽນ | <br></br>-
                ທາງຫ້ອງຮຽນກະກຽມໃຫ້ແລ້ວ. ຫຼື ຖ້າທ່ານມີ iPad ແລະ Pencil ຄວນຖືມານຳ
                ເພື່ອຕິດຕັ້ງເອົາຊຸດBrushສຳລັບເອົາກັບໄປຊ້ອມຂຽນຢູ່ເຮືອນຫຼັງຈາກຮຽນຈົບ.
                <br></br> <br></br>| ຄ່າຮຽນ | <br></br>+ ບຸກຄົນທົ່ວໄປ 600,000
                ກີບ/ທ່ານ. + ຜູ້ທີ່ເຄີຍມາຮຽນໃນWorkshop ຊຸດຜ່ານມາ
                ເພື່ອຮ່ວມຮຽນທວນຄືນ 150,000 ກີບ.
                <br></br> <br></br>| ໄລຍະເວລາຮຽນ | <br></br>
                ຮຽນເຕັມວັນ ໃນວັນເສົາ ທີ 29/8/2020: 9:00 - 16:00 (ລວມອາຫານທ່ຽງ)
                ແລະ ຕອນເຊົ້າຂອງວັນອາທິດ ທີ 30/8/2020: 8:30 - 11:00. (ໃຊ້ເວລາລວມ
                1ມື້ ກັບ ເຄິ່ງ)
                <br></br> <br></br>| ການລົງທະບຽນ ແລະ ສອບຖາມຂໍ້ມູນຕ່າງໆ |{" "}
                <br></br>
                ລົງທະບຽນ, ຊຳລະຄ່າຮຽນ ແລະ ເລືອກເມນູອາຫານທ່ຽງໄວ້ລ່ວງໜ້າ. ທາງ
                Whatsapp +856 20 2999 3255
                <br></br> <br></br>| ສະຖານທີ່ | <br></br>
                ເຮືອນຮັກອັກສອນ (ຫ້ອງເຮັດວຽກຂອງວິທະຍາກອນເອງ) ຮ່ອມ8 ຮ່ອມກົງກັນຂ້າມ
                ຊ້າຍສວຍກັບໂຮງແຮມອິນແປງ, ບ້ານດອນກອຍ, ເມືອງສີສັດຕະນາກ,
                ນະຄອນຫຼວງວຽງຈັນ. (ແຜນທີ່ລະອຽດ ແມ່ນຈະສົ່ງໃຫ້ທາງ whatsapp)
                <br></br> <br></br>
                ຂອບໃຈ ແລະ
                ຍິນດີຕ້ອນຮັບຜູ້ທີ່ຮັກການຮຽນຮູ້ເລື່ອງອອກແບບອັກສອນທຸກທ່ານ.
              </p>
            </Col>
          </Row>
        </Container>
      </div>
      <DemoFooter />
    </>
  );
}

class SectionWorkShop extends React.Component {
  // overload data
  componentDidMount = () => {
    this.props.dispatch(getAllDatas());
    this.props.dispatch(getImageDatas());
  };

  render() {
    const { error, pending, Allworkshops, imagesworkshopprofile } = this.props;
    if (pending) {
      return <PendingPage />;
    } else if (error) {
      return <ErrorPage />;
    } else {
      return (
        <>
          <SectionWorkShopComponent
            data={Allworkshops}
            imagesworkshopprofile={imagesworkshopprofile}
          />
        </>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  Allworkshops: state.workshop.Allworkshops,
  imagesworkshopprofile: state.workshopprof.imagesworkshopprofile,
  pending: state.workshop.pending,
  error: state.workshop.error,
});

export default compose(connect(mapStateToProps, null))(SectionWorkShop);
