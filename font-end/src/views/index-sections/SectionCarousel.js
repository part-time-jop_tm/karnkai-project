/*eslint-disable*/
import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
// reactstrap components
import {
  Card,
  Container,
  Row,
  Col,
  Carousel,
  CarouselItem,
  CarouselIndicators,
  CarouselCaption,
} from "reactstrap";
// core components
// search data
import PendingPage from "../examples/PendingPage";
import ErrorPage from "../examples/ErrorPage"; 
// core api
import { getAllAdverses } from "../../stores/requests/adverse.request";

const CarouselComponent = (props) => {
  // props data
  const { Alladverses } = props;

  const [activeIndex, setActiveIndex] = React.useState(0);
  const [animating, setAnimating] = React.useState(false);

  const onExiting = () => {
    setAnimating(true);
  };
  const onExited = () => {
    setAnimating(false);
  };
  const next = () => {
    if (animating) return;
    const nextIndex =
      activeIndex === Alladverses.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  };
  const previous = () => {
    if (animating) return;
    const nextIndex =
      activeIndex === 0 ? Alladverses.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  };
  const goToIndex = (newIndex) => {
    if (animating) return;
    setActiveIndex(newIndex);
  };

  return (
    <>
      <div className="section pt-o section-dark" id="carousel">
        <Container>
          {/* <div className="title">
            <h4 className="text-customer">ໂຄສະນາຕ່າງໆ</h4>
          </div> */}
          <Row>
            <Col className="ml-auto mr-auto" md="12">
              <Card className="page-carousel">
                <Carousel
                  activeIndex={activeIndex}
                  next={next}
                  previous={previous}
                >
                  <CarouselIndicators
                    items={Alladverses}
                    activeIndex={activeIndex}
                    onClickHandler={goToIndex}
                  />
                  {Alladverses.map((item) => {
                    return (
                      <CarouselItem
                        onExiting={onExiting}
                        onExited={onExited}
                        key={item.id}
                      >
                        <img
                          src={`https://api.karnkaiart.com/api/upload/adverse/${item.image}`}
                          alt={item.image}
                        />
                        <CarouselCaption 
                          captionText={item.description}
                          captionHeader=""
                        />
                      </CarouselItem>
                    );
                  })}
                  <a
                    className="left carousel-control carousel-control-prev"
                    data-slide="prev"
                    href="#pablo"
                    onClick={(e) => {
                      e.preventDefault();
                      previous();
                    }}
                    role="button"
                  >
                    <span className="fa fa-angle-left" />
                    <span className="sr-only">Previous</span>
                  </a>
                  <a
                    className="right carousel-control carousel-control-next"
                    data-slide="next"
                    href="#pablo"
                    onClick={(e) => {
                      e.preventDefault();
                      next();
                    }}
                    role="button"
                  >
                    <span className="fa fa-angle-right" />
                    <span className="sr-only">Next</span>
                  </a>
                </Carousel>
              </Card>
            </Col>
          </Row> 
        </Container>
      </div>{" "}
    </>
  );
};

class SectionCarousel extends React.Component {
  // overload data
  componentDidMount = () => {
    this.props.dispatch(getAllAdverses());
  };

  render() {
    const { error, pending, Alladverses } = this.props;
    if (pending) {
      return <PendingPage />;
    } else if (error) {
      return <ErrorPage />;
    } else {
      return (
        <>
          <CarouselComponent Alladverses={Alladverses} />
        </>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  Alladverses: state.adverse.Alladverses,
  pending: state.adverse.pending,
  error: state.adverse.error,
});

export default compose(connect(mapStateToProps, null))(SectionCarousel);
