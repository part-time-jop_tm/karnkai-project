/*eslint-disable*/
import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";

// reactstrap components
import {
  Button,
  Card,
  CardBody,
  CardLink,
  CardSubtitle,
  CardText,
  CardTitle,
  Carousel,
  CarouselCaption,
  CarouselIndicators,
  CarouselItem,
  Col,
  Collapse,
  Container,
  Row,
  UncontrolledCollapse,
} from "reactstrap";

// core components
import PendingPage from "views/examples/PendingPage";
import ErrorPage from "views/examples/ErrorPage";

// core api
import { getAllDownloads } from "../../stores/requests/download.request";

function SectionDownloadComponent(props) {
  // props data
  const { data } = props;

  const [activeIndex, setActiveIndex] = React.useState(0);
  const [animating, setAnimating] = React.useState(false);
  const [openIcon, setOpenIcon] = React.useState(false);

  const onExiting = () => {
    setAnimating(true);
  };
  const onExited = () => {
    setAnimating(false);
  };
  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === data.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  };
  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? data.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  };
  const goToIndex = (newIndex) => {
    if (animating) return;
    setActiveIndex(newIndex);
  };

  return (
    <>
      <div className="section section-dark">
        <Container className="text-center">
          <div className="title">
            <h3 className="text-customer">Free vector</h3>
          </div>
          <Row>
            <Col className="ml-auto mr-auto" md="8">
              <Card className="page-carousel">
                <Carousel
                  activeIndex={activeIndex}
                  next={next}
                  previous={previous}
                >
                  <CarouselIndicators
                    items={data}
                    activeIndex={activeIndex}
                    onClickHandler={goToIndex}
                  />
                  {data.map((item) => {
                    return (
                      <CarouselItem
                        onExiting={onExiting}
                        onExited={onExited}
                        key={item.id}
                      >
                        <img
                          src={`https://api.karnkaiart.com/api/upload/download/${item.image}`}
                          alt={item.image}
                        />
                        <CarouselCaption
                          captionText={item.description}
                          captionHeader=""
                        />
                      </CarouselItem>
                    );
                  })}
                  <a
                    className="left carousel-control carousel-control-prev"
                    data-slide="prev"
                    href="#pablo"
                    onClick={(e) => {
                      e.preventDefault();
                      previous();
                    }}
                    role="button"
                  >
                    <span className="fa fa-angle-left" />
                    <span className="sr-only">Previous</span>
                  </a>
                  <a
                    className="right carousel-control carousel-control-next"
                    data-slide="next"
                    href="#pablo"
                    onClick={(e) => {
                      e.preventDefault();
                      next();
                    }}
                    role="button"
                  >
                    <span className="fa fa-angle-right" />
                    <span className="sr-only">Next</span>
                  </a>
                </Carousel>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col>
              <div>
                <a
                  id="toggler"
                  className="btn-link ml-1"
                  style={{ color: "#7e7669" }}
                  type="button"
                  onClick={function noRefCheck() {
                    if (openIcon === true) {
                      setOpenIcon(false);
                    } else {
                      setOpenIcon(true);
                    }
                  }}
                  style={
                    openIcon === true
                      ? { color: "chocolate" }
                      : { color: "#baaa99" }
                  }
                >
                  <h5>
                    {openIcon === true ? (
                      <i className="nc-icon nc-minimal-up mr-2" />
                    ) : (
                      <i className="nc-icon nc-minimal-down mr-2" />
                    )}
                    See more
                  </h5>
                </a>
                <UncontrolledCollapse toggler="#toggler">
                  <Card>
                    <CardBody>
                      <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Nesciunt magni, voluptas debitis similique porro a
                        molestias consequuntur earum odio officiis natus, amet
                        hic, iste sed dignissimos esse fuga! Minus, alias.
                      </p>
                    </CardBody>
                  </Card>
                </UncontrolledCollapse>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

class SectionDownload extends React.Component {
  // overload data
  componentDidMount = () => {
    this.props.dispatch(getAllDownloads());
  };

  render() {
    const { error, pending, Alldownloads } = this.props;
    if (pending) {
      return <PendingPage />;
    } else if (error) {
      return <ErrorPage />;
    } else {
      return (
        <>
          <SectionDownloadComponent data={Alldownloads} />
        </>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  Alldownloads: state.download.Alldownloads,
  pending: state.download.pending,
  error: state.download.error,
});

export default compose(connect(mapStateToProps, null))(SectionDownload);
