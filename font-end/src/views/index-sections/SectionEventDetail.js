/*eslint-disable*/
import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";

// reactstrap components
import {
  Card,
  Container,
  Row,
  Col,
  Carousel,
  CarouselItem,
  CarouselIndicators,
  CarouselCaption,
} from "reactstrap";

// core components
import DemoFooter from "components/Footers/DemoFooter";
import PendingPage from "views/examples/PendingPage";
import ErrorPage from "views/examples/ErrorPage";

// core api
import { getImageDatas } from "../../stores/requests/eventprofile.request";
import { getImagesDatas } from "../../stores/requests/event.request";

const SectionEventDetailComponent = (props) => {
  // props data
  const { data, imageevents } = props;

  const [activeIndex, setActiveIndex] = React.useState(0);
  const [animating, setAnimating] = React.useState(false);

  const onExiting = () => {
    setAnimating(true);
  };
  const onExited = () => {
    setAnimating(false);
  };
  const next = () => {
    if (animating) return;
    const nextIndex =
      activeIndex === imageevents.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  };
  const previous = () => {
    if (animating) return;
    const nextIndex =
      activeIndex === 0 ? imageevents.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  };
  const goToIndex = (newIndex) => {
    if (animating) return;
    setActiveIndex(newIndex);
  };

  return (
    <>
      <div className="section section-dark">
        <Container className="text-center text-customer">
          <div className="title">
            <h3 className="text-customer">Event</h3>
          </div>
          <Row>
            {data.map((item) => {
              return (
                <Col key={item.id} sm="4" className="mt-2">
                  <div>
                    <img
                      alt="..."
                      src={`https://api.karnkaiart.com/api/upload/eventprofile/${item.image}`}
                      width="100%"
                    />
                  </div>
                </Col>
              );
            })}
            <Col sm="8" className="text-left text-customer">
              <h5>ລາຍລະອຽດ</h5>
              <p>
                ງານວາງສະແດງສີລະປະອັກສອນບັນຈົງ ຄັ້ງຕໍ່ໄປ ຊື່ຫົວຂໍ້ງານ:
                “ໃນວັນທີ່ຕ້ອງ ໄກ”<br></br>
                ວັນເວລາ: 22.11.22 <br></br>
                ສະຖານທີ່: ... <br></br>
                ຈາກການວາງສະແດງຄັ້ງຜ່ານມາ ເຮົາສະຫຼຸບໄດ້ວ່າ <br></br>
                ຕະຫຼອດຫຼາຍປີ ຜົນງານລວມໆຂອງເຮົາ ຂ້ອນຂ້າງສັບສົນປົນເປ <br></br>
                ມັນຄົງດີຖ້າໄດ້ຈັດລຽງໃຫ້ເປັນລະບຽບຫຼາຍກວ່ານີ້. <br></br>
                ກາງປີນີ້ຈຶ່ງເກີດມີປື້ມໂຮມຜົນງານ 9ປີການໄກ <br></br>
                ແລະ ຈັດວາງສະແດງ ໃຫ້ຜູ້ທີ່ສົນໃຈໄດ້ເຂົ້າຊົມ.
              </p>
            </Col>
          </Row>
          <hr className="mt-5 mb-5" />
          <Row>
            <Col>
              <h6>Event Year 2021 (10 Photo)</h6>
              <h6>Event Year 2020 (10 Photo)</h6>
              <h6>Event Year 2019 (10 Photo)</h6>
            </Col>
          </Row>
          <Row className="mt-5">
            <Col className="ml-auto mr-auto" md="8">
              <Card className="page-carousel">
                <Carousel
                  activeIndex={activeIndex}
                  next={next}
                  previous={previous}
                >
                  <CarouselIndicators
                    items={imageevents}
                    activeIndex={activeIndex}
                    onClickHandler={goToIndex}
                  />
                  {imageevents.map((item) => {
                    return (
                      <CarouselItem
                        onExiting={onExiting}
                        onExited={onExited}
                        key={item.id}
                      >
                        <img
                          src={`https://api.karnkaiart.com/api/upload/event/${item.image}`}
                          alt={item.image}
                        />
                        <CarouselCaption
                          captionText={item.description}
                          captionHeader=""
                        />
                      </CarouselItem>
                    );
                  })}
                  <a
                    className="left carousel-control carousel-control-prev"
                    data-slide="prev"
                    href="#pablo"
                    onClick={(e) => {
                      e.preventDefault();
                      previous();
                    }}
                    role="button"
                  >
                    <span className="fa fa-angle-left" />
                    <span className="sr-only">Previous</span>
                  </a>
                  <a
                    className="right carousel-control carousel-control-next"
                    data-slide="next"
                    href="#pablo"
                    onClick={(e) => {
                      e.preventDefault();
                      next();
                    }}
                    role="button"
                  >
                    <span className="fa fa-angle-right" />
                    <span className="sr-only">Next</span>
                  </a>
                </Carousel>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
      <DemoFooter />
    </>
  );
};

class SectionEventDetail extends React.Component {
  // overload data
  componentDidMount = () => {
    this.props.dispatch(getImageDatas());
    this.props.dispatch(getImagesDatas());
  };

  render() {
    const { error, pending, imageeventprofile, imageevents } = this.props;
    if (pending) {
      return <PendingPage />;
    } else if (error) {
      return <ErrorPage />;
    } else {
      return (
        <>
          <SectionEventDetailComponent
            data={imageeventprofile}
            imageevents={imageevents}
          />
        </>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  imageeventprofile: state.eventprof.imageeventprofile,
  imageevents: state.event.imageevents,
  pending: state.eventprof.pending,
  error: state.eventprof.error,
});

export default compose(connect(mapStateToProps, null))(SectionEventDetail);
