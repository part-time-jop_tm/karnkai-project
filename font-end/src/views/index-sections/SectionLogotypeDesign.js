/*eslint-disable*/
import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";

// reactstrap components
import {
  Button,
  Card,
  Carousel,
  CarouselCaption,
  CarouselIndicators,
  CarouselItem,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from "reactstrap";

// core components
import PendingPage from "views/examples/PendingPage";
import ErrorPage from "views/examples/ErrorPage";
import DemoFooter from "components/Footers/DemoFooter";

// core api
import { getAllDatas } from "../../stores/requests/logotypedesign.request";
import { insertData } from "../../stores/requests/package.request";
import { sendMail } from "../../stores/requests/mail.request";

function SectionLogotypeDesignComponent(props) {
  // props data
  const { data, handleSubmitData, handleSendmail } = props;

  const [activeIndex, setActiveIndex] = React.useState(0);
  const [animating, setAnimating] = React.useState(false);

  const onExiting = () => {
    setAnimating(true);
  };
  const onExited = () => {
    setAnimating(false);
  };
  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === data.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  };
  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? data.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  };
  const goToIndex = (newIndex) => {
    if (animating) return;
    setActiveIndex(newIndex);
  };

  // set value
  const [openPackage, setOpenPackage] = React.useState("");
  const [openBoy, setOpenBoy] = React.useState("");
  const [openGirl, setOpenGirl] = React.useState("");
  const [openBoy1, setOpenBoy1] = React.useState("");
  const [openGirl1, setOpenGirl1] = React.useState("");
  const [openBoy2, setOpenBoy2] = React.useState("");
  const [openGirl2, setOpenGirl2] = React.useState("");
  const [openCustomerName, setOpenCustomerName] = React.useState("");
  const [openCustomerEmail, setOpenCustomerEmail] = React.useState("");
  const [openCustomerTel, setOpenCustomerTel] = React.useState("");
  const [openCustomerText, setOpenCustomerText] = React.useState("");

  function handleChangePackage(event) {
    setOpenPackage(event.target.value);
  }

  function handleChangeCustomerName(event) {
    setOpenCustomerName(event.target.value);
  }

  function handleChangeCustomerEmail(event) {
    setOpenCustomerEmail(event.target.value);
  }

  function handleChangeCustomerTel(event) {
    setOpenCustomerTel(event.target.value);
  }

  function handleChangeCustomerText(event) {
    setOpenCustomerText(event.target.value);
  }

  function handleChangeBoy(event) {
    setOpenBoy(event.target.value);
  }

  function handleChangeGirl(event) {
    setOpenGirl(event.target.value);
  }

  function handleChangeBoy1(event) {
    setOpenBoy1(event.target.value);
  }

  function handleChangeGirl1(event) {
    setOpenGirl1(event.target.value);
  }

  function handleChangeBoy2(event) {
    setOpenBoy2(event.target.value);
  }

  function handleChangeGirl2(event) {
    setOpenGirl2(event.target.value);
  }

  // submit
  function SubmitData(event) {
    event.preventDefault();
    let packageData = "";
    if (
      !openPackage ||
      !openCustomerName ||
      !openCustomerEmail ||
      !openCustomerTel ||
      !openCustomerText
    )
      return alert("ກະລຸນາຕື່ມຂໍ້ມູນໃຫ້ຄົບ");
    if (openPackage === "package1") {
      if (!openBoy || !openGirl)
        return alert("ກະລຸນາຕື່ມຂໍ້ມູນຊື່ເຈົ້າບ່າວເຈົ້າສາວໃຫ້ຄົບ");
      packageData = `package: 800,000, ຊື່ເຈົ້າບ່າວ: ${openBoy}, ຊື່ເຈົ້າສາວ: ${openGirl} `;
    }
    if (openPackage === "package2") {
      if (!openBoy1 || !openGirl1)
        return alert("ກະລຸນາຕື່ມຂໍ້ມູນຊື່ເຈົ້າບ່າວເຈົ້າສາວໃຫ້ຄົບ");
      packageData = `package: 300,000, ຊື່ເຈົ້າບ່າວ: ${openBoy1}, ຊື່ເຈົ້າສາວ: ${openGirl1} `;
    }
    if (openPackage === "package3") {
      if (!openBoy2 || !openGirl2)
        return alert("ກະລຸນາຕື່ມຂໍ້ມູນຊື່ເຈົ້າບ່າວເຈົ້າສາວໃຫ້ຄົບ");
      packageData = `package: 200,000, ຊື່ເຈົ້າບ່າວ: ${openBoy2}, ຊື່ເຈົ້າສາວ: ${openGirl2} `;
    }
    if (!confirm("Are you sure?")) return;
    const data = {
      package: packageData,
      customerName: openCustomerName,
      customerEmail: openCustomerEmail,
      customerTel: openCustomerTel,
      customerText: openCustomerText,
      status: "waiting",
    };
    handleSubmitData(data);
    // send mail
    const sendmailCust = {
      from: "karnkai.2022@gmail.com",
      to: openCustomerEmail,
      subject: "From karnkai",
      title: `Dear ${openCustomerEmail} <br /> you submit data successfully.<br />${packageData}, ${openCustomerText} <br /><br /><br />`,
    };
    const sendmailAdmin = {
      from: "karnkai.2022@gmail.com",
      to: "kidkarnkai@gmail.com",
      subject: "notification mail from customer",
      title: `Dear karnkai <br /> the customer submit data for you successfully.<br />Email:  ${openCustomerEmail}<br />${packageData}, ${openCustomerText} <br /><br /><br />`,
    };
    handleSendmail(sendmailCust);
    handleSendmail(sendmailAdmin);
  }

  return (
    <>
      <div className="section section-dark">
        <Container className=" text-center">
          <u>
            <a href="/index">
              <h5 className="text-customer">- ກັບຄືນສູ່ໜ້າຫຼັກ -</h5>
            </a>
          </u>
          <div className="title text-left">
            <h3 className="text-customer">Service</h3>
          </div>
          <Row className="mb-2">
            <Col sm="5">
              <h3 className="text-left">Logotype Design</h3>
            </Col>
          </Row>
          <Row>
            <Col className="ml-auto mr-auto" md="8">
              <Card className="page-carousel">
                <Carousel
                  activeIndex={activeIndex}
                  next={next}
                  previous={previous}
                >
                  <CarouselIndicators
                    items={data}
                    activeIndex={activeIndex}
                    onClickHandler={goToIndex}
                  />
                  {data.map((item) => {
                    return (
                      <CarouselItem
                        onExiting={onExiting}
                        onExited={onExited}
                        key={item.id}
                      >
                        <img
                          src={`https://api.karnkaiart.com/api/upload/logotypedesign/${item.image}`}
                          alt={item.image}
                        />
                        <CarouselCaption
                          captionText={item.description}
                          captionHeader=""
                        />
                      </CarouselItem>
                    );
                  })}
                  <a
                    className="left carousel-control carousel-control-prev"
                    data-slide="prev"
                    href="#pablo"
                    onClick={(e) => {
                      e.preventDefault();
                      previous();
                    }}
                    role="button"
                  >
                    <span className="fa fa-angle-left" />
                    <span className="sr-only">Previous</span>
                  </a>
                  <a
                    className="right carousel-control carousel-control-next"
                    data-slide="next"
                    href="#pablo"
                    onClick={(e) => {
                      e.preventDefault();
                      next();
                    }}
                    role="button"
                  >
                    <span className="fa fa-angle-right" />
                    <span className="sr-only">Next</span>
                  </a>
                </Carousel>
              </Card>
            </Col>
          </Row>
          <Row className="mt-5 mb-5">
            <Col className="text-customer">
              <p>
                ກວ່າທີ່ຄິ່ບ່າວສາວຈະເດີທາງມາພົບກັນທາມກາງຜູ້ຄົນເປັນພັນເປັນລ້ານກວ່າຈະມາພົບກັນໄດ້ຕົກລົງປົງໃຈຮ່ວມທາງຊີວິດນຳກັນຊື່ທັງສອງກໍເຊັ່ນກັນ
                ຈະອອກແບບແນວໃດໃຫ້ກົມກຽວ ເປັນອັນດຽວກັນ
                ສົມກັບຄວາມຮັກທີ່ງົດງາມຂອງທັງສອງທີ່ມີໃຫ້ກັນເຮົາບໍ່ສາມາດປ່ຽນໄປຊື່ຜູ້ໃດ
                ໃຫ້ການອອກແບບງ່າຍດັ່ງໃຈ ພາລະກິດນີ້ອອກແບບດ້ວຍກຽດແລະສັກສີ
                ສຸດສີມີທັງໝົດທີ່ມີ.
              </p>
            </Col>
          </Row>
          <Row className="mt-5">
            <Col className="text-customer text-left">
              <i className="nc-icon nc-email-85" style={{ fontSize: "70px" }} />
            </Col>
          </Row>
          <hr />
          <Form className="contact-form">
            <Row className="text-customer">
              <Col sm="3" className="text-left ml-4">
                <div className="mt-2">
                  <Input
                    defaultValue="package1"
                    id="exampleRadios1"
                    name="exampleRadios"
                    type="radio"
                    onChange={handleChangePackage}
                  />
                  package ໂຕຫຍໍ້ຄູ່ບ່າວສາວ ພາສາອັງກິດ
                  <h5>800,000 ກີບ</h5>
                </div>
              </Col>
              <Col sm="4" className="text-left">
                <label className="mt-2">ອັກສອນຫຍໍ້ບ່າວ</label>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="nc-icon nc-single-02" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    placeholder="ຊື່ເຈົ້າບ່າວ"
                    type="text"
                    value={openBoy}
                    onChange={handleChangeBoy}
                  />
                </InputGroup>
              </Col>
              <Col sm="4" className="text-left">
                <label className="mt-2">ອັກສອນຫຍໍ້ສາວ</label>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="nc-icon nc-single-02" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    placeholder="ຊື່ເຈົ້າສາວ"
                    type="text"
                    value={openGirl}
                    onChange={handleChangeGirl}
                  />
                </InputGroup>
              </Col>
            </Row>
            <hr />
            <Row className="text-customer">
              <Col sm="3" className="text-left ml-4">
                <div className="mt-2">
                  <Input
                    defaultValue="package2"
                    id="exampleRadios2"
                    name="exampleRadios"
                    type="radio"
                    onChange={handleChangePackage}
                  />
                  package ໂຕຫຍໍ້ຄູ່ບ່າວສາວ ພາສາອັງກິດ
                  <h5>300,000 ກີບ</h5>
                </div>
              </Col>
              <Col sm="4" className="text-left">
                <label className="mt-2">ອັກສອນຫຍໍ້ບ່າວ</label>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="nc-icon nc-single-02" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    placeholder="ຊື່ເຈົ້າບ່າວ"
                    type="text"
                    value={openBoy1}
                    onChange={handleChangeBoy1}
                  />
                </InputGroup>
              </Col>
              <Col sm="4" className="text-left">
                <label className="mt-2">ອັກສອນຫຍໍ້ສາວ</label>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="nc-icon nc-single-02" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    placeholder="ຊື່ເຈົ້າສາວ"
                    type="text"
                    value={openGirl1}
                    onChange={handleChangeGirl1}
                  />
                </InputGroup>
              </Col>
            </Row>{" "}
            <hr />
            <Row className="text-customer">
              <Col sm="3" className="text-left ml-4">
                <div className="mt-2">
                  <Input
                    defaultValue="package3"
                    id="exampleRadios3"
                    name="exampleRadios"
                    type="radio"
                    onChange={handleChangePackage}
                  />
                  package ໂຕຫຍໍ້ຄູ່ບ່າວສາວ ພາສາອັງກິດ
                  <h5>200,000 ກີບ</h5>
                </div>
              </Col>
              <Col sm="4" className="text-left">
                <label className="mt-2">ອັກສອນຫຍໍ້ບ່າວ</label>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="nc-icon nc-single-02" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    placeholder="ຊື່ເຈົ້າບ່າວ"
                    type="text"
                    value={openBoy2}
                    onChange={handleChangeBoy2}
                  />
                </InputGroup>
              </Col>
              <Col sm="4" className="text-left">
                <label className="mt-2">ອັກສອນຫຍໍ້ສາວ</label>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="nc-icon nc-single-02" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    placeholder="ຊື່ເຈົ້າສາວ"
                    type="text"
                    value={openGirl2}
                    onChange={handleChangeGirl2}
                  />
                </InputGroup>
              </Col>
            </Row>{" "}
            <hr />
            <Row className="text-white">
              <Col sm="12" className="text-left ml-2">
                <label>ຊື່ລູກຄ້າ</label>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="nc-icon nc-single-02" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    placeholder="ຊື່ລູກຄ້າ"
                    type="text"
                    value={openCustomerName}
                    onChange={handleChangeCustomerName}
                  />
                </InputGroup>
              </Col>
            </Row>
            <Row className="text-white mt-3">
              <Col sm="12" className="text-left ml-2">
                <label>ອີເມວລູກຄ້າ</label>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="nc-icon nc-email-85" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    placeholder="ອີເມວລູກຄ້າ"
                    type="text"
                    value={openCustomerEmail}
                    onChange={handleChangeCustomerEmail}
                  />
                </InputGroup>
              </Col>
            </Row>
            <Row className="text-white mt-3">
              <Col sm="12" className="text-left ml-2">
                <label>ເບີໂທລະສັບລູກຄ້າ</label>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="nc-icon nc-tablet-2" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    placeholder="ເບີໂທລະສັບລູກຄ້າ"
                    type="text"
                    value={openCustomerTel}
                    onChange={handleChangeCustomerTel}
                  />
                </InputGroup>
              </Col>
            </Row>
            <Row className="text-white mt-3">
              <Col sm="12" className="text-left ml-2">
                <label>ຂໍ້ຄວາມ</label>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="nc-icon nc-chat-33" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    placeholder="ຂໍ້ຄວາມ"
                    type="text"
                    value={openCustomerText}
                    onChange={handleChangeCustomerText}
                  />
                </InputGroup>
              </Col>
            </Row>
            <Row>
              <Col sm="12" className="text-right ml-2">
                <Button color="success" outline onClick={SubmitData}>
                  <i className="nc-icon nc-send mr-2" /> ສົ່ງ
                </Button>
              </Col>
            </Row>
          </Form>
          <hr />
          <Row className="text-center mt-5 mb-5">
            <Col sm="4">
              {" "}
              <h3>ອາທິດທີ 1</h3>
              <div className="text-white mb-5">
                <blockquote className="blockquote">
                  <i
                    className="nc-icon nc-ruler-pencil mt-3"
                    style={{ fontSize: "70px" }}
                  />
                  <p>
                    ອອກແບບຮູບຊົງຄ້າວໆ(Sketch) ຮ່າງລາຍເສັ້ນ ໃຫ້ລູກຄ້າກວດ 2-4 ແບບ
                  </p>
                  <h5>ລູກຄ້າ ເລືອກແບນທີ່ມັກ</h5>
                </blockquote>
              </div>
              <p className="text-customer">
                ໝາຍເຫດຖ້າບໍມີແບບໃດທີ່ມັກຖືວ່າສຸດສີມືອອກນັກອອກແບບແລ້ວ.
                <br /> ຈິງໃຈຕໍ່ກັນແບບຮ່າງທັງໝົດແມ່ນບໍ່ອະນຸດຍາດ <br />
                ໃຫ້ເອົາໄປນໍາໃຊ້ພັດທະນາຕໍ່ດັດແປງໃດໆ.
                <br />
                ເປັນການສີ້ນສຸດຂັ້ນຕອນໄວ້ພຽງເທົ່ານີ້ແລະບໍ່ມີຄ່າໃຊ້ຈ່າຍໃດໆ.
              </p>
            </Col>
            <Col sm="4">
              {" "}
              <h3>ອາທິດທີ 2</h3>
              <div className="text-white mb-5">
                <blockquote className="blockquote">
                  <i
                    className="nc-icon nc-vector mt-3"
                    style={{ fontSize: "70px" }}
                  />
                  <p>ພັດທະນາແບບທີ່ລູກຄ້າເລືອກ ອອກແບບເພີ່ມເປັນທາງເລືອກ 3 ແບບ</p>
                  <h5>(ໂຕບາງ, ໂຕກາງ, ໂຕໜາ)</h5>
                </blockquote>
              </div>
              <p className="text-customer">
                ອາດມີການຄາດເຄື່ອນເລື່ອງນັດສົ່ງວຽກອິງຕາມສະຖານະການ
                <br />
                ຄວາມຈຳເປັນແລະຍາກງ່າຍຂອງການອອກແບບ.
                <br />
                (ຖ້າລູກຄ້າມີຄວາມຈຳເປັນຕ້ອງການໃຊ້ໂລໂກ
                <br />
                ແບບຮີບດ່ວນສາມາດແຈ້ງບອກໄດ້).
              </p>
            </Col>
            <Col sm="4">
              {" "}
              <h3>ອາທິດທີ 3</h3>
              <div className="text-white mb-5">
                <blockquote className="blockquote">
                  <i
                    className="nc-icon nc-album-2 mt-3"
                    style={{ fontSize: "70px" }}
                  />
                  <p>ເກັບລາຍລະອຽດ</p>
                  <h5>ຊຳລະຄ່າອອກແບບ ຕາມລາຄາທີ່ຕົກລົງກັນໄວ້</h5>
                </blockquote>
              </div>
              <p className="text-customer">
                ສົ່ງຟາຍສົມບູນໃຫ້ລູກຄ້າ
                <br />
                Logo.pdf
                <br />
                Logo.ai
                <br />
                Logo.png
              </p>
            </Col>
          </Row>
        </Container>
      </div>
      <DemoFooter />
    </>
  );
}

class SectionLogotypeDesign extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.handleSubmitData = this.handleSubmitData.bind(this);
    this.handleSendmail = this.handleSendmail.bind(this);
  }

  // overload data
  componentDidMount = () => {
    this.props.dispatch(getAllDatas());
  };

  // submit send mail
  handleSendmail(data) {
    this.props.dispatch(sendMail(data));
  }

  // submit customer data
  handleSubmitData(data) {
    this.props.dispatch(insertData(data));
  }

  render() {
    const { error, pending, alllogotypedesigns } = this.props;
    if (pending) {
      return <PendingPage />;
    } else if (error) {
      return <ErrorPage />;
    } else {
      return (
        <>
          <SectionLogotypeDesignComponent
            data={alllogotypedesigns}
            handleSubmitData={this.handleSubmitData}
            handleSendmail={this.handleSendmail}
          />
        </>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  alllogotypedesigns: state.logotypedesign.alllogotypedesigns,
  pending: state.logotypedesign.pending,
  error: state.logotypedesign.error,
});

export default compose(connect(mapStateToProps, null))(SectionLogotypeDesign);
