/*eslint-disable*/
import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";

// reactstrap components
import { Container, Row, Col } from "reactstrap";

// core components
import PendingPage from "views/examples/PendingPage";
import ErrorPage from "views/examples/ErrorPage";

// core api
import { getImageDatas } from "../../stores/requests/eventprofile.request";

const SectionEventComponent = (props) => {
  // props data
  const { data } = props;

  return (
    <>
      <div className="section section-dark text-center">
        <Container>
          <div className="title">
            <h3 className="text-customer">Events</h3>
          </div>
          <Row>
            {data.map((item) => {
              return (
                <Col key={item.id} className="ml-auto mr-auto" md="8">
                  <div>
                    <img
                      alt="..."
                      src={`https://api.karnkaiart.com/api/upload/eventprofile/${item.image}`}
                      width="100%"
                    />
                  </div>
                </Col>
              );
            })}
          </Row>{" "}
          {/* <Row className="text-customer mt-4">
            <Col className="ml-auto mr-auto" md="8">
              {" "}
              <a href="/event" target="_blank">
                <h5>See more</h5>
              </a>
            </Col>
          </Row> */}
        </Container>
      </div>
    </>
  );
};

class SectionEvent extends React.Component {
  // overload data
  componentDidMount = () => {
    this.props.dispatch(getImageDatas());
  };

  render() {
    const { error, pending, imageeventprofile } = this.props;
    if (pending) {
      return <PendingPage />;
    } else if (error) {
      return <ErrorPage />;
    } else {
      return (
        <>
          <SectionEventComponent
            data={imageeventprofile}
            handleSubmitData={this.handleSubmitData}
          />
        </>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  imageeventprofile: state.eventprof.imageeventprofile,
  pending: state.eventprof.pending,
  error: state.eventprof.error,
});

export default compose(connect(mapStateToProps, null))(SectionEvent);
