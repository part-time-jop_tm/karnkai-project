/*eslint-disable*/
import React from "react";

// reactstrap components
import {
  Card,
  Container,
  Row,
  Col,
  CardBody,
  CardText,
  CardTitle,
} from "reactstrap";
// core components

const SectionProduction = () => {
  return (
    <>
      <div className="section section-customer text-center">
        <Container>
          <div className="title">
            <h3 className="text-customer">karnkai Product</h3>
          </div>
          <Row md="3" sm="2" xs="1">
            <Col>
              <a href="/product/bag-canvas">
                <Card
                  body
                  inverse
                  style={{
                    backgroundColor: "#7e7669",
                    borderColor: "#333",
                  }}
                >
                  <CardBody>
                    <CardTitle>
                      {" "}
                      <i
                        className="nc-icon nc-bag-16"
                        style={{ fontSize: "70px" }}
                      />
                    </CardTitle>
                  </CardBody>
                </Card>{" "}
                <h5>All Product</h5>
              </a>
            </Col>
            <Col>
              <a href="/product/photobook">
                <Card
                  body
                  inverse
                  style={{
                    backgroundColor: "#7e7669",
                    borderColor: "#333",
                  }}
                >
                  <CardBody>
                    <CardTitle>
                      {" "}
                      <i
                        className="nc-icon nc-book-bookmark"
                        style={{ fontSize: "70px" }}
                      />
                    </CardTitle>
                  </CardBody>
                </Card>{" "}
                <h5>Photo Book</h5>
              </a>
            </Col>
            <Col>
              <a href="/product/calendar">
                <Card
                  body
                  inverse
                  style={{
                    backgroundColor: "#7e7669",
                    borderColor: "#333",
                  }}
                >
                  <CardBody>
                    <CardTitle>
                      {" "}
                      <i
                        className="nc-icon nc-calendar-60"
                        style={{ fontSize: "70px" }}
                      />
                    </CardTitle>
                  </CardBody>
                </Card>
                <h5>Calendar</h5>
              </a>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};

export default SectionProduction;
