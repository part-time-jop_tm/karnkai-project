/*eslint-disable*/
import React from "react";

// reactstrap components
import { Row, Col, Container } from "reactstrap";

// core components
import SectionBag from "./SectionBag";
import SectionCanvas from "./SectionCanvas";
import SectionPhotoBook from "./SectionPhotoBook";
import SectionCalendar from "./SectionCalendar";

function SectionSouvenir() {
  return (
    <>
      <div className="section section-dark">
        <Container> 
          <Row>
            <Col>
              <SectionBag />
            </Col>
          </Row>
          <Row>
            <Col>
              <SectionPhotoBook />
            </Col>
          </Row>
          <Row>
            <Col>
              <SectionCanvas />
            </Col>
          </Row>
          <Row>
            <Col>
              <SectionCalendar />
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

export default SectionSouvenir;
