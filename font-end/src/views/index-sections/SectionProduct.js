/*eslint-disable*/
import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";

// reactstrap components
import {
  Card,
  Col,
  Container,
  Row,
  Form,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  Button,
} from "reactstrap";
// core components
import PendingPage from "views/examples/PendingPage";
import ErrorPage from "views/examples/ErrorPage";
import DemoFooter from "components/Footers/DemoFooter";

// core api
import { getAllDatas } from "../../stores/requests/product.request";
import { insertData } from "../../stores/requests/package.request";
import { sendMail } from "../../stores/requests/mail.request";

function SectionProductComponent(props) {
  // props data
  const { data, handleSubmitData, handleSendmail } = props;

  // set value
  const [openCustomerEmail, setOpenCustomerEmail] = React.useState("");
  const [openCustomerText, setOpenCustomerText] = React.useState("");

  function handleChangeCustomerEmail(event) {
    setOpenCustomerEmail(event.target.value);
  }

  function handleChangeCustomerText(event) {
    setOpenCustomerText(event.target.value);
  }

  // submit
  function SubmitData(event) {
    event.preventDefault();
    if (!openCustomerEmail || !openCustomerText)
      return alert("ກະລຸນາຕື່ມຂໍ້ມູນໃຫ້ຄົບ");
    if (!confirm("Are you sure?")) return;
    const data = {
      package: "package: all product",
      customerName: "NULL",
      customerEmail: openCustomerEmail,
      customerTel: "NULL",
      customerText: openCustomerText,
      status: "waiting",
    };
    handleSubmitData(data);
    // send mail
    const sendmailCust = {
      from: "karnkai.2022@gmail.com",
      to: openCustomerEmail,
      subject: "From karnkai",
      title: `Dear ${openCustomerEmail} <br /> you submit data successfully.<br />package: all product, ${openCustomerText} <br /><br /><br />`,
    };
    const sendmailAdmin = {
      from: "karnkai.2022@gmail.com",
      to: "kidkarnkai@gmail.com",
      subject: "notification mail from customer",
      title: `Dear karnkai <br /> the customer submit data for you successfully.<br />Email:  ${openCustomerEmail}<br />package: all product, ${openCustomerText} <br /><br /><br />`,
    };
    handleSendmail(sendmailCust);
    handleSendmail(sendmailAdmin);
  }

  const priceSplitter = (number) =>
    number && number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

  return (
    <>
      <div className="section section-dark">
        <Container>
          <div className="title text-center">
            <u>
              <a href="/index">
                <h5 className="text-customer">- ກັບຄືນສູ່ໜ້າຫຼັກ -</h5>
              </a>
            </u>
            <h3 className="text-customer">All Product</h3>
          </div>
          <div id="images">
            <Row className="mb-2 text-center">
              {" "}
              {data.map((item) => {
                return (
                  <Col key={item.id} md="4" sm="4">
                    <Card>
                      <img
                        alt={item.description}
                        className="img-thumbnail img-responsive"
                        src={`https://api.karnkaiart.com/api/upload/product/${item.image}`}
                      />
                      <div className="text-customer">
                        <p>ລາຍລະອຽດ: {item.description}</p>
                        <p>ລະຫັດສີນຄ້າ: ​{item.productName}</p>
                        <p>ລາຄາ: {priceSplitter(item.productPrice)} ກີບ.</p>
                      </div>
                    </Card>
                  </Col>
                );
              })}
            </Row>
          </div>
          <Row className="mb-2">
            <Col>
              <p className="text-customer">
                ບາງລາຍການຮອບຜະລິດ ແລະ ຈັດສົ່ງແມ່ນທຸກໆ 3 ເດືອນ. (ທ້າຍເດືອນ ມີນາ,
                ມີຖຸນາ, ກັນຍາ ແລະ ທັນວາ)
              </p>
              <p className="text-customer">
                ສົນໃຈສັ່ງຈອງໄດ້ທາງຂໍ້ຄວາມດ້ານລຸ່ມນີ້ ຫຼື ທີ່ເບີ whatsapp 020
                2999 3255.
              </p>
            </Col>
          </Row>
          <Row className="text-left">
            <Col>
              <Form>
                <div>
                  <label>ອີເມວ</label>
                  <InputGroup>
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="nc-icon nc-email-85" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      placeholder="ອີເມວຂອງທ່ານ"
                      type="email"
                      value={openCustomerEmail}
                      onChange={handleChangeCustomerEmail}
                    />
                  </InputGroup>
                </div>
                <div className="mt-3">
                  <label>ຂໍ້ຄວາມ</label>
                  <Input
                    placeholder="ຂໍ້ຄວາມ"
                    type="textarea"
                    value={openCustomerText}
                    onChange={handleChangeCustomerText}
                  />
                </div>
                <div className="mt-3">
                  <Button outline color="success" onClick={SubmitData}>
                    <i className="nc-icon nc-send mr-2" />
                    ສົ່ງ
                  </Button>
                </div>
              </Form>
            </Col>
          </Row>
        </Container>
      </div>
      <DemoFooter />
    </>
  );
}

class SectionProduct extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.handleSubmitData = this.handleSubmitData.bind(this);
    this.handleSendmail = this.handleSendmail.bind(this);
  }

  // submit customer data
  handleSubmitData(data) {
    this.props.dispatch(insertData(data));
  }

  // submit send mail
  handleSendmail(data) {
    this.props.dispatch(sendMail(data));
  }

  // overload data
  componentDidMount = () => {
    this.props.dispatch(getAllDatas());
  };

  render() {
    const { error, pending, allproducts } = this.props;
    if (pending) {
      return <PendingPage />;
    } else if (error) {
      return <ErrorPage />;
    } else {
      return (
        <>
          <SectionProductComponent
            data={allproducts}
            handleSubmitData={this.handleSubmitData}
            handleSendmail={this.handleSendmail}
          />
        </>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  allproducts: state.product.allproducts,
  pending: state.product.pending,
  error: state.product.error,
});

export default compose(connect(mapStateToProps, null))(SectionProduct);
