/*eslint-disable*/
import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";

// reactstrap components
import {
  Button,
  Card,
  CardBody,
  CardText,
  CardTitle,
  Carousel,
  CarouselCaption,
  CarouselIndicators,
  CarouselItem,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from "reactstrap";

// core components
import PendingPage from "views/examples/PendingPage";
import ErrorPage from "views/examples/ErrorPage";
import DemoFooter from "components/Footers/DemoFooter";

// core api
import { getAllDatas } from "../../stores/requests/photobook.request";
import { insertData } from "../../stores/requests/package.request";
import { sendMail } from "../../stores/requests/mail.request";

function SectionPhotoBookComponent(props) {
  // props data
  const { data, handleSubmitData, handleSendmail } = props;

  const [activeIndex, setActiveIndex] = React.useState(0);
  const [animating, setAnimating] = React.useState(false);

  const onExiting = () => {
    setAnimating(true);
  };
  const onExited = () => {
    setAnimating(false);
  };
  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === data.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  };
  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? data.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  };
  const goToIndex = (newIndex) => {
    if (animating) return;
    setActiveIndex(newIndex);
  };

  // set value
  const [openCustomerEmail, setOpenCustomerEmail] = React.useState("");
  const [openCustomerText, setOpenCustomerText] = React.useState("");

  function handleChangeCustomerEmail(event) {
    setOpenCustomerEmail(event.target.value);
  }

  function handleChangeCustomerText(event) {
    setOpenCustomerText(event.target.value);
  }

  // submit
  function SubmitData(event) {
    event.preventDefault();
    if (!openCustomerEmail || !openCustomerText)
      return alert("ກະລຸນາຕື່ມຂໍ້ມູນໃຫ້ຄົບ");
    if (!confirm("Are you sure?")) return;
    const data = {
      package: "package: photo book",
      customerName: "NULL",
      customerEmail: openCustomerEmail,
      customerTel: "NULL",
      customerText: openCustomerText,
      status: "waiting",
    };
    handleSubmitData(data);
    // send mail
    const sendmailCust = {
      from: "karnkai.2022@gmail.com",
      to: openCustomerEmail,
      subject: "From karnkai",
      title: `Dear ${openCustomerEmail} <br /> you submit data successfully.<br />package: photo book, ${openCustomerText} <br /><br /><br />`,
    };
    const sendmailAdmin = {
      from: "karnkai.2022@gmail.com",
      to: "kidkarnkai@gmail.com",
      subject: "notification mail from customer",
      title: `Dear karnkai <br /> the customer submit data for you successfully.<br />Email:  ${openCustomerEmail}<br />package: photo book, ${openCustomerText} <br /><br /><br />`,
    };
    handleSendmail(sendmailCust);
    handleSendmail(sendmailAdmin);
  }

  return (
    <>
      <div className="section section-dark">
        <Container>
          <div className="title text-center">
            {" "}
            <u>
              <a href="/index">
                <h5 className="text-customer">- ກັບຄືນສູ່ໜ້າຫຼັກ -</h5>
              </a>
            </u>
            <h3 className="text-customer">Photo Book</h3>
          </div>
          <Row>
            <Col className="ml-auto mr-auto" md="8">
              <Card className="page-carousel">
                <Carousel
                  activeIndex={activeIndex}
                  next={next}
                  previous={previous}
                >
                  <CarouselIndicators
                    items={data}
                    activeIndex={activeIndex}
                    onClickHandler={goToIndex}
                  />
                  {data.map((item) => {
                    return (
                      <CarouselItem
                        onExiting={onExiting}
                        onExited={onExited}
                        key={item.id}
                      >
                        <img
                          src={`https://api.karnkaiart.com/api/upload/photobook/${item.image}`}
                          alt={item.image}
                        />
                        <CarouselCaption
                          captionText={item.description}
                          captionHeader=""
                        />
                      </CarouselItem>
                    );
                  })}
                  <a
                    className="left carousel-control carousel-control-prev"
                    data-slide="prev"
                    href="#pablo"
                    onClick={(e) => {
                      e.preventDefault();
                      previous();
                    }}
                    role="button"
                  >
                    <span className="fa fa-angle-left" />
                    <span className="sr-only">Previous</span>
                  </a>
                  <a
                    className="right carousel-control carousel-control-next"
                    data-slide="next"
                    href="#pablo"
                    onClick={(e) => {
                      e.preventDefault();
                      next();
                    }}
                    role="button"
                  >
                    <span className="fa fa-angle-right" />
                    <span className="sr-only">Next</span>
                  </a>
                </Carousel>
              </Card>
            </Col>
          </Row>
          <Row className="text-center">
            <Col sm="4">
              <h4 className="text-customer">ມື້ເປີດໂຕ</h4>
              <Card
                body
                inverse
                style={{
                  backgroundColor: "#7e7669",
                  borderColor: "#333",
                }}
              >
                <CardBody>
                  <CardTitle>
                    {" "}
                    <i
                      className="nc-icon nc-calendar-60"
                      style={{ fontSize: "70px" }}
                    />
                  </CardTitle>
                </CardBody>
                <h5 className="text-white">22.11.2022</h5>
              </Card>
            </Col>
            <Col sm="4">
              <h4 className="text-customer">ປຶ້ມ</h4>
              <Card
                body
                inverse
                style={{
                  backgroundColor: "#7e7669",
                  borderColor: "#333",
                }}
              >
                <CardBody>
                  <CardTitle>
                    <Row>
                      <Col>
                        <i
                          className="nc-icon nc-book-bookmark"
                          style={{ fontSize: "70px" }}
                        />
                      </Col>
                      <Col>
                        {" "}
                        <i
                          className="nc-icon nc-book-bookmark"
                          style={{ fontSize: "70px" }}
                        />
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <h5 className="text-white">ເຫຼັ້ມ1</h5>
                      </Col>
                      <Col>
                        <h5 className="text-white">ເຫຼັ້ມ2</h5>
                      </Col>
                    </Row>
                  </CardTitle>
                </CardBody>
              </Card>
              <div className="text-customer">
                <p>
                  <small>ຂະໜາດ B5</small>
                </p>
                <p>
                  <small>ຈຳນວນ 100 ໜ້າ</small>
                </p>
                <p>
                  <small>ພີມ ຂາວດຳ, ປົກສີ</small>
                </p>
              </div>
            </Col>
            <Col sm="4">
              <h4 className="text-customer">ສັ່ງຈອງ</h4>
              <Card
                body
                inverse
                style={{
                  backgroundColor: "#7e7669",
                  borderColor: "#333",
                }}
              >
                <CardBody>
                  <CardTitle>
                    {" "}
                    <i
                      className="nc-icon nc-bulb-63"
                      style={{ fontSize: "70px" }}
                    />
                  </CardTitle>
                </CardBody>{" "}
                <h5 className="text-white">.</h5>
              </Card>
              <div className="text-customer">
                <p>
                  <small>ຈັດພິມຈຳນວນຈຳກັດ 99 ຊຸດ</small>
                </p>{" "}
                <p>
                  <small>ລາຄາເຫຼັ້ມລະ 99,000 ກີບ</small>
                </p>{" "}
                <p>
                  <small>ສັ່ງທັງ 2 ເຫຼັ້ມ ແຖມ 1 Poster</small>
                </p>{" "}
              </div>
            </Col>
          </Row>
          <Row>
            <Col>
              <h4 className="text-customer text-center mb-2">ຕິດຕໍ່ສັ່ງຈອງ</h4>
              <Form className="text-left">
                <div>
                  <label>ອີເມວ</label>
                  <InputGroup>
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="nc-icon nc-email-85" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      placeholder="ອີເມວ"
                      type="email"
                      value={openCustomerEmail}
                      onChange={handleChangeCustomerEmail}
                    />
                  </InputGroup>
                </div>
                <div className="mt-3">
                  <label>ຂໍ້ຄວາມ</label>
                  <Input
                    placeholder="ຂໍ້ຄວາມ"
                    type="textarea"
                    value={openCustomerText}
                    onChange={handleChangeCustomerText}
                  />
                </div>
                <div className="mt-3">
                  <Button outline color="success" onClick={SubmitData}>
                    <i className="nc-icon nc-send mr-2" />
                    ສົ່ງ
                  </Button>
                </div>
              </Form>
            </Col>
          </Row>
        </Container>
      </div>
      <DemoFooter />
    </>
  );
}

class SectionPhotoBook extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.handleSubmitData = this.handleSubmitData.bind(this);
    this.handleSendmail = this.handleSendmail.bind(this);
  }

  // submit customer data
  handleSubmitData(data) {
    this.props.dispatch(insertData(data));
  }

  // submit send mail
  handleSendmail(data) {
    this.props.dispatch(sendMail(data));
  }

  // overload data
  componentDidMount = () => {
    this.props.dispatch(getAllDatas());
  };

  render() {
    const { error, pending, Allphotobooks } = this.props;
    if (pending) {
      return <PendingPage />;
    } else if (error) {
      return <ErrorPage />;
    } else {
      return (
        <>
          <SectionPhotoBookComponent
            data={Allphotobooks}
            handleSubmitData={this.handleSubmitData}
            handleSendmail={this.handleSendmail}
          />
        </>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  Allphotobooks: state.photobook.Allphotobooks,
  pending: state.photobook.pending,
  error: state.photobook.error,
});

export default compose(connect(mapStateToProps, null))(SectionPhotoBook);
