/*eslint-disable*/
import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";

// reactstrap components
import { Container, Row, Col } from "reactstrap";

// core components
import PendingPage from "views/examples/PendingPage";
import ErrorPage from "views/examples/ErrorPage";

// core api
import { getImageDatas } from "../../stores/requests/workshopprofile.request";

const SectionWorkShopBannerComponent = (props) => {
  // props data
  const { data } = props;

  return (
    <>
      <div className="section section-dark text-center">
        <Container>
          <div className="title">
            <h3 className="text-customer">Workshop</h3>
          </div>
          <Row>
            {data.map((item) => {
              return (
                <Col key={item.id} className="ml-auto mr-auto" md="8">
                  <div>
                    <img
                      alt="..."
                      src={`https://api.karnkaiart.com/api/upload/workshopprofile/${item.image}`}
                      width="100%"
                    />
                  </div>
                </Col>
              );
            })}
          </Row>{" "}
          {/* <Row className="text-customer mt-4">
            <Col className="ml-auto mr-auto" md="8">
              <a href="/workshop" target="_blank">
                <h5>See more details</h5>
              </a>
            </Col>
          </Row> */}
        </Container>
      </div>
    </>
  );
};

class SectionWorkShopBanner extends React.Component {
  // overload data
  componentDidMount = () => {
    this.props.dispatch(getImageDatas());
  };

  render() {
    const { error, pending, imagesworkshopprofile } = this.props;
    if (pending) {
      return <PendingPage />;
    } else if (error) {
      return <ErrorPage />;
    } else {
      return (
        <>
          <SectionWorkShopBannerComponent
            data={imagesworkshopprofile} 
          />
        </>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  imagesworkshopprofile: state.workshopprof.imagesworkshopprofile,
  pending: state.workshopprof.pending,
  error: state.workshopprof.error,
});

export default compose(connect(mapStateToProps, null))(SectionWorkShopBanner);
