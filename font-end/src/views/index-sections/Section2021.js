/*eslint-disable*/
import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";

// reactstrap components
import {
  Col,
  Container,
  Row,
  Card,
  Carousel,
  CarouselIndicators,
  CarouselCaption,
  CarouselItem,
} from "reactstrap";

// core components
import PendingPage from "views/examples/PendingPage";
import ErrorPage from "views/examples/ErrorPage";

// core api
import { getImageDatas } from "../../stores/requests/twentyone.request";

function Section2021Component(props) {
  // props data
  const { data } = props;

  const [activeIndex, setActiveIndex] = React.useState(0);
  const [animating, setAnimating] = React.useState(false);

  const onExiting = () => {
    setAnimating(true);
  };
  const onExited = () => {
    setAnimating(false);
  };
  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === data.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  };
  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? data.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  };
  const goToIndex = (newIndex) => {
    if (animating) return;
    setActiveIndex(newIndex);
  };

  return (
    <> 
      <div>
        <img 
          alt="..."
          src={require("assets/img/ex2021.jpg").default}
          width="100%"
        /> 
      </div> 
      <div className="section section-dark">
        <Container className="text-center">
          <Row className="mb-5 text-danger">
            <Col>
              <h5>
                <u>ລາຍລະອຽດ</u>
              </h5>
            </Col>
          </Row>
          <Row className="mb-5">
            <Col className="ml-auto mr-auto">
              <h5 className="description">
                This is the paragraph where you can write more details about
                your product. Keep you user engaged by providing meaningful
                information. Remember that by this time, the user is curious,
                otherwise he wouldn't scroll to get here. Add a button if you
                want the user to see more.
              </h5>
            </Col>
          </Row>
          <Row>
            <Col className="ml-auto mr-auto" md="8">
              <Card className="page-carousel">
                <Carousel
                  activeIndex={activeIndex}
                  next={next}
                  previous={previous}
                >
                  <CarouselIndicators
                    items={data}
                    activeIndex={activeIndex}
                    onClickHandler={goToIndex}
                  />
                  {data.map((item) => {
                    return (
                      <CarouselItem
                        onExiting={onExiting}
                        onExited={onExited}
                        key={item.id}
                      >
                        <img
                          src={`https://api.karnkaiart.com/api/upload/2021/${item.image}`}
                          alt={item.image}
                        />
                        <CarouselCaption
                          captionText={item.description}
                          captionHeader=""
                        />
                      </CarouselItem>
                    );
                  })}
                  <a
                    className="left carousel-control carousel-control-prev"
                    data-slide="prev"
                    href="#pablo"
                    onClick={(e) => {
                      e.preventDefault();
                      previous();
                    }}
                    role="button"
                  >
                    <span className="fa fa-angle-left" />
                    <span className="sr-only">Previous</span>
                  </a>
                  <a
                    className="right carousel-control carousel-control-next"
                    data-slide="next"
                    href="#pablo"
                    onClick={(e) => {
                      e.preventDefault();
                      next();
                    }}
                    role="button"
                  >
                    <span className="fa fa-angle-right" />
                    <span className="sr-only">Next</span>
                  </a>
                </Carousel>
              </Card>
            </Col>
          </Row>
          <hr />
        </Container>
      </div>
    </>
  );
}

class Section2021 extends React.Component {
  // overload data
  componentDidMount = () => {
    this.props.dispatch(getImageDatas());
  };

  render() {
    const { error, pending, imagestwentyones } = this.props;
    if (pending) {
      return <PendingPage />;
    } else if (error) {
      return <ErrorPage />;
    } else {
      return (
        <>
          <Section2021Component data={imagestwentyones} />
        </>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  imagestwentyones: state.twentyone.imagestwentyones,
  pending: state.twentyone.pending,
  error: state.twentyone.error,
});

export default compose(connect(mapStateToProps, null))(Section2021);
