/*eslint-disable*/
import React from "react";

// reactstrap components
import { Row, Col, Container } from "reactstrap"; 

function SectionExhibition() {
  return (
    <>
      <div className="section section-dark">
        <Container className="text-center">
          {" "}
          <div className="title ">
            <h3>Exhibition</h3>
          </div>
          <Row>
            <Col>
              {" "}
              <h5 className="text-danger">2021</h5>
            </Col>
          </Row> 
        </Container>
      </div>
    </>
  );
}

export default SectionExhibition;
