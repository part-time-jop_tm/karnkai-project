/*eslint-disable*/
import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";

// reactstrap components
import {
  Card,
  Col,
  Container,
  Row,
  Form,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  Button,
} from "reactstrap";
// core components
import PendingPage from "views/examples/PendingPage";
import ErrorPage from "views/examples/ErrorPage";

// core api
import { getImagesDatas } from "../../stores/requests/bag.request";
import { insertData } from "../../stores/requests/package.request";

function SectionBagComponent(props) {
  // props data
  const { data, handleSubmitData } = props;

  // set value
  const [openCustomerEmail, setOpenCustomerEmail] = React.useState("");
  const [openCustomerText, setOpenCustomerText] = React.useState("");

  function handleChangeCustomerEmail(event) {
    setOpenCustomerEmail(event.target.value);
  }

  function handleChangeCustomerText(event) {
    setOpenCustomerText(event.target.value);
  }

  // submit
  function SubmitData(event) {
    event.preventDefault();
    if (!openCustomerEmail || !openCustomerText)
      return alert("ກະລຸນາຕື່ມຂໍ້ມູນໃຫ້ຄົບ");
    if (!confirm("Are you sure?")) return;
    const data = {
      package: "package: bag",
      customerName: "NULL",
      customerEmail: openCustomerEmail,
      customerTel: "NULL",
      customerText: openCustomerText,
      status: "waiting",
    };
    handleSubmitData(data);
  }

  return (
    <>
      <div className="section section-dark">
        <Container>
          <div className="title text-center">
            <h4 className="text-customer">Bag</h4>
          </div>
          <div id="images">
            <Row className="mb-5 text-center">
              {" "}
              {data.map((item) => {
                return (
                  <Col key={item.id} md="4" sm="4">
                    <Card>
                      <img
                        alt={item.description}
                        className="img-thumbnail img-responsive"
                        src={`https://api.karnkaiart.com/api/upload/bag/${item.image}`}
                        style={{
                          width: "100%",
                          height: "225px",
                        }}
                      />
                      <p className="card-description mt-2 mb-2 text-customer">
                        {item.description}
                      </p>
                    </Card>
                  </Col>
                );
              })}
            </Row>
          </div>
          <Row className="text-left">
            <Col>
              <Form>
                <div>
                  <label>ອີເມວ</label>
                  <InputGroup>
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="nc-icon nc-email-85" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      placeholder="ອີເມວ"
                      type="email"
                      value={openCustomerEmail}
                      onChange={handleChangeCustomerEmail}
                    />
                  </InputGroup>
                </div>
                <div className="mt-3">
                  <label>ຂໍ້ຄວາມ</label>
                  <Input
                    placeholder="ຂໍ້ຄວາມ"
                    type="textarea"
                    value={openCustomerText}
                    onChange={handleChangeCustomerText}
                  />
                </div>
                <div className="mt-3">
                  <Button outline color="success" onClick={SubmitData}>
                    <i className="nc-icon nc-send mr-2" />
                    ສົ່ງ
                  </Button>
                </div>
              </Form>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

class SectionBag extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.handleSubmitData = this.handleSubmitData.bind(this);
  }

  // submit customer data
  handleSubmitData(data) {
    this.props.dispatch(insertData(data));
  }

  // overload data
  componentDidMount = () => {
    this.props.dispatch(getImagesDatas());
  };

  render() {
    const { error, pending, Allbags } = this.props;
    if (pending) {
      return <PendingPage />;
    } else if (error) {
      return <ErrorPage />;
    } else {
      return (
        <>
          <SectionBagComponent
            data={Allbags}
            handleSubmitData={this.handleSubmitData}
          />
        </>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  Allbags: state.bag.Allbags,
  pending: state.bag.pending,
  error: state.bag.error,
});

export default compose(connect(mapStateToProps, null))(SectionBag);
