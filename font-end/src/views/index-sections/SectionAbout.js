/*eslint-disable*/
import React from "react";

// reactstrap components
import {
  Container,
  Row,
  Col,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";

function SectionAbout() {
  return (
    <>
      <div className="section section-dark">
        <Container>
          <Row className="text-center">
            <Col sm="3">
              <UncontrolledDropdown inNavbar>
                <DropdownToggle
                  aria-expanded={false}
                  aria-haspopup={true}
                  caret
                  color="default"
                  data-toggle="dropdown"
                  href="#pablo"
                  nav
                  onClick={(e) => e.preventDefault()}
                  role="button"
                >
                  <i aria-hidden={true} className="nc-icon nc-app mr-2" />
                  Download
                </DropdownToggle>
                <DropdownMenu className="dropdown-danger text-center" right>
                  <DropdownItem href="/freevector" target="_blank">
                    Free vector
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Col>
            <Col sm="3">
              <UncontrolledDropdown inNavbar>
                <DropdownToggle
                  aria-expanded={false}
                  aria-haspopup={true}
                  caret
                  color="default"
                  data-toggle="dropdown"
                  href="#pablo"
                  nav
                  onClick={(e) => e.preventDefault()}
                  role="button"
                >
                  <i aria-hidden={true} className="nc-icon nc-app mr-2" />
                  Product
                </DropdownToggle>
                <DropdownMenu className="dropdown-danger text-center" right>
                  <DropdownItem href="/product/bag-canvas" target="_blank">
                    All Product
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem href="/product/photobook" target="_blank">
                    Photo Book
                  </DropdownItem>{" "}
                  <DropdownItem divider />
                  <DropdownItem href="/product/calendar" target="_blank">
                    Calendar
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Col>
            <Col sm="3">
              <UncontrolledDropdown inNavbar>
                <DropdownToggle
                  aria-expanded={false}
                  aria-haspopup={true}
                  caret
                  color="default"
                  data-toggle="dropdown"
                  href="#pablo"
                  nav
                  onClick={(e) => e.preventDefault()}
                  role="button"
                >
                  <i aria-hidden={true} className="nc-icon nc-app mr-2" />
                  Events
                </DropdownToggle>
                <DropdownMenu className="dropdown-danger text-center" right>
                  <DropdownItem href="/event" target="_blank">
                    Event Details
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Col>
            <Col sm="3">
              <UncontrolledDropdown inNavbar>
                <DropdownToggle
                  aria-expanded={false}
                  aria-haspopup={true}
                  caret
                  color="default"
                  data-toggle="dropdown"
                  href="#pablo"
                  nav
                  onClick={(e) => e.preventDefault()}
                  role="button"
                >
                  <i aria-hidden={true} className="nc-icon nc-app mr-2" />
                  Service
                </DropdownToggle>
                <DropdownMenu className="dropdown-danger text-center" right>
                  <DropdownItem href="/logotypedesign" target="_blank">
                    Logotype Design
                  </DropdownItem>
                  <DropdownItem divider />{" "}
                  <DropdownItem href="/workshop" target="_blank">
                    Workshop
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Col>
          </Row>
        </Container>{" "}
      </div>
    </>
  );
}

export default SectionAbout;
