/*eslint-disable*/
import React from "react";

// reactstrap components
import { Col, Container, Row } from "reactstrap";

// core components

function Section2022() {
  return (
    <>
      <div className="section section-dark">
        <Container className="text-center">
          {" "}
          <div className="title ">
            <h3>Exhibition</h3>
          </div>
          <Row>
            <Col>
              {" "}
              <h5 className="text-danger">2022</h5>
            </Col>
          </Row>
          <Row>
            <Col>
              {" "}
              <h3>Coming soon</h3>
            </Col>
          </Row>{" "}
          <Row>
            <Col>
              {" "}
              <h2 className="text-white">22/11/2022</h2>
            </Col>
          </Row>
        </Container>
      </div> 
      <div>
        <img
          alt="..."
          src={require("assets/img/ex2022.jpg").default}
          width="100%"
        />
      </div>
      <div className="section section-dark">
        <Container className="text-center">
          <hr />
        </Container>
      </div>
    </>
  );
}

export default Section2022;
