/*eslint-disable*/
import React from "react";

// reactstrap components
import { Container, Row, Col } from "reactstrap";

// core components

const SectionAboutDetail = () => {
  return (
    <>
      <div className="section section-dark">
        <Container className="text-center text-customer">
          <div className="title">
            <u>
              <a href="/index">
                <h5 className="text-customer">- ກັບຄືນສູ່ໜ້າຫຼັກ -</h5>
              </a>
            </u>
            <h3 className="text-customer">About</h3>
          </div>
          <Row>
            <Col sm="4">
              <div>
                <img
                  alt="..."
                  src={require("assets/img/faces/about-1.jpg").default}
                  width="100%"
                />
              </div>
            </Col>
            <Col sm="8" className="text-left text-customer">
              <h5>
                ໄກຍະສິດ ສິຣິວົງສາ
                <br></br>
                Kaiyasith SIRIVONGSA
              </h5>

              <p>
                ດ້ວຍຊື່ຫຼີ້ນ ທີ່ທຸກຄົນມັກ ເອີ້ນວ່າ “ໄກ” ຕອນທີ່ຕັ້ງນາມປາກກາ
                ຢາກໃຫ້ມີຄວາມເປັນທາງການ ແລະ ຄວາມໝາຍດີ ເຮົາຈຶ່ງພຽງຕື່ມ ຄຳວ່າ “ການ”
                ໃສ່ທາງໜ້າ ກາຍມາເປັນ “ການໄກ” “Karnkai” ເຮົາມັກຊື່ນີ້ຫຼາຍ
                ເຖິງແມ່ນວ່າ ໃນIG ຫຼື
                Youtubeຈະມີຄົນອື່ນເອົາຊື່ນີ້ໄປຕັ້ງກ່ອນແລ້ວກໍຕາມ ເຮົາກໍຈຶ່ງ
                ຕື່ມຄຳວ່າ ຄິດ(kid) ໃສ່ທາງໜ້າອີກ ກາຍເປັນ “kidkarnkai” “ຄິດການໄກ”.
              </p>
            </Col>
          </Row>
          <Row className="mt-5">
            <Col className="ml-auto mr-auto" md="12">
              <div>
                <img
                  alt="..."
                  src={require("assets/img/faces/about-2.jpg").default}
                  width="100%"
                />
              </div>
            </Col>
          </Row>{" "}
          <Row className="mt-5">
            <Col className="ml-auto mr-auto" md="12">
              <div>
                <p>
                  ເຮົາເອງບໍ່ໄດ້ຮຽນຈົບສາຍຄູ ຫຼື ຮຽນຈົບວິຊາສະເພາະສາຍສິລະປະ,
                  ທັງໝົດທີ່ເຫັນເປັນການໄຝ່ຮູ້ ແລະ ພອນສະແຫວງ ສຶກສາຮຽນຮູ້
                  ຝຶກຝົນລອງຜິດລອງຖືກໄປເລື້ອຍໆ.
                  <br></br>
                  ນນາມ ການໄກ ທີ່ມີທຸກມື້ນີ້ໄດ້ ຕ້ອງຂໍຂອບໃຈຄອບຄົວ ແລະ
                  ຂໍສະແດງຮູ້ບຸນຄຸນບັນດາຜູ້ມີພະຄຸນທຸກທ່ານ, <br></br>
                  ຄູບາອາຈານ ສິລະປິນ ແລະ ບັນດານັກຂຽນອັກສອນວິຈິດບັນຈົງທຸກໆທ່ານ{" "}
                  <br></br>
                  ລວມທັງໝູ່ເພື່ອນ ທີ່ເປັນຕົ້ນແບບທາງຄວາມຄິດ ແລະ ທັກສະການຮຽນຮູ້
                  ຂອງເຮົາ.​
                </p>
              </div>
            </Col>
          </Row>{" "}
        </Container>
      </div>
    </>
  );
};

export default SectionAboutDetail;
