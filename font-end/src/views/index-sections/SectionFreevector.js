/*eslint-disable*/
import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";

// reactstrap components
import {
  Card,
  Container,
  Row,
  Col,
  Carousel,
  CarouselItem,
  CarouselIndicators,
  CarouselCaption,
} from "reactstrap";

// core components
import DemoFooter from "components/Footers/DemoFooter";
import PendingPage from "views/examples/PendingPage";
import ErrorPage from "views/examples/ErrorPage";

// core api
import { getAllDownloads } from "../../stores/requests/download.request";

const SectionFreevectorComponent = (props) => {
  // props data
  const { data } = props;

  const [activeIndex, setActiveIndex] = React.useState(0);
  const [animating, setAnimating] = React.useState(false);

  const onExiting = () => {
    setAnimating(true);
  };
  const onExited = () => {
    setAnimating(false);
  };
  const next = () => {
    if (animating) return;
    const nextIndex =
      activeIndex === data.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  };
  const previous = () => {
    if (animating) return;
    const nextIndex =
      activeIndex === 0 ? data.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  };
  const goToIndex = (newIndex) => {
    if (animating) return;
    setActiveIndex(newIndex);
  };

  return (
    <>
      <div className="section section-dark">
        <Container className="text-center text-customer">
          <div className="title">
            <h3 className="text-customer">Free vector</h3>
          </div>
          <Row>
            <Col sm="6" className="mt-2">
              <Card className="page-carousel">
                <Carousel
                  activeIndex={activeIndex}
                  next={next}
                  previous={previous}
                >
                  <CarouselIndicators
                    items={data}
                    activeIndex={activeIndex}
                    onClickHandler={goToIndex}
                  />
                  {data.map((item) => {
                    return (
                      <CarouselItem
                        onExiting={onExiting}
                        onExited={onExited}
                        key={item.id}
                      >
                        <img
                          src={`https://api.karnkaiart.com/api/upload/download/${item.image}`}
                          alt={item.image}
                        />
                        <CarouselCaption
                          captionText={item.description}
                          captionHeader=""
                        />
                      </CarouselItem>
                    );
                  })}
                  <a
                    className="left carousel-control carousel-control-prev"
                    data-slide="prev"
                    href="#pablo"
                    onClick={(e) => {
                      e.preventDefault();
                      previous();
                    }}
                    role="button"
                  >
                    <span className="fa fa-angle-left" />
                    <span className="sr-only">Previous</span>
                  </a>
                  <a
                    className="right carousel-control carousel-control-next"
                    data-slide="next"
                    href="#pablo"
                    onClick={(e) => {
                      e.preventDefault();
                      next();
                    }}
                    role="button"
                  >
                    <span className="fa fa-angle-right" />
                    <span className="sr-only">Next</span>
                  </a>
                </Carousel>
              </Card>
            </Col>
            <Col sm="6" className="text-left text-customer">
              <h5>ລາຍລະອຽດ</h5>
              <p>
                Lorem Ipsum is simply dummy text of the print- ing and
                typesetting industry. Lorem Ipsum has been the industry's
                standard dummy text ever since the 1500s, when an unknown
                printer took a galley of type and scrambled it to make a type
                specimen book. It has survived not only five cen- turies, but
                also the leap into electronic typeset- ting, remaining
                essentially unchanged. It was popularised in the 1960s with the
                release of Let- raset sheets containing Lorem Ipsum passages,
                and more recently with desktop publishing soft- ware like Aldus
                PageMaker including versions of Lorem Ipsum.
              </p>
            </Col>
          </Row>
        </Container>
      </div>
      <DemoFooter />
    </>
  );
};

class SectionFreevector extends React.Component {
  // overload data
  componentDidMount = () => {
    this.props.dispatch(getAllDownloads());
  };

  render() {
    const { error, pending, Alldownloads } = this.props;
    if (pending) {
      return <PendingPage />;
    } else if (error) {
      return <ErrorPage />;
    } else {
      return (
        <>
          <SectionFreevectorComponent data={Alldownloads} />
        </>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  Alldownloads: state.download.Alldownloads,
  pending: state.download.pending,
  error: state.download.error,
});

export default compose(connect(mapStateToProps, null))(SectionFreevector);
