/*eslint-disable*/
import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";

// reactstrap components
import { Container, Row, Col } from "reactstrap";
// core components

const SectionContact = () => {
  return (
    <>
      <div className="section section-customer">
        <Container className="text-center">
          <Row>
            <Col className="ml-auto mr-auto" md="8"> 
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3796.089777652652!2d102.6429603!3d17.9279661!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31246672af25e6a9%3A0x996258c7ef13dcf0!2zMTfCsDU1JzQwLjciTiAxMDLCsDM4JzQyLjUiRQ!5e0!3m2!1sen!2sla!4v1647687432984!5m2!1sen!2sla" 
                width="100%"
                height="350px"
                className="mt-3"
              ></iframe>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};

export default SectionContact;
