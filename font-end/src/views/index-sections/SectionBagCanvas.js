/*eslint-disable*/
import React from "react";
 
// core components
import SectionBag from "./SectionBag";
import SectionCanvas from "./SectionCanvas";
import DemoFooter from "components/Footers/DemoFooter";

const SectionBagCanvas = () => {
  return (
    <>
      <SectionBag />
      <SectionCanvas />
      <DemoFooter />
    </>
  );
};

export default SectionBagCanvas;
