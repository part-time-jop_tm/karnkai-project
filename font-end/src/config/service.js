const Service = {
  // login
  apiLogin: "auth/login",
  // user
  apiGetUsers: "user/",
  apiInsertUser: "auth/register",
  apiDeleteUser: "user/delete/",
  apiLockUser: "user/lock/",
  apiActiveUser: "user/active/",
  apiChangePasswordUser: "user/change/password/",
  // download
  apiGetDownload: "download/",
  apiInsertDownload: "download/insert",
  apiDeleteDownload: "download/delete/",
  apiDisabledDownload: "download/disabled/",
  apiEnabledDownload: "download/enabled/",
  // adverse
  apiGetAdverses: "adverse/",
  apiInsertAdverse: "adverse/insert",
  apiDeleteAdverse: "adverse/delete/",
  apiDisabledAdverse: "adverse/disabled/",
  apiEnabledAdverse: "adverse/enabled/",
  // bag
  apiGetBags: "bag/",
  apiInsertBag: "bag/insert",
  apiDeleteBag: "bag/delete/",
  apiDisabledBag: "bag/disabled/",
  apiEnabledBag: "bag/enabled/",
  // canvas
  apiGetCanvases: "canvas/",
  apiInsertCanvas: "canvas/insert",
  apiDeleteCanvas: "canvas/delete/",
  apiDisabledCanvas: "canvas/disabled/",
  apiEnabledCanvas: "canvas/enabled/",
  // photo book
  apiGetPhotoBooks: "photobook/",
  apiInsertPhotoBook: "photobook/insert",
  apiDeletePhotoBook: "photobook/delete/",
  apiDisabledPhotoBook: "photobook/disabled/",
  apiEnabledPhotoBook: "photobook/enabled/",
  // calendar
  apiGetCalendars: "calendar/",
  apiInsertCalendar: "calendar/insert",
  apiDeleteCalendar: "calendar/delete/",
  apiDisabledCalendar: "calendar/disabled/",
  apiEnabledCalendar: "calendar/enabled/",
  // 2021
  apiGet2021: "twentyone/",
  apiInsert2021: "twentyone/insert",
  apiDelete2021: "twentyone/delete/",
  apiDisabled2021: "twentyone/disabled/",
  apiEnabled2021: "twentyone/enabled/",
  // 2022
  apiGet2022: "twentytwo/",
  apiInsert2022: "twentytwo/insert",
  apiDelete2022: "twentytwo/delete/",
  apiDisabled2022: "twentytwo/disabled/",
  apiEnabled2022: "twentytwo/enabled/",
  // workshop
  apiGetWorkshops: "workshop/",
  apiInsertWorkshop: "workshop/insert",
  apiDeleteWorkshop: "workshop/delete/",
  apiDisabledWorkshop: "workshop/disabled/",
  apiEnabledWorkshop: "workshop/enabled/",
  // workshop profile 
  apiGetWorkshopProfile: "workshopprofile/",
  apiInsertWorkshopProfile: "workshopprofile/insert", 
  // logotype design
  apiGetLogotypeDesigns: "logotypedesign/",
  apiInsertLogotypeDesign: "logotypedesign/insert",
  apiDeleteLogotypeDesign: "logotypedesign/delete/",
  apiDisabledLogotypeDesign: "logotypedesign/disabled/",
  apiEnabledLogotypeDesign: "logotypedesign/enabled/",
  apiInsertPackageLogotypeDesign: "packagelogotypedesign/insert",
  // package
  apiInsertPackage: "package/insert",
  apiGetPackage: "packageAdmin/",
  apieUpdatePackag: "packageAdmin/update/",
  // all images not authen
  apiGetImagesAlAadverse: "images/all/adverse",
  apiGetImagesAllDownload: "images/all/download",
  apiGetImagesAllBag: "images/all/bag",
  apiGetImagesAllPhotobook: "images/all/photobook",
  apiGetImagesAllWorkshop: "images/all/workshop",
  apiGetImagesAllLogotypedesign: "images/all/logotypedesign",
  apiGetImagesAllCalendar: "images/all/calendar",
  apiGetImagesAllProduct: "images/all/product",
  // images not authen limit 9
  apiGetImageCanvas: "images/canvas",
  apiGetImageCalendar: "images/calendar",
  apiGetImageTwentyone: "images/twentyone",
  apiGetImageWorkshop: "images/workshop",
  apiGetImageBag: "images/bag",
  apiGetImageEvent: "images/event",
  apiGetImageEventProfile: "images/eventprofile",
  apiGetImageWorkshopProfile: "images/workshopprofile",
  apiGetImagePreWeddingProfile: "images/preweddingprofile",
  // event
  apiGetEvents: "event/",
  apiInsertEvent: "event/insert",
  apiDeleteEvent: "event/delete/",
  apiDisabledEvent: "event/disabled/",
  apiEnabledEvent: "event/enabled/",
  // event profile
  apiGetEventProfile: "eventprofile/",
  apiInsertEventProfile: "eventprofile/insert", 
  // photo book
  apiGetProducts: "product/",
  apiInsertProduct: "product/insert",
  apiDeleteProduct: "product/delete/",
  apiDisabledProduct: "product/disabled/",
  apiEnabledProduct: "product/enabled/", 
  // prewedding profile
  apiGetPreWeddingProfile: "preweddingprofile/",
  apiInsertPreWeddingProfile: "preweddingprofile/insert", 
  // send mail
  apiSendMail: "mail/sending"
};

export default Service;
