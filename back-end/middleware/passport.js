const passport = require("passport");
const LocalStrategy = require("passport-local");
const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;
var bcrypt = require("bcrypt");
var sql = require("../config/connectdb");
const config = require("../config/config");
const Response = require("../models/response");

const localLogin = new LocalStrategy(
  {
    usernameField: "username",
  },
  function (username, password, done) {
    sql.query(
      "select * from users where username='" + username + "' ",
      function (err, user) {
        try { 
          if (!user || !bcrypt.compareSync(password, user[0].password)) {
            let date_ob = new Date();
            let date = ("0" + date_ob.getDate()).slice(-2);
            let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
            let year = date_ob.getFullYear();
            let hours = date_ob.getHours();
            let minutes = date_ob.getMinutes();
            let seconds = date_ob.getSeconds();
            Response.buildResponseEntity(
              username,
              year + month + date + hours + minutes + seconds,
              year +
                "-" +
                month +
                "-" +
                date +
                " " +
                hours +
                ":" +
                minutes +
                ":" +
                seconds,
              "POST",
              "/api/service/auth/login",
              401,
              false,
              "Login Incorrect",
              { username: username, password: password },
              function (err, data) {
                if (err) return err;
                return data;
              }
            );
            return done(null, false, { message: "Incorrect user name" });
          } 
          return done(null, user);
        } catch (error) {
          return done(null, err);
        }
      }
    );
  }
);

const jwtLogin = new JwtStrategy(
  {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: config.jwtsecret,
  },
  function (username, done) {
    sql.query(
      "select * from users where username='" + username + "' ",
      function (err, user) {
        try {
          if (err) {
            return done(null, err);
          }
          if (!user) {
            return done(null, false);
          }
          return done(null, true, user);
        } catch (error) {
          return done(null, err);
        }
      }
    );
  }
);

passport.use(jwtLogin);
passport.use(localLogin);

module.exports = passport;
