const express = require("express");
const user = require("./user.route");
const auth = require("./auth.route");
const mail = require("./mail.route");
const instagram = require("./instagram.route");
const download = require("./download.route");
const bag = require("./bag.route");
const canvas = require("./canvas.route");
const photobook = require("./photobook.route");
const calendar = require("./calendar.route");
const twentyone = require("./2021.route");
const twentytwo = require("./2022.route");
const workshop = require("./workshop.route");
const logotypedesign = require("./logotypedesign.route");
const images = require("./images.route");
const imagesall = require("./imagesall.route");
const adverse = require("./adverse.route");
const packagelogotypedesignAdmin = require("./packagelogotypedesignAdmin.route");
const packagelogotypedesign = require("./packagelogotypedesign.route");
const package = require("./package.route");
const packageAdmin = require("./packageAdmin.route");
const event = require("./event.route");
const eventprofile = require("./eventprofile.route");
const product = require("./product.route");
const workshopprofile = require("./workshopprofile.route");
const preweddingprofile = require("./preweddingprofile.route");
const routes = express.Router(); // eslint-disable-line new-cap

/** GET /health-check - Check service health */
routes.get("/health-check", (req, res) => res.send("OK"));

routes.use("/user", user);
routes.use("/auth", auth);
routes.use("/mail", mail);
routes.use("/instagram", instagram);
routes.use("/download", download);
routes.use("/bag", bag);
routes.use("/canvas", canvas);
routes.use("/photobook", photobook);
routes.use("/calendar", calendar);
routes.use("/twentyone", twentyone);
routes.use("/twentytwo", twentytwo);
routes.use("/workshop", workshop);
routes.use("/workshopprofile", workshopprofile);
routes.use("/logotypedesign", logotypedesign);
routes.use("/images", images);
routes.use("/images/all", imagesall);
routes.use("/adverse", adverse);
routes.use("/packagelogotypedesignAdmin", packagelogotypedesignAdmin);
routes.use("/packagelogotypedesign", packagelogotypedesign); 
routes.use("/package", package);
routes.use("/packageAdmin", packageAdmin);
routes.use("/event", event);
routes.use("/eventprofile", eventprofile); 
routes.use("/product", product);
routes.use("/preweddingprofile", preweddingprofile);

module.exports = routes;
