const express = require('express');
const asyncHandler = require('express-async-handler')
const passport = require('passport'); 
const Download = require('../../controllers/download.controller');
const routes = express.Router();     

module.exports = routes;  

routes.use(passport.authenticate('jwt', { session: false }));
routes.post('/insert', [passport.authenticate('jwt', { session: false })], asyncHandler(Download.insertImage));  
routes.post('/', [passport.authenticate('jwt', { session: false })], asyncHandler(Download.getAllImages));  
routes.post('/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Download.getImageID)); 
routes.post('/delete/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Download.DeleteImage)); 
routes.put('/update/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Download.UpdateImage));
routes.put('/disabled/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Download.DisabledImage));
routes.put('/enabled/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Download.EnabledImage));