const express = require('express');
const asyncHandler = require('express-async-handler')
const passport = require('passport'); 
const TwentyOne = require('../../controllers/2021.controller');
const routes = express.Router();     

module.exports = routes;  

routes.use(passport.authenticate('jwt', { session: false }));
routes.post('/insert', [passport.authenticate('jwt', { session: false })], asyncHandler(TwentyOne.insertImage));  
routes.post('/', [passport.authenticate('jwt', { session: false })], asyncHandler(TwentyOne.getAllImages));  
routes.post('/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(TwentyOne.getImageID)); 
routes.post('/delete/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(TwentyOne.DeleteImage)); 
routes.put('/update/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(TwentyOne.UpdateImage));
routes.put('/disabled/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(TwentyOne.DisabledImage));
routes.put('/enabled/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(TwentyOne.EnabledImage));