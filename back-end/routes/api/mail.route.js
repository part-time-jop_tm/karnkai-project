const express = require("express");
const asyncHandler = require("express-async-handler"); 
const mail = require("../../controllers/mail.controller");
const routes = express.Router();

module.exports = routes;

routes.post("/sending", asyncHandler(mail.sendMail));
