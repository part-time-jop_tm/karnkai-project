const express = require('express');
const asyncHandler = require('express-async-handler')
const passport = require('passport'); 
const user = require('../../controllers/user.controller');
const routes = express.Router();     

module.exports = routes;  

routes.use(passport.authenticate('jwt', { session: false }));
routes.post('/', [passport.authenticate('jwt', { session: false })], asyncHandler(user.getUsers)); 
routes.post('/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(user.getUserID)); 
routes.post('/delete/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(user.deleteUser)); 
// routes.put('/update/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(user.updateUser));
routes.put('/lock/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(user.lockUser));
routes.put('/active/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(user.activeUser));
routes.post('/activity/logs', [passport.authenticate('jwt', { session: false })], asyncHandler(user.getLogs));
routes.put('/change/password/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(user.changePassword));