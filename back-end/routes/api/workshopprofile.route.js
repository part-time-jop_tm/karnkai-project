const express = require('express');
const asyncHandler = require('express-async-handler')
const passport = require('passport'); 
const WorkShopProfile = require('../../controllers/workshopprofile.controller');
const routes = express.Router();     

module.exports = routes;  

routes.use(passport.authenticate('jwt', { session: false }));
routes.post('/insert', [passport.authenticate('jwt', { session: false })], asyncHandler(WorkShopProfile.insertImage));  
routes.post('/', [passport.authenticate('jwt', { session: false })], asyncHandler(WorkShopProfile.getAllImages));  