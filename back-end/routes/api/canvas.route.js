const express = require('express');
const asyncHandler = require('express-async-handler')
const passport = require('passport'); 
const Canvas = require('../../controllers/canvas.controller');
const routes = express.Router();     

module.exports = routes;  

routes.use(passport.authenticate('jwt', { session: false }));
routes.post('/insert', [passport.authenticate('jwt', { session: false })], asyncHandler(Canvas.insertImage));  
routes.post('/', [passport.authenticate('jwt', { session: false })], asyncHandler(Canvas.getAllImages));  
routes.post('/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Canvas.getImageID)); 
routes.post('/delete/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Canvas.DeleteImage)); 
routes.put('/update/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Canvas.UpdateImage));
routes.put('/disabled/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Canvas.DisabledImage));
routes.put('/enabled/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Canvas.EnabledImage));