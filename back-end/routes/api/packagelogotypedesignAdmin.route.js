const express = require('express');
const asyncHandler = require('express-async-handler')
const passport = require('passport'); 
const packagelogotypedesign = require('../../controllers/packagelogotypedesignAdmin.controller');
const routes = express.Router();     

module.exports = routes;  

routes.use(passport.authenticate('jwt', { session: false }));  
routes.post('/', [passport.authenticate('jwt', { session: false })], asyncHandler(packagelogotypedesign.getAllPackage));  
routes.post('/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(packagelogotypedesign.getPackage));  
routes.put('/update/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(packagelogotypedesign.UpdatePackage)); 