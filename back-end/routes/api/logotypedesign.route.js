const express = require('express');
const asyncHandler = require('express-async-handler')
const passport = require('passport'); 
const LogotypeDesign = require('../../controllers/logotypedesign.controller');
const routes = express.Router();     

module.exports = routes;  

routes.use(passport.authenticate('jwt', { session: false }));
routes.post('/insert', [passport.authenticate('jwt', { session: false })], asyncHandler(LogotypeDesign.insertImage));  
routes.post('/', [passport.authenticate('jwt', { session: false })], asyncHandler(LogotypeDesign.getAllImages));  
routes.post('/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(LogotypeDesign.getImageID)); 
routes.post('/delete/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(LogotypeDesign.DeleteImage)); 
routes.put('/update/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(LogotypeDesign.UpdateImage));
routes.put('/disabled/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(LogotypeDesign.DisabledImage));
routes.put('/enabled/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(LogotypeDesign.EnabledImage));