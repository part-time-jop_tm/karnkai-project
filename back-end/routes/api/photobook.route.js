const express = require('express');
const asyncHandler = require('express-async-handler')
const passport = require('passport'); 
const PhotoBook = require('../../controllers/photobook.controller');
const routes = express.Router();     

module.exports = routes;  

routes.use(passport.authenticate('jwt', { session: false }));
routes.post('/insert', [passport.authenticate('jwt', { session: false })], asyncHandler(PhotoBook.insertImage));  
routes.post('/', [passport.authenticate('jwt', { session: false })], asyncHandler(PhotoBook.getAllImages));  
routes.post('/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(PhotoBook.getImageID)); 
routes.post('/delete/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(PhotoBook.DeleteImage)); 
routes.put('/update/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(PhotoBook.UpdateImage));
routes.put('/disabled/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(PhotoBook.DisabledImage));
routes.put('/enabled/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(PhotoBook.EnabledImage));