const express = require('express');
const asyncHandler = require('express-async-handler')
const passport = require('passport'); 
const Product = require('../../controllers/product.controller');
const routes = express.Router();     

module.exports = routes;  

routes.use(passport.authenticate('jwt', { session: false }));
routes.post('/insert', [passport.authenticate('jwt', { session: false })], asyncHandler(Product.insertImage));  
routes.post('/', [passport.authenticate('jwt', { session: false })], asyncHandler(Product.getAllImages));  
routes.post('/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Product.getImageID)); 
routes.post('/delete/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Product.DeleteImage)); 
routes.put('/update/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Product.UpdateImage));
routes.put('/disabled/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Product.DisabledImage));
routes.put('/enabled/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Product.EnabledImage));