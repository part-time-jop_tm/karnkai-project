const express = require('express');
const asyncHandler = require('express-async-handler')
const passport = require('passport'); 
const PreWeddingProfile = require('../../controllers/preweddingprofile.controller');
const routes = express.Router();     

module.exports = routes;  

routes.use(passport.authenticate('jwt', { session: false }));
routes.post('/insert', [passport.authenticate('jwt', { session: false })], asyncHandler(PreWeddingProfile.insertImage));  
routes.post('/', [passport.authenticate('jwt', { session: false })], asyncHandler(PreWeddingProfile.getAllImages));  