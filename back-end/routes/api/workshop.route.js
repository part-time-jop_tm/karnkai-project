const express = require('express');
const asyncHandler = require('express-async-handler')
const passport = require('passport'); 
const WorkShop = require('../../controllers/workshop.controller');
const routes = express.Router();     

module.exports = routes;  

routes.use(passport.authenticate('jwt', { session: false }));
routes.post('/insert', [passport.authenticate('jwt', { session: false })], asyncHandler(WorkShop.insertImage));  
routes.post('/', [passport.authenticate('jwt', { session: false })], asyncHandler(WorkShop.getAllImages));  
routes.post('/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(WorkShop.getImageID)); 
routes.post('/delete/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(WorkShop.DeleteImage)); 
routes.put('/update/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(WorkShop.UpdateImage));
routes.put('/disabled/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(WorkShop.DisabledImage));
routes.put('/enabled/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(WorkShop.EnabledImage));