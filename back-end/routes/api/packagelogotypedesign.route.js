const express = require("express");
const asyncHandler = require("express-async-handler");
const packagelogotypedesign = require("../../controllers/packagelogotypedesign.controller");
const routes = express.Router();

module.exports = routes;
routes.post("/insert", asyncHandler(packagelogotypedesign.insertPackage));
