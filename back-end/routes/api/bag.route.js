const express = require('express');
const asyncHandler = require('express-async-handler')
const passport = require('passport'); 
const Bag = require('../../controllers/bag.controller');
const routes = express.Router();     

module.exports = routes;  

routes.use(passport.authenticate('jwt', { session: false }));
routes.post('/insert', [passport.authenticate('jwt', { session: false })], asyncHandler(Bag.insertImage));  
routes.post('/', [passport.authenticate('jwt', { session: false })], asyncHandler(Bag.getAllImages));  
routes.post('/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Bag.getImageID)); 
routes.post('/delete/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Bag.DeleteImage)); 
routes.put('/update/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Bag.UpdateImage));
routes.put('/disabled/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Bag.DisabledImage));
routes.put('/enabled/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Bag.EnabledImage));