const express = require('express');
const asyncHandler = require('express-async-handler') 
const Images = require('../../controllers/images.controller');
const routes = express.Router();     

module.exports = routes;   

routes.get('/download', asyncHandler(Images.getDownload));   
routes.get('/bag', asyncHandler(Images.getBag));   
routes.get('/canvas', asyncHandler(Images.getCanvas));   
routes.get('/photobook', asyncHandler(Images.getPhotoBook));   
routes.get('/calendar', asyncHandler(Images.getCalendar));   
routes.get('/twentyone', asyncHandler(Images.getTwentyOne));   
routes.get('/twentytwo', asyncHandler(Images.getTwentyTwo));   
routes.get('/workshop', asyncHandler(Images.getWorkShop));   
routes.get('/logotypedesign', asyncHandler(Images.getLogotypeDesign));   
routes.get('/adverse', asyncHandler(Images.getAdverse));   
routes.get('/event', asyncHandler(Images.getEvent));   
routes.get('/eventprofile', asyncHandler(Images.getEventProfile));   
routes.get('/workshopprofile', asyncHandler(Images.getWorkShopProfile)); 
routes.get('/preweddingprofile', asyncHandler(Images.getPreweddingProfile)); 