const express = require("express");
const asyncHandler = require("express-async-handler");
const package = require("../../controllers/package.controller");
const routes = express.Router();

module.exports = routes;
routes.post("/insert", asyncHandler(package.insertPackage));
