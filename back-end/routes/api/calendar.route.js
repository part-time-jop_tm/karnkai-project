const express = require('express');
const asyncHandler = require('express-async-handler')
const passport = require('passport'); 
const Calendar = require('../../controllers/calendar.controller');
const routes = express.Router();     

module.exports = routes;  

routes.use(passport.authenticate('jwt', { session: false }));
routes.post('/insert', [passport.authenticate('jwt', { session: false })], asyncHandler(Calendar.insertImage));  
routes.post('/', [passport.authenticate('jwt', { session: false })], asyncHandler(Calendar.getAllImages));  
routes.post('/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Calendar.getImageID)); 
routes.post('/delete/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Calendar.DeleteImage)); 
routes.put('/update/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Calendar.UpdateImage));
routes.put('/disabled/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Calendar.DisabledImage));
routes.put('/enabled/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(Calendar.EnabledImage));