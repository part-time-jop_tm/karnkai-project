const express = require('express');
const asyncHandler = require('express-async-handler')
const passport = require('passport'); 
const TwentyTwo = require('../../controllers/2022.controller');
const routes = express.Router();     

module.exports = routes;  

routes.use(passport.authenticate('jwt', { session: false }));
routes.post('/insert', [passport.authenticate('jwt', { session: false })], asyncHandler(TwentyTwo.insertImage));  
routes.post('/', [passport.authenticate('jwt', { session: false })], asyncHandler(TwentyTwo.getAllImages));  
routes.post('/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(TwentyTwo.getImageID)); 
routes.post('/delete/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(TwentyTwo.DeleteImage)); 
routes.put('/update/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(TwentyTwo.UpdateImage));
routes.put('/disabled/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(TwentyTwo.DisabledImage));
routes.put('/enabled/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(TwentyTwo.EnabledImage));