const express = require('express');
const asyncHandler = require('express-async-handler')
const passport = require('passport'); 
const EventProfile = require('../../controllers/eventprofile.controller');
const routes = express.Router();     

module.exports = routes;  

routes.use(passport.authenticate('jwt', { session: false }));
routes.post('/insert', [passport.authenticate('jwt', { session: false })], asyncHandler(EventProfile.insertImage));  
routes.post('/', [passport.authenticate('jwt', { session: false })], asyncHandler(EventProfile.getAllImages));  