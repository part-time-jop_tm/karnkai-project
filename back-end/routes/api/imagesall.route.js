const express = require('express');
const asyncHandler = require('express-async-handler') 
const ImagesAll = require('../../controllers/imagesall.controller');
const routes = express.Router();     

module.exports = routes;   

routes.get('/download', asyncHandler(ImagesAll.getDownload));   
routes.get('/bag', asyncHandler(ImagesAll.getBag));   
routes.get('/canvas', asyncHandler(ImagesAll.getCanvas));   
routes.get('/photobook', asyncHandler(ImagesAll.getPhotoBook));   
routes.get('/calendar', asyncHandler(ImagesAll.getCalendar));   
routes.get('/twentyone', asyncHandler(ImagesAll.getTwentyOne));   
routes.get('/twentytwo', asyncHandler(ImagesAll.getTwentyTwo));   
routes.get('/workshop', asyncHandler(ImagesAll.getWorkShop));   
routes.get('/logotypedesign', asyncHandler(ImagesAll.getLogotypeDesign));   
routes.get('/adverse', asyncHandler(ImagesAll.getAdverse));   
routes.get('/product', asyncHandler(ImagesAll.getProduct));   