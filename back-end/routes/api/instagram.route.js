const express = require("express");
const asyncHandler = require("express-async-handler");
const passport = require("passport");
const instagram = require("../../controllers/instagram.controller");
const routes = express.Router();

module.exports = routes;

// routes.use(passport.authenticate('jwt', { session: false }));
routes.get("/picture", instagram.getPicture);
