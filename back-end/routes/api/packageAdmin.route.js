const express = require('express');
const asyncHandler = require('express-async-handler')
const passport = require('passport'); 
const package = require('../../controllers/packageAdmin.controller');
const routes = express.Router();     

module.exports = routes;  

routes.use(passport.authenticate('jwt', { session: false }));  
routes.post('/', [passport.authenticate('jwt', { session: false })], asyncHandler(package.getAllPackage));  
routes.post('/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(package.getPackage));  
routes.put('/update/:id', [passport.authenticate('jwt', { session: false })], asyncHandler(package.UpdatePackage)); 