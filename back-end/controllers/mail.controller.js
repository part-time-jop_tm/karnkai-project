const Response = require("../models/response");
const Mail = require("../models/mail.model");

exports.sendMail = function (req, res) {
  var request = new Response(req.body);
  try {
    if (
      !req.body.from ||
      !req.body.to ||
      !req.body.subject ||
      !req.body.title
    ) {
      res.status(400);
      Response.buildResponseEntity(
        null,
        request.requestId,
        request.datetime,
        req.method,
        req.originalUrl,
        400,
        false,
        "Request body Incomplete",
        req.body,
        function (err, data) {
          if (err) res.send(err);
          res.json(data);
        }
      );
    } else {
      Mail.sendMail(req.body, function (err, responseData) {
        if (err) {
          res.status(404);
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            404,
            false,
            null,
            err,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        } else {
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            200,
            true,
            "Send mail successfully",
            responseData,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        }
      });
    }
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      error,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};
