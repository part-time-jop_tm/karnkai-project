var Users = require("../models/user.model");
const Response = require("../models/response");

exports.loginUser = function (req, res) {
  var request = new Response(req.body);
  var BODY = new Users(req.body);
  try {
    if (!BODY.username || !BODY.password) {
      res.status(400);
      Response.buildResponseEntity(
        BODY.username,
        request.requestId,
        request.datetime,
        req.method,
        req.originalUrl,
        400,
        false,
        "Request body Incomplete",
        req.body,
        function (err, data) {
          if (err) res.send(err);
          res.json(data);
        }
      );
    } else {
      Users.loginUser(BODY, function (err, responseData) {
        if (err) {
          res.status(404);
          Response.buildResponseEntity(
            BODY.username,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            404,
            false,
            err.sqlMessage,
            err.sql,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        } else if (responseData === "lock") {
          Response.buildResponseEntity(
            BODY.username,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            404,
            false,
            "this userId is locked",
            null,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        } else {
          Response.buildResponseEntity(
            BODY.username,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            200,
            true,
            "Login successfully",
            responseData,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        }
      });
    }
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      BODY.username,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      error,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};
