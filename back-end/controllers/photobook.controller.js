const PhotoBook = require("../models/photobook.model");
const Response = require("../models/response");
var multer = require("multer");

const myStorage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "upload/photobook/");
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + file.originalname);
  },
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({
  storage: myStorage,
  limits: {
    fileSize: 1024 * 1024 * 5,
  },
  fileFilter: fileFilter,
}).single("picturefile");

exports.insertImage = function (req, res) {
  var request = new Response(req.body);
  try {
    // if (!req.body.userId || req.file.filename || req.body.description) {
    //   res.status(400);
    //   Response.buildResponseEntity(
    //     null,
    //     request.requestId,
    //     request.datetime,
    //     req.method,
    //     req.originalUrl,
    //     400,
    //     false,
    //     "Request body Incomplete",
    //     req.body,
    //     function (err, data) {
    //       if (err) res.send(err);
    //       res.json(data);
    //     }
    //   );
    // } else {
      upload(req, res, function (err) {
        if (err) {
          res.status(404);
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            404,
            false,
            "Error uploading file.",
            null,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        }
        PhotoBook.insertImage(
          req.file.filename,
          req.body.description,
          function (err, responseData) {
            if (err) {
              res.status(404);
              Response.buildResponseEntity(
                req.body.userId,
                request.requestId,
                request.datetime,
                req.method,
                req.originalUrl,
                404,
                false,
                err.sqlMessage,
                err.sql,
                function (err, data) {
                  if (err) res.send(err);
                  res.json(data);
                }
              );
            } else {
              Response.buildResponseEntity(
                req.body.userId,
                request.requestId,
                request.datetime,
                req.method,
                req.originalUrl,
                200,
                true,
                req.method + " data successfully",
                responseData,
                function (err, data) {
                  if (err) res.send(err);
                  res.json(data);
                }
              );
            }
          }
        );
      });
    // }
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      error,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.getAllImages = function (req, res) { 
  var request = new Response(req.body);
  try {
    if (!req.body.userId) {
      res.status(400);
      Response.buildResponseEntity(
        null,
        request.requestId,
        request.datetime,
        req.method,
        req.originalUrl,
        400,
        false,
        "Request body Incomplete",
        req.body,
        function (err, data) {
          if (err) res.send(err);
          res.json(data);
        }
      );
    } else {
      PhotoBook.getAllImages(function (err, responseData) {
        if (err) {
          res.status(404);
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            404,
            false,
            err.sqlMessage,
            err.sql,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        } else {
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            200,
            true,
            req.method + " data successfully",
            responseData,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        }
      });
    }
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      error,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.getImageID = function (req, res) { 
  var request = new Response(req.body);
  try {
    if (!req.body.userId) {
      res.status(400);
      Response.buildResponseEntity(
        null,
        request.requestId,
        request.datetime,
        req.method,
        req.originalUrl,
        400,
        false,
        "Request body Incomplete",
        req.body,
        function (err, data) {
          if (err) res.send(err);
          res.json(data);
        }
      );
    } else {
      PhotoBook.getImageID(req.params.id, function (err, responseData) {
        if (err) {
          res.status(404);
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            404,
            false,
            err.sqlMessage,
            err.sql,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        } else {
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            200,
            true,
            req.method + " data successfully",
            responseData,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        }
      });
    }
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      error,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.DeleteImage = function (req, res) { 
  var request = new Response(req.body);
  try {
    if (!req.params.id || !req.body.userId) {
      res.status(400);
      Response.buildResponseEntity(
        null,
        request.requestId,
        request.datetime,
        req.method,
        req.originalUrl,
        400,
        false,
        "Request body Incomplete",
        req.body,
        function (err, data) {
          if (err) res.send(err);
          res.json(data);
        }
      );
    } else {
      PhotoBook.DeleteImage(req.params.id, function (err, responseData) {
        if (err) {
          res.status(404);
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            404,
            false,
            err.sqlMessage,
            err.sql,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        } else {
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            200,
            true,
            "Delete data successfully",
            responseData,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        }
      });
    }
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      error,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.UpdateImage = function (req, res) { 
  var request = new Response(req.body);
  try {
    // if (
    //   !req.params.id ||
    //   !req.file.filename ||
    //   !req.body.description ||
    //   !req.body.userId
    // ) {
    //   res.status(400);
    //   Response.buildResponseEntity(
    //     null,
    //     request.requestId,
    //     request.datetime,
    //     req.method,
    //     req.originalUrl,
    //     400,
    //     false,
    //     "Request body Incomplete",
    //     req.body,
    //     function (err, data) {
    //       if (err) res.send(err);
    //       res.json(data);
    //     }
    //   );
    // } else {
      upload(req, res, function (err) {
        if (err) {
          res.status(404);
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            404,
            false,
            "Error uploading file.",
            null,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        }
        PhotoBook.UpdateImage(
          req.params.id,
          req.file.filename,
          req.body.description,
          function (err, responseData) {
            if (err) {
              res.status(404);
              Response.buildResponseEntity(
                req.body.userId,
                request.requestId,
                request.datetime,
                req.method,
                req.originalUrl,
                404,
                false,
                err.sqlMessage,
                err.sql,
                function (err, data) {
                  if (err) res.send(err);
                  res.json(data);
                }
              );
            } else {
              Response.buildResponseEntity(
                req.body.userId,
                request.requestId,
                request.datetime,
                req.method,
                req.originalUrl,
                200,
                true,
                req.method + " data successfully",
                responseData,
                function (err, data) {
                  if (err) res.send(err);
                  res.json(data);
                }
              );
            }
          }
        );
      });
    // }
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      error,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.DisabledImage = function (req, res) {
  var request = new Response(req.body);
  try {
    if (!req.params.id || !req.body.userId) {
      res.status(400);
      Response.buildResponseEntity(
        null,
        request.requestId,
        request.datetime,
        req.method,
        req.originalUrl,
        400,
        false,
        "Request body Incomplete",
        req.body,
        function (err, data) {
          if (err) res.send(err);
          res.json(data);
        }
      );
    } else {
      PhotoBook.DisabledImage(req.params.id, function (err, responseData) {
        if (err) {
          res.status(404);
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            404,
            false,
            err.sqlMessage,
            err.sql,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        } else {
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            200,
            true,
            "Disabled data successfully",
            responseData,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        }
      });
    }
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      error,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.EnabledImage = function (req, res) {
  var request = new Response(req.body);
  try {
    if (!req.params.id || !req.body.userId) {
      res.status(400);
      Response.buildResponseEntity(
        null,
        request.requestId,
        request.datetime,
        req.method,
        req.originalUrl,
        400,
        false,
        "Request body Incomplete",
        req.body,
        function (err, data) {
          if (err) res.send(err);
          res.json(data);
        }
      );
    } else {
      PhotoBook.EnabledImage(req.params.id, function (err, responseData) {
        if (err) {
          res.status(404);
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            404,
            false,
            err.sqlMessage,
            err.sql,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        } else {
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            200,
            true,
            "Enabled data successfully",
            responseData,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        }
      });
    }
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      error,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};
