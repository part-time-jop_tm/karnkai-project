var Users = require("../models/user.model");
const Response = require("../models/response");

exports.getUsers = function (req, res) {
  var request = new Response(req.body);
  try {
    if (!req.body.userId) {
      res.status(400);
      Response.buildResponseEntity(
        null,
        request.requestId,
        request.datetime,
        req.method,
        req.originalUrl,
        400,
        false,
        "Request body Incomplete",
        req.body,
        function (err, data) {
          if (err) res.send(err);
          res.json(data);
        }
      );
    } else {
      Users.getAllUsers(function (err, responseData) {
        if (err) {
          res.status(404);
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            404,
            false,
            err.sqlMessage,
            err.sql,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        } else {
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            200,
            true,
            req.method + " data successfully",
            responseData,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        }
      });
    }
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      error,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.getUserID = function (req, res) {
  var request = new Response(req.body);
  try {
    if (!req.params.id || !req.body.userId) {
      res.status(400);
      Response.buildResponseEntity(
        req.body.userId,
        request.requestId,
        request.datetime,
        req.method,
        req.originalUrl,
        400,
        false,
        "Request body Incomplete",
        null,
        function (err, data) {
          if (err) res.send(err);
          res.json(data);
        }
      );
    } else {
      Users.getUserID(req.params.id, function (err, responseData) {
        if (err) {
          res.status(404);
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            404,
            false,
            err.sqlMessage,
            err.sql,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        } else {
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            200,
            true,
            req.method + " user successfully",
            responseData,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        }
      });
    }
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      error,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.insertUser = function (req, res) {
  var request = new Response(req.body);
  var new_user = new Users(req.body);
  try {
    if (
      !new_user.email ||
      !new_user.username ||
      !new_user.password ||
      !new_user.role ||
      !new_user.status ||
      !req.body.userId
    ) {
      res.status(400);
      Response.buildResponseEntity(
        req.body.userId,
        request.requestId,
        request.datetime,
        req.method,
        req.originalUrl,
        400,
        false,
        "Request body Incomplete",
        req.body,
        function (err, data) {
          if (err) res.send(err);
          res.json(data);
        }
      );
    } else {
      Users.insertUser(new_user, function (err, responseData) {
        if (err) {
          res.status(404);
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            404,
            false,
            err.sqlMessage,
            err,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        } else {
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            200,
            true,
            "Register successfully",
            responseData,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        }
      });
    }
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      error,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

// exports.insertUserAccount = function (req, res) {
//   var new_user = new Users(req.body);

//   //handles null error
//   if (!new_user.username || !new_user.email) {
//     res
//       .status(400)
//       .send({ error: false, message: "Please provide users/status" });
//   } else {
//     Users.insertUserAccount(new_user, function (err, user) {
//       if (err) res.send(err);
//       res.json(user);
//     });
//   }
// };

exports.deleteUser = function (req, res) {
  var request = new Response(req.body);
  try {
    if (!req.params.id || !req.body.userId) {
      res.status(400);
      Response.buildResponseEntity(
        req.body.userId,
        request.requestId,
        request.datetime,
        req.method,
        req.originalUrl,
        400,
        false,
        "Request body Incomplete",
        req.body,
        function (err, data) {
          if (err) res.send(err);
          res.json(data);
        }
      );
    } else {
      Users.deleteUser(req.params.id, function (err, responseData) {
        if (err) {
          res.status(404);
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            404,
            false,
            err.sqlMessage,
            err.sql,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        } else {
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            200,
            true,
            "Delete data successfully",
            responseData,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        }
      });
    }
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      error,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

// exports.updateUser = function (req, res) {
//   var newuser = new Users(req.body);

//   Users.updateUser(req.params.id, newuser, function (err, user) {
//     if (err) res.send(err);
//     res.json(user);
//   });
// };

exports.lockUser = function (req, res) {
  var request = new Response(req.body);
  try {
    if (!req.params.id || !req.body.userId) {
      res.status(400);
      Response.buildResponseEntity(
        req.body.userId,
        request.requestId,
        request.datetime,
        req.method,
        req.originalUrl,
        400,
        false,
        "Request body Incomplete",
        null,
        function (err, data) {
          if (err) res.send(err);
          res.json(data);
        }
      );
    } else {
      Users.lockUser(req.params.id, function (err, responseData) {
        if (err) {
          res.status(404);
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            404,
            false,
            err.sqlMessage,
            err.sql,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        } else {
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            200,
            true,
            "Lock user successfully",
            responseData,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        }
      });
    }
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      error,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.activeUser = function (req, res) {
  var request = new Response(req.body);
  try {
    if (!req.params.id || !req.body.userId) {
      res.status(400);
      Response.buildResponseEntity(
        req.body.userId,
        request.requestId,
        request.datetime,
        req.method,
        req.originalUrl,
        400,
        false,
        "Request body Incomplete",
        null,
        function (err, data) {
          if (err) res.send(err);
          res.json(data);
        }
      );
    } else {
      Users.activeUser(req.params.id, function (err, responseData) {
        if (err) {
          res.status(404);
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            404,
            false,
            err.sqlMessage,
            err.sql,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        } else {
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            200,
            true,
            "Active user successfully",
            responseData,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        }
      });
    }
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      error,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.changePassword = function (req, res) {
  var request = new Response(req.body); 
  var new_user = new Users(req.body);
  try {
    if (!new_user.password || !req.params.id || !req.body.userId) {
      res.status(400);
      Response.buildResponseEntity(
        null,
        request.requestId,
        request.datetime,
        req.method,
        req.originalUrl,
        400,
        false,
        "Request body Incomplete",
        req.body,
        function (err, data) {
          if (err) res.send(err);
          res.json(data);
        }
      );
    } else {
      Users.changePassword(
        req.params.id,
        new_user.password,
        function (err, responseData) {
          if (err) {
            res.status(404);
            Response.buildResponseEntity(
              req.body.userId,
              request.requestId,
              request.datetime,
              req.method,
              req.originalUrl,
              404,
              false,
              err.sqlMessage,
              err.sql,
              function (err, data) {
                if (err) res.send(err);
                res.json(data);
              }
            );
          } else {
            Response.buildResponseEntity(
              req.body.userId,
              request.requestId,
              request.datetime,
              req.method,
              req.originalUrl,
              200,
              true,
              req.method + " data successfully",
              responseData,
              function (err, data) {
                if (err) res.send(err);
                res.json(data);
              }
            );
          }
        }
      );
    }
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      error,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.getLogs = function (req, res) {
  var request = new Response(req.body);
  try {
    if (!req.body.userId) {
      res.status(400);
      Response.buildResponseEntity(
        null,
        request.requestId,
        request.datetime,
        req.method,
        req.originalUrl,
        400,
        false,
        "Request body Incomplete",
        req.body,
        function (err, data) {
          if (err) res.send(err);
          res.json(data);
        }
      );
    } else {
      Users.getLogs(req.body, function (err, responseData) {
        if (err) {
          res.status(404);
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            404,
            false,
            err.sqlMessage,
            err.sql,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        } else {
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            200,
            true,
            req.method + " data successfully",
            responseData,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        }
      });
    }
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      error,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};
