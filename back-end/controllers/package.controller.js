var Package = require("../models/package.model");
const Response = require("../models/response"); 

exports.insertPackage = function (req, res) {
  var request = new Response(req.body);
  var customer = new Package(req.body);
  try {
    if (
      !customer.package ||
      !customer.customerName ||
      !customer.customerEmail ||
      !customer.customerTel ||
      !customer.customerText ||
      !customer.status 
    ) {
      res.status(400);
      Response.buildResponseEntity(
        req.body.userId,
        request.requestId,
        request.datetime,
        req.method,
        req.originalUrl,
        400,
        false,
        "Request body Incomplete",
        req.body,
        function (err, data) {
          if (err) res.send(err);
          res.json(data);
        }
      );
    } else {
        Package.insertPackage(
        customer,
        function (err, responseData) {
          if (err) {
            res.status(404);
            Response.buildResponseEntity(
              req.body.userId,
              request.requestId,
              request.datetime,
              req.method,
              req.originalUrl,
              404,
              false,
              err.sqlMessage,
              err,
              function (err, data) {
                if (err) res.send(err);
                res.json(data);
              }
            );
          } else {
            Response.buildResponseEntity(
              req.body.userId,
              request.requestId,
              request.datetime,
              req.method,
              req.originalUrl,
              200,
              true,
              "Register successfully",
              responseData,
              function (err, data) {
                if (err) res.send(err);
                res.json(data);
              }
            );
          }
        }
      );
    }
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      error,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};
 