const Response = require("../models/response");
const Instagram = require("../models/instagram.model"); 

exports.getPicture = function (req, res) {
  var request = new Response(req.body);
  try {
    Instagram.getPicture(function (err, responseData) {
      if (err) {
        res.status(404);
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          404,
          false,
          err.message,
          null,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      } else {
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          200,
          true,
          "Get picture from Instagram successfully",
          responseData,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        ); 
      }
    });
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      error,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};
