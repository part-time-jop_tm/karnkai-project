var PackageLogotypeDesign = require("../models/packagelogotypedesign.model");
const Response = require("../models/response");

exports.getAllPackage = function (req, res) {
  var request = new Response(req.body);
  try {
    if (!req.body.userId) {
      res.status(400);
      Response.buildResponseEntity(
        null,
        request.requestId,
        request.datetime,
        req.method,
        req.originalUrl,
        400,
        false,
        "Request body Incomplete",
        req.body,
        function (err, data) {
          if (err) res.send(err);
          res.json(data);
        }
      );
    } else {
      PackageLogotypeDesign.getAllPackage(function (err, responseData) {
        if (err) {
          res.status(404);
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            404,
            false,
            err.sqlMessage,
            err.sql,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        } else {
          Response.buildResponseEntity(
            req.body.userId,
            request.requestId,
            request.datetime,
            req.method,
            req.originalUrl,
            200,
            true,
            req.method + " data successfully",
            responseData,
            function (err, data) {
              if (err) res.send(err);
              res.json(data);
            }
          );
        }
      });
    }
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      error,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.getPackage = function (req, res) {
  var request = new Response(req.body);
  try {
    if (!req.params.id || !req.body.userId) {
      res.status(400);
      Response.buildResponseEntity(
        req.body.userId,
        request.requestId,
        request.datetime,
        req.method,
        req.originalUrl,
        400,
        false,
        "Request body Incomplete",
        null,
        function (err, data) {
          if (err) res.send(err);
          res.json(data);
        }
      );
    } else {
      PackageLogotypeDesign.getPackage(
        req.params.id,
        function (err, responseData) {
          if (err) {
            res.status(404);
            Response.buildResponseEntity(
              req.body.userId,
              request.requestId,
              request.datetime,
              req.method,
              req.originalUrl,
              404,
              false,
              err.sqlMessage,
              err.sql,
              function (err, data) {
                if (err) res.send(err);
                res.json(data);
              }
            );
          } else {
            Response.buildResponseEntity(
              req.body.userId,
              request.requestId,
              request.datetime,
              req.method,
              req.originalUrl,
              200,
              true,
              req.method + " data successfully",
              responseData,
              function (err, data) {
                if (err) res.send(err);
                res.json(data);
              }
            );
          }
        }
      );
    }
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      error,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
}; 

exports.UpdatePackage = function (req, res) {
  var request = new Response(req.body);
  try {
    if (!req.params.id || !req.body.userId) {
      res.status(400);
      Response.buildResponseEntity(
        req.body.userId,
        request.requestId,
        request.datetime,
        req.method,
        req.originalUrl,
        400,
        false,
        "Request body Incomplete",
        null,
        function (err, data) {
          if (err) res.send(err);
          res.json(data);
        }
      );
    } else {
      PackageLogotypeDesign.UpdatePackage(
        req.params.id,
        function (err, responseData) {
          if (err) {
            res.status(404);
            Response.buildResponseEntity(
              req.body.userId,
              request.requestId,
              request.datetime,
              req.method,
              req.originalUrl,
              404,
              false,
              err.sqlMessage,
              err.sql,
              function (err, data) {
                if (err) res.send(err);
                res.json(data);
              }
            );
          } else {
            Response.buildResponseEntity(
              req.body.userId,
              request.requestId,
              request.datetime,
              req.method,
              req.originalUrl,
              200,
              true,
              "accept customer successfully",
              responseData,
              function (err, data) {
                if (err) res.send(err);
                res.json(data);
              }
            );
          }
        }
      );
    }
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      error,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};
