const Images = require("../models/images.model");
const Response = require("../models/response");

exports.getDownload = function (req, res) {
  var request = new Response(req.body);
  try {
    Images.getDownload(function (err, responseData) {
      if (err) {
        res.status(404);
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          404,
          false,
          err.sqlMessage,
          err.sql,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      } else {
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          200,
          true,
          req.method + " data successfully",
          responseData,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      }
    });
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      null,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.getBag = function (req, res) {
  var request = new Response(req.body);
  try {
    Images.getBag(function (err, responseData) {
      if (err) {
        res.status(404);
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          404,
          false,
          err.sqlMessage,
          err.sql,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      } else {
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          200,
          true,
          req.method + " data successfully",
          responseData,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      }
    });
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      null,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.getCanvas = function (req, res) {
  var request = new Response(req.body);
  try {
    Images.getCanvas(function (err, responseData) {
      if (err) {
        res.status(404);
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          404,
          false,
          err.sqlMessage,
          err.sql,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      } else {
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          200,
          true,
          req.method + " data successfully",
          responseData,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      }
    });
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      null,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.getPhotoBook = function (req, res) {
  var request = new Response(req.body);
  try {
    Images.getPhotoBook(function (err, responseData) {
      if (err) {
        res.status(404);
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          404,
          false,
          err.sqlMessage,
          err.sql,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      } else {
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          200,
          true,
          req.method + " data successfully",
          responseData,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      }
    });
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      null,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.getCalendar = function (req, res) {
  var request = new Response(req.body);
  try {
    Images.getCalendar(function (err, responseData) {
      if (err) {
        res.status(404);
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          404,
          false,
          err.sqlMessage,
          err.sql,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      } else {
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          200,
          true,
          req.method + " data successfully",
          responseData,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      }
    });
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      null,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.getTwentyOne = function (req, res) {
  var request = new Response(req.body);
  try {
    Images.getTwentyOne(function (err, responseData) {
      if (err) {
        res.status(404);
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          404,
          false,
          err.sqlMessage,
          err.sql,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      } else {
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          200,
          true,
          req.method + " data successfully",
          responseData,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      }
    });
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      null,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.getTwentyTwo = function (req, res) {
  var request = new Response(req.body);
  try {
    Images.getTwentyTwo(function (err, responseData) {
      if (err) {
        res.status(404);
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          404,
          false,
          err.sqlMessage,
          err.sql,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      } else {
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          200,
          true,
          req.method + " data successfully",
          responseData,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      }
    });
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      null,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.getWorkShop = function (req, res) {
  var request = new Response(req.body);
  try {
    Images.getWorkShop(function (err, responseData) {
      if (err) {
        res.status(404);
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          404,
          false,
          err.sqlMessage,
          err.sql,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      } else {
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          200,
          true,
          req.method + " data successfully",
          responseData,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      }
    });
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      null,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.getLogotypeDesign = function (req, res) {
  var request = new Response(req.body);
  try {
    Images.getLogotypeDesign(function (err, responseData) {
      if (err) {
        res.status(404);
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          404,
          false,
          err.sqlMessage,
          err.sql,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      } else {
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          200,
          true,
          req.method + " data successfully",
          responseData,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      }
    });
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      null,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.getAdverse = function (req, res) {
  var request = new Response(req.body);
  try {
    Images.getAdverse(function (err, responseData) {
      if (err) {
        res.status(404);
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          404,
          false,
          err.sqlMessage,
          err.sql,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      } else {
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          200,
          true,
          req.method + " data successfully",
          responseData,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      }
    });
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      null,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.getEvent = function (req, res) {
  var request = new Response(req.body);
  try {
    Images.getEvent(function (err, responseData) {
      if (err) {
        res.status(404);
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          404,
          false,
          err.sqlMessage,
          err.sql,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      } else {
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          200,
          true,
          req.method + " data successfully",
          responseData,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      }
    });
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      null,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.getEventProfile = function (req, res) {
  var request = new Response(req.body);
  try {
    Images.getEventProfile(function (err, responseData) {
      if (err) {
        res.status(404);
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          404,
          false,
          err.sqlMessage,
          err.sql,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      } else {
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          200,
          true,
          req.method + " data successfully",
          responseData,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      }
    });
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      null,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.getWorkShopProfile = function (req, res) {
  var request = new Response(req.body);
  try {
    Images.getWorkShopProfile(function (err, responseData) {
      if (err) {
        res.status(404);
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          404,
          false,
          err.sqlMessage,
          err.sql,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      } else {
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          200,
          true,
          req.method + " data successfully",
          responseData,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      }
    });
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      null,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.getPreweddingProfile = function (req, res) {
  var request = new Response(req.body);
  try {
    Images.getPreweddingProfile(function (err, responseData) {
      if (err) {
        res.status(404);
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          404,
          false,
          err.sqlMessage,
          err.sql,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      } else {
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          200,
          true,
          req.method + " data successfully",
          responseData,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      }
    });
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      null,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

