const ImagesAll = require("../models/imagesall.model");
const Response = require("../models/response");

exports.getDownload = function (req, res) {
  var request = new Response(req.body);
  try {
    ImagesAll.getDownload(function (err, responseData) {
      if (err) {
        res.status(404);
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          404,
          false,
          err.sqlMessage,
          err.sql,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      } else {
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          200,
          true,
          req.method + " data successfully",
          responseData,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      }
    });
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      null,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.getBag = function (req, res) {
  var request = new Response(req.body);
  try {
    ImagesAll.getBag(function (err, responseData) {
      if (err) {
        res.status(404);
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          404,
          false,
          err.sqlMessage,
          err.sql,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      } else {
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          200,
          true,
          req.method + " data successfully",
          responseData,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      }
    });
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      null,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.getCanvas = function (req, res) {
  var request = new Response(req.body);
  try {
    ImagesAll.getCanvas(function (err, responseData) {
      if (err) {
        res.status(404);
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          404,
          false,
          err.sqlMessage,
          err.sql,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      } else {
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          200,
          true,
          req.method + " data successfully",
          responseData,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      }
    });
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      null,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.getPhotoBook = function (req, res) {
  var request = new Response(req.body);
  try {
    ImagesAll.getPhotoBook(function (err, responseData) {
      if (err) {
        res.status(404);
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          404,
          false,
          err.sqlMessage,
          err.sql,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      } else {
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          200,
          true,
          req.method + " data successfully",
          responseData,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      }
    });
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      null,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.getCalendar = function (req, res) {
  var request = new Response(req.body);
  try {
    ImagesAll.getCalendar(function (err, responseData) {
      if (err) {
        res.status(404);
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          404,
          false,
          err.sqlMessage,
          err.sql,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      } else {
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          200,
          true,
          req.method + " data successfully",
          responseData,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      }
    });
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      null,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.getTwentyOne = function (req, res) {
  var request = new Response(req.body);
  try {
    ImagesAll.getTwentyOne(function (err, responseData) {
      if (err) {
        res.status(404);
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          404,
          false,
          err.sqlMessage,
          err.sql,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      } else {
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          200,
          true,
          req.method + " data successfully",
          responseData,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      }
    });
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      null,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.getTwentyTwo = function (req, res) {
  var request = new Response(req.body);
  try {
    ImagesAll.getTwentyTwo(function (err, responseData) {
      if (err) {
        res.status(404);
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          404,
          false,
          err.sqlMessage,
          err.sql,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      } else {
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          200,
          true,
          req.method + " data successfully",
          responseData,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      }
    });
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      null,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.getWorkShop = function (req, res) {
  var request = new Response(req.body);
  try {
    ImagesAll.getWorkShop(function (err, responseData) {
      if (err) {
        res.status(404);
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          404,
          false,
          err.sqlMessage,
          err.sql,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      } else {
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          200,
          true,
          req.method + " data successfully",
          responseData,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      }
    });
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      null,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.getLogotypeDesign = function (req, res) {
  var request = new Response(req.body);
  try {
    ImagesAll.getLogotypeDesign(function (err, responseData) {
      if (err) {
        res.status(404);
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          404,
          false,
          err.sqlMessage,
          err.sql,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      } else {
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          200,
          true,
          req.method + " data successfully",
          responseData,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      }
    });
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      null,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.getAdverse = function (req, res) {
  var request = new Response(req.body);
  try {
    ImagesAll.getAdverse(function (err, responseData) {
      if (err) {
        res.status(404);
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          404,
          false,
          err.sqlMessage,
          err.sql,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      } else {
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          200,
          true,
          req.method + " data successfully",
          responseData,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      }
    });
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      null,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};

exports.getProduct = function (req, res) {
  var request = new Response(req.body);
  try {
    ImagesAll.getProduct(function (err, responseData) {
      if (err) {
        res.status(404);
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          404,
          false,
          err.sqlMessage,
          err.sql,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      } else {
        Response.buildResponseEntity(
          req.body.userId,
          request.requestId,
          request.datetime,
          req.method,
          req.originalUrl,
          200,
          true,
          req.method + " data successfully",
          responseData,
          function (err, data) {
            if (err) res.send(err);
            res.json(data);
          }
        );
      }
    });
  } catch (error) {
    res.status(403);
    Response.buildResponseEntity(
      req.body.userId,
      request.requestId,
      request.datetime,
      req.method,
      req.originalUrl,
      403,
      false,
      null,
      null,
      function (err, data) {
        if (err) res.send(err);
        res.json(data);
      }
    );
  }
};
