// config should be imported before importing any other file
const config = require('./config/config');
const app = require('./config/express'); 
require('./config/connectdb');

// module.parent check is required to support mocha watch
// src: https://github.com/mochajs/mocha/issues/1912
if (!module.parent) {
    app.listen(config.port, () => {
        console.info(`Server running at http://${ config.host }:${ config.port }...`);
    });
}

module.exports = app;