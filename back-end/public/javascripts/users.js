// user Id
var getId = localStorage.getItem("userId");

function unlock(id) {
  if (!confirm("Are you sure?")) return;
  $.ajax({
    type: "PUT",
    headers: { Authorization: "Bearer " + localStorage.getItem("token") },
    url: "/api/service/user/active/" + id,
    data: JSON.stringify({ userId: getId }),
    contentType: "application/JSON",
    success: function (data) {
      reload();
    },
    error: onRequestError,
  });
}
function lock(id) {
  if (!confirm("Are you sure?")) return;
  $.ajax({
    type: "PUT",
    headers: { Authorization: "Bearer " + localStorage.getItem("token") },
    url: "/api/service/user/lock/" + id,
    data: JSON.stringify({ userId: getId }),
    contentType: "application/JSON",
    success: function (data) {
      reload();
    },
    error: onRequestError,
  });
}

function init() {
  $("#users-table").append(`
  <tr><td colspan="4" class="text-center">
  <div class="spinner-border" role="status">
    <span class="sr-only">Loading...</span>
  </div></td></tr>`);
  $.ajax({
    type: "POST",
    url: "/api/service/user",
    headers: { Authorization: "Bearer " + localStorage.getItem("token") },
    data: JSON.stringify({ userId: getId }),
    contentType: "application/JSON",
    success: function (data) {
      $("#users-table").html("");
      if (data.data.users.length == 0)
        $("#kyc-table").append('<tr><td colspan="4">No Data</td></tr>');
      for (var index in data.data.users) {
        var user = data.data.users[index];
        $("#users-table").append(`
        <tr>
          <td>
           ${user.id}
          </td>
          <td>
           ${user.username}
          </td>
          <td>
           ${user.email}<br/>
          </td>
          <td class="text-primary">
            ${user.role}<br/>
          </td>
          <td>
          ${new Date(user.date).toGMTString()}<br/>
          </td>
          <td class="${
            user.status === "active" ? "text-success" : "text-danger"
          }">
            ${user.status}<br/>
          </td>
          <td>
            ${
              user.status === "active"
                ? `<button onclick="lock('${user.id}')" class="btn btn-sm btn-danger">Lock</button>`
                : `<button onclick="unlock('${user.id}')" class="btn btn-sm btn-outline-success">Unlock</button>
             `
            }
          </td>
        </tr>
        `);
      }
    },
    error: onRequestError,
  });
}

init();
