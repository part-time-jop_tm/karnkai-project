function login() {
  $.ajax({
    type: "POST",
    url: '/api/service/auth/login',
    data: {username:$('#username').val(),password:$('#password').val()},
    success: function(data) { 
      // console.log(data) 
      if (data.data.token) {
        localStorage.setItem('token', data.data.token);
        localStorage.setItem('userId', data.data.user.id);
        window.location.replace("/users");
      }
    },
    error: onRequestError,
  });
  return false;
}
function logout() {
  localStorage.removeItem('token');
  localStorage.removeItem('userId');
  window.location.replace("/");
}

function onRequestError() {
  logout()
}

function reload() {
  location.reload();
}
