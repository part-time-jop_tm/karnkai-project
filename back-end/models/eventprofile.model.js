var sql = require("../config/connectdb");

// Images object constructor
var EventProfile = function (img) {
  this.image = img.image; 
};

EventProfile.insertImage = function (filename, result) {
  sql.query(
    "insert into tbeventprofile (image) value (?);",
    [filename],
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { image: filename });
      }
    }
  );
};

EventProfile.getAllImages = function (result) {
  sql.query("select * from tbeventprofile order by id desc limit 1;", function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { images: res });
    }
  });
}; 

module.exports = EventProfile;
