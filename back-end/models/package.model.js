var sql = require("../config/connectdb");

//Task object constructor
var Package = function (item) {
  this.id = item.id;
  this.package = item.package;
  this.customerName = item.customerName;
  this.customerEmail = item.customerEmail;
  this.customerTel = item.customerTel;
  this.customerText = item.customerText;
  this.status = item.status;
};

Package.getAllPackage = function (result) {
  sql.query("select * from tbpackage order by status desc", function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { customers: res });
    }
  });
};

Package.getPackage = function (id, result) {
  sql.query("SELECT * FROM tbpackage WHERE id = ?;", id, function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { customer: res });
    }
  });
};

Package.insertPackage = function (customer, result) {
  try {
    sql.query(
      "INSERT INTO tbpackage (package, customerName, customerEmail, customerTel, customerText, status) VALUES (?,?,?,?,?,?)",
      [
        customer.package,
        customer.customerName,
        customer.customerEmail,
        customer.customerTel,
        customer.customerText,
        customer.status,
      ],
      function (err, res) {
        if (err) {
          result(err);
        } else {
          result(null, { customer: customer });
        }
      }
    );
  } catch (error) {
    result(error);
  }
};

Package.UpdatePackage = function (id, result) {
  sql.query(
    "UPDATE tbpackage set status = 'accept' where id = ?;",
    id,
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, "accept customer id: " + id);
      }
    }
  );
};

module.exports = Package;
