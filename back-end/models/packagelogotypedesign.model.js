var sql = require("../config/connectdb");

//Task object constructor
var PackageLogotypeDesign = function (item) {
  this.id = item.id;
  this.package = item.package;
  this.customerName = item.customerName;
  this.customerEmail = item.customerEmail;
  this.customerTel = item.customerTel;
  this.customerText = item.customerText;
  this.status = item.status;
};

PackageLogotypeDesign.getAllPackage = function (result) {
  sql.query("select * from tbpackageLogotypeDesign order by id desc", function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { customers: res });
    }
  });
};

PackageLogotypeDesign.getPackage = function (id, result) {
  sql.query("SELECT * FROM tbpackageLogotypeDesign WHERE id = ?;", id, function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { customer: res });
    }
  });
};

PackageLogotypeDesign.insertPackage = function (customer, result) {
  try {
    sql.query(
      "INSERT INTO tbpackageLogotypeDesign (package, customerName, customerEmail, customerTel, customerText, status) VALUES (?,?,?,?,?,?)",
      [
        customer.package,
        customer.customerName,
        customer.customerEmail,
        customer.customerTel,
        customer.customerText,
        customer.status,
      ],
      function (err, res) {
        if (err) {
          result(err);
        } else {
          result(null, { customer: customer });
        }
      }
    );
  } catch (error) {
    result(error);
  }
};

PackageLogotypeDesign.UpdatePackage = function (id, result) {
  sql.query(
    "UPDATE tbpackageLogotypeDesign set status = 'accept' where id = ?;",
    id,
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, "accept customer id: " + id);
      }
    }
  );
};

module.exports = PackageLogotypeDesign;
