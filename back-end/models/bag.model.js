var sql = require("../config/connectdb");

// Images object constructor
var Bag = function (img) {
  this.image = img.image;
  this.description = img.description;
};

Bag.insertImage = function (filename, description, result) {
  sql.query(
    "insert into tbbag (image, description) values (?,?)",
    [filename, description],
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { image: filename, description: description });
      }
    }
  );
};

Bag.getAllImages = function (result) {
  sql.query("select * from tbbag order by status desc", function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { images: res });
    }
  });
};

Bag.getImageID = function (id, result) {
  sql.query("select * from tbbag where id= ?", id, function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { image: res });
    }
  });
};

Bag.DeleteImage = function (id, result) {
  sql.query("delete from tbbag where id= ?", id, function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { imageId: id });
    }
  });
};

Bag.UpdateImage = function (id, filename, description, result) {
  sql.query(
    "update tbbag set image = ?, description = ? where id =" + id,
    [filename, description],
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, image: filename, description: description });
      } 
    }
  );
};

Bag.DisabledImage = function (id, result) {
  sql.query(
    "update tbbag SET status = 0 WHERE id =" + id, 
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, status: 'disabled' });
      } 
    }
  );
};

Bag.EnabledImage = function (id, result) {
  sql.query(
    "update tbbag SET status = 1 WHERE id =" + id, 
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, status: 'enabled' });
      } 
    }
  );
};

module.exports = Bag;
