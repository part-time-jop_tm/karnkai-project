var sql = require("../config/connectdb");

// Images object constructor
var Adverse = function (img) {
  this.image = img.image;
  this.description = img.description;
};

Adverse.insertImage = function (filename, description, result) {
  sql.query(
    "insert into tbadverse (image, description) values (?,?)",
    [filename, description],
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { image: filename, description: description });
      }
    }
  );
};

Adverse.getAllImages = function (result) {
  sql.query("select * from tbadverse order by status desc", function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { images: res });
    }
  });
};

Adverse.getImageID = function (id, result) {
  sql.query("select * from tbadverse where id= ?", id, function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { image: res });
    }
  });
};

Adverse.DeleteImage = function (id, result) {
  sql.query("delete from tbadverse where id= ?", id, function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { imageId: id });
    }
  });
};

Adverse.UpdateImage = function (id, filename, description, result) {
  sql.query(
    "update tbadverse set image = ?, description = ? where id =" + id,
    [filename, description],
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, image: filename, description: description });
      } 
    }
  );
};

Adverse.DisabledImage = function (id, result) {
  sql.query(
    "update tbadverse SET status = 0 WHERE id =" + id, 
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, status: 'disabled' });
      } 
    }
  );
};

Adverse.EnabledImage = function (id, result) {
  sql.query(
    "update tbadverse SET status = 1 WHERE id =" + id, 
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, status: 'enabled' });
      } 
    }
  );
};

module.exports = Adverse;
