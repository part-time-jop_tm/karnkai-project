var sql = require("../config/connectdb");

// Images object constructor
var Product = function (img) {
  this.image = img.image;
  this.description = img.description;
  this.productName = img.productName;
  this.productPrice = img.productPrice;
};

Product.insertImage = function (
  filename,
  description,
  productName,
  productPrice,
  result
) {
  sql.query(
    "insert into tbproduct (image, description, productName, productPrice) values (?,?,?,?)",
    [filename, description, productName, productPrice],
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, {
          image: filename,
          description: description,
          productName: productName,
          productPrice: productPrice,
        });
      }
    }
  );
};

Product.getAllImages = function (result) {
  sql.query(
    "select * from tbproduct order by status desc",
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { images: res });
      }
    }
  );
};

Product.getImageID = function (id, result) {
  sql.query("select * from tbproduct where id= ?", id, function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { image: res });
    }
  });
};

Product.DeleteImage = function (id, result) {
  sql.query("delete from tbproduct where id= ?", id, function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { imageId: id });
    }
  });
};

Product.UpdateImage = function (
  id,
  filename,
  description,
  productName,
  productPrice,
  result
) {
  sql.query(
    "update tbproduct set image = ?, description = ?, productName = ?, productPrice = ?, where id =" +
      id,
    [filename, description, productName, productPrice],
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, {
          imageId: id,
          image: filename,
          description: description,
          productName: productName,
          productPrice: productPrice,
        });
      }
    }
  );
};

Product.DisabledImage = function (id, result) {
  sql.query(
    "update tbproduct set status = 0 where id =" + id,
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, status: "disabled" });
      }
    }
  );
};

Product.EnabledImage = function (id, result) {
  sql.query(
    "update tbproduct set status = 1 where id =" + id,
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, status: "enabled" });
      }
    }
  );
};

module.exports = Product;
