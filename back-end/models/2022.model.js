var sql = require("../config/connectdb");

// Images object constructor
var TwentyTwo = function (img) {
  this.image = img.image;
  this.description = img.description;
};

TwentyTwo.insertImage = function (filename, description, result) {
  sql.query(
    "insert into tb2022 (image, description) values (?,?)",
    [filename, description],
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { image: filename, description: description });
      }
    }
  );
};

TwentyTwo.getAllImages = function (result) {
  sql.query("select * from tb2022 order by status desc", function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { images: res });
    }
  });
};

TwentyTwo.getImageID = function (id, result) {
  sql.query("select * from tb2022 where id= ?", id, function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { image: res });
    }
  });
};

TwentyTwo.DeleteImage = function (id, result) {
  sql.query("delete from tb2022 where id= ?", id, function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { imageId: id });
    }
  });
};

TwentyTwo.UpdateImage = function (id, filename, description, result) {
  sql.query(
    "update tb2022 set image = ?, description = ? where id =" + id,
    [filename, description],
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, image: filename, description: description });
      } 
    }
  );
};

TwentyTwo.DisabledImage = function (id, result) {
  sql.query(
    "update tb2022 set status = 0 where id =" + id, 
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, status: 'disabled' });
      } 
    }
  );
};

TwentyTwo.EnabledImage = function (id, result) {
  sql.query(
    "update tb2022 set status = 1 where id =" + id, 
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, status: 'enabled' });
      } 
    }
  );
};

module.exports = TwentyTwo;
