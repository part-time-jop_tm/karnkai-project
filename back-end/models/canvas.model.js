var sql = require("../config/connectdb");

// Images object constructor
var Canvas = function (img) {
  this.image = img.image;
  this.description = img.description;
};

Canvas.insertImage = function (filename, description, result) {
  sql.query(
    "insert into tbcanvas (image, description) values (?,?)",
    [filename, description],
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { image: filename, description: description });
      }
    }
  );
};

Canvas.getAllImages = function (result) {
  sql.query("select * from tbcanvas order by status desc", function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { images: res });
    }
  });
};

Canvas.getImageID = function (id, result) {
  sql.query("select * from tbcanvas where id= ?", id, function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { image: res });
    }
  });
};

Canvas.DeleteImage = function (id, result) {
  sql.query("delete from tbcanvas where id= ?", id, function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { imageId: id });
    }
  });
};

Canvas.UpdateImage = function (id, filename, description, result) {
  sql.query(
    "update tbcanvas set image = ?, description = ? where id =" + id,
    [filename, description],
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, image: filename, description: description });
      } 
    }
  );
};

Canvas.DisabledImage = function (id, result) {
  sql.query(
    "update tbcanvas set status = 0 where id =" + id, 
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, status: 'disabled' });
      } 
    }
  );
};

Canvas.EnabledImage = function (id, result) {
  sql.query(
    "update tbcanvas set status = 1 where id =" + id, 
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, status: 'enabled' });
      } 
    }
  );
};

module.exports = Canvas;
