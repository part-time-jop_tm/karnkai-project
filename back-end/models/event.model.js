var sql = require("../config/connectdb");

// Images object constructor
var Event = function (img) {
  this.image = img.image;
  this.description = img.description;
};

Event.insertImage = function (filename, description, result) {
  sql.query(
    "insert into tbevent (image, description) values (?,?)",
    [filename, description],
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { image: filename, description: description });
      }
    }
  );
};

Event.getAllImages = function (result) {
  sql.query("select * from tbevent order by status desc", function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { images: res });
    }
  });
};

Event.getImageID = function (id, result) {
  sql.query("select * from tbevent where id= ?", id, function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { image: res });
    }
  });
};

Event.DeleteImage = function (id, result) {
  sql.query("delete from tbevent where id= ?", id, function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { imageId: id });
    }
  });
};

Event.UpdateImage = function (id, filename, description, result) {
  sql.query(
    "update tbevent set image = ?, description = ? where id =" + id,
    [filename, description],
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, image: filename, description: description });
      } 
    }
  );
};

Event.DisabledImage = function (id, result) {
  sql.query(
    "update tbevent set status = 0 where id =" + id, 
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, status: 'disabled' });
      } 
    }
  );
};

Event.EnabledImage = function (id, result) {
  sql.query(
    "update tbevent set status = 1 where id =" + id, 
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, status: 'enabled' });
      } 
    }
  );
};

module.exports = Event;
