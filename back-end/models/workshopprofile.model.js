var sql = require("../config/connectdb");

// Images object constructor
var WorkShopProfile = function (img) {
  this.image = img.image;
  this.description = img.description;
};

WorkShopProfile.insertImage = function (filename, description, result) {
  sql.query(
    "insert into tbworkshopprofile (image, description) value (?,?);",
    [filename, description],
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { image: filename, description: description });
      }
    }
  );
};

WorkShopProfile.getAllImages = function (result) {
  sql.query(
    "select * from tbworkshopprofile order by id desc limit 1;",
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { images: res });
      }
    }
  );
};

module.exports = WorkShopProfile;
