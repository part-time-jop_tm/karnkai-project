var sql = require("../config/connectdb");

// Images object constructor
var Images = function (img) {
  this.image = img.image;
  this.description = img.description;
};

Images.getDownload = function (result) {
  sql.query(
    "select * from tbdownload where status = 1 order by id desc limit 9;",
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { images: res });
      }
    }
  );
};

Images.getBag = function (result) {
  sql.query(
    "select * from tbbag where status = 1 order by id desc limit 9;",
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { images: res });
      }
    }
  );
};

Images.getCanvas = function (result) {
  sql.query(
    "select * from tbcanvas where status = 1 order by id desc limit 9;",
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { images: res });
      }
    }
  );
};

Images.getPhotoBook = function (result) {
  sql.query(
    "select * from tbphotobook where status = 1 order by id desc limit 9;",
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { images: res });
      }
    }
  );
};

Images.getCalendar = function (result) {
  sql.query(
    "select * from tbcalendar where status = 1 order by id desc limit 9;",
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { images: res });
      }
    }
  );
};

Images.getTwentyOne = function (result) {
  sql.query(
    "select * from tb2021 where status = 1 order by id desc limit 9;",
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { images: res });
      }
    }
  );
};

Images.getTwentyTwo = function (result) {
  sql.query(
    "select * from tb2022 where status = 1 order by id desc limit 9;",
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { images: res });
      }
    }
  );
};

Images.getWorkShop = function (result) {
  sql.query(
    "select * from tbworkshop where status = 1 order by id desc limit 9;",
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { images: res });
      }
    }
  );
};

Images.getLogotypeDesign = function (result) {
  sql.query(
    "select * from tblogotypedesign where status = 1 order by id desc limit 9;",
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { images: res });
      }
    }
  );
};

Images.getAdverse = function (result) {
  sql.query(
    "select * from tbadverse where status = 1 order by id desc limit 9;",
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { images: res });
      }
    }
  );
};

Images.getEvent = function (result) {
  sql.query(
    "select * from tbevent where status = 1 order by id desc;",
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { images: res });
      }
    }
  );
};

Images.getEventProfile = function (result) {
  sql.query(
    "select * from tbeventprofile order by id desc limit 1;",
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { images: res });
      }
    }
  );
};

Images.getWorkShopProfile = function (result) {
  sql.query(
    "select * from tbworkshopprofile order by id desc limit 1;",
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { images: res });
      }
    }
  );
};

Images.getPreweddingProfile = function (result) {
  sql.query(
    "select * from tbpreweddingprofile order by id desc limit 1;",
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { images: res });
      }
    }
  );
};

module.exports = Images;
