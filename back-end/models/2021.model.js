var sql = require("../config/connectdb");

// Images object constructor
var TwentyOne = function (img) {
  this.image = img.image;
  this.description = img.description;
};

TwentyOne.insertImage = function (filename, description, result) {
  sql.query(
    "insert into tb2021 (image, description) values (?,?)",
    [filename, description],
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { image: filename, description: description });
      }
    }
  );
};

TwentyOne.getAllImages = function (result) {
  sql.query("select * from tb2021 order by status desc", function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { images: res });
    }
  });
};

TwentyOne.getImageID = function (id, result) {
  sql.query("select * from tb2021 where id= ?", id, function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { image: res });
    }
  });
};

TwentyOne.DeleteImage = function (id, result) {
  sql.query("delete from tb2021 where id= ?", id, function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { imageId: id });
    }
  });
};

TwentyOne.UpdateImage = function (id, filename, description, result) {
  sql.query(
    "update tb2021 set image = ?, description = ? where id =" + id,
    [filename, description],
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, image: filename, description: description });
      } 
    }
  );
};

TwentyOne.DisabledImage = function (id, result) {
  sql.query(
    "update tb2021 set status = 0 where id =" + id, 
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, status: 'disabled' });
      } 
    }
  );
};

TwentyOne.EnabledImage = function (id, result) {
  sql.query(
    "update tb2021 set status = 1 where id =" + id, 
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, status: 'enabled' });
      } 
    }
  );
};

module.exports = TwentyOne;
