var sql = require("../config/connectdb");

// Images object constructor
var WorkShop = function (img) {
  this.image = img.image;
  this.description = img.description;
};

WorkShop.insertImage = function (filename, description, result) {
  sql.query(
    "insert into tbworkshop (image, description) values (?,?)",
    [filename, description],
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { image: filename, description: description });
      }
    }
  );
};

WorkShop.getAllImages = function (result) {
  sql.query("select * from tbworkshop order by status desc", function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { images: res });
    }
  });
};

WorkShop.getImageID = function (id, result) {
  sql.query("select * from tbworkshop where id= ?", id, function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { image: res });
    }
  });
};

WorkShop.DeleteImage = function (id, result) {
  sql.query("delete from tbworkshop where id= ?", id, function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { imageId: id });
    }
  });
};

WorkShop.UpdateImage = function (id, filename, description, result) {
  sql.query(
    "update tbworkshop set image = ?, description = ? where id =" + id,
    [filename, description],
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, image: filename, description: description });
      } 
    }
  );
};

WorkShop.DisabledImage = function (id, result) {
  sql.query(
    "update tbworkshop set status = 0 where id =" + id, 
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, status: 'disabled' });
      } 
    }
  );
};

WorkShop.EnabledImage = function (id, result) {
  sql.query(
    "update tbworkshop set status = 1 where id =" + id, 
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, status: 'enabled' });
      } 
    }
  );
};

module.exports = WorkShop;
