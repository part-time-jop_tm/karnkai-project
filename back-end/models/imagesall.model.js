var sql = require("../config/connectdb");

// Images object constructor
var ImagesAll = function (img) {
  this.image = img.image;
  this.description = img.description;
};

ImagesAll.getDownload = function (result) {
  sql.query("select * from tbdownload where status = 1 order by id desc;", function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { images: res });
    }
  });
};

ImagesAll.getBag = function (result) {
  sql.query("select * from tbbag where status = 1 order by id desc;", function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { images: res });
    }
  });
};

ImagesAll.getCanvas = function (result) {
  sql.query("select * from tbcanvas where status = 1 order by id desc;", function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { images: res });
    }
  });
};

ImagesAll.getPhotoBook = function (result) {
  sql.query("select * from tbphotobook where status = 1 order by id desc;", function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { images: res });
    }
  });
};

ImagesAll.getCalendar = function (result) {
  sql.query("select * from tbcalendar where status = 1 order by id desc;", function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { images: res });
    }
  });
};

ImagesAll.getTwentyOne = function (result) {
  sql.query("select * from tb2021 where status = 1 order by id desc;", function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { images: res });
    }
  });
};

ImagesAll.getTwentyTwo = function (result) {
  sql.query("select * from tb2022 where status = 1 order by id desc;", function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { images: res });
    }
  });
};

ImagesAll.getWorkShop = function (result) {
  sql.query("select * from tbworkshop where status = 1 order by id desc;", function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { images: res });
    }
  });
};

ImagesAll.getLogotypeDesign = function (result) {
  sql.query("select * from tblogotypedesign where status = 1 order by id desc;", function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { images: res });
    }
  });
};

ImagesAll.getAdverse = function (result) {
  sql.query("select * from tbadverse where status = 1 order by id desc;", function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { images: res });
    }
  });
};

ImagesAll.getProduct = function (result) {
  sql.query("select * from tbproduct where status = 1 order by id desc;", function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { images: res });
    }
  });
};


module.exports = ImagesAll;
