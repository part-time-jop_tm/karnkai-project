var sql = require("../config/connectdb");

// Images object constructor
var LogotypeDesign = function (img) {
  this.image = img.image;
  this.description = img.description;
};

LogotypeDesign.insertImage = function (filename, description, result) {
  sql.query(
    "insert into tblogotypedesign (image, description) values (?,?)",
    [filename, description],
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { image: filename, description: description });
      }
    }
  );
};

LogotypeDesign.getAllImages = function (result) {
  sql.query("select * from tblogotypedesign order by status desc", function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { images: res });
    }
  });
};

LogotypeDesign.getImageID = function (id, result) {
  sql.query("select * from tblogotypedesign where id= ?", id, function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { image: res });
    }
  });
};

LogotypeDesign.DeleteImage = function (id, result) {
  sql.query("delete from tblogotypedesign where id= ?", id, function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { imageId: id });
    }
  });
};

LogotypeDesign.UpdateImage = function (id, filename, description, result) {
  sql.query(
    "update tblogotypedesign set image = ?, description = ? where id =" + id,
    [filename, description],
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, image: filename, description: description });
      } 
    }
  );
};

LogotypeDesign.DisabledImage = function (id, result) {
  sql.query(
    "update tblogotypedesign set status = 0 where id =" + id, 
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, status: 'disabled' });
      } 
    }
  );
};

LogotypeDesign.EnabledImage = function (id, result) {
  sql.query(
    "update tblogotypedesign set status = 1 where id =" + id, 
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, status: 'enabled' });
      } 
    }
  );
};

module.exports = LogotypeDesign;
