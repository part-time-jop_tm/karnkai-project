var sql = require("../config/connectdb");

// Images object constructor
var PreWeddingProfile = function (img) {
  this.image = img.image;
  this.description = img.description;
};

PreWeddingProfile.insertImage = function (filename, description, result) {
  sql.query(
    "insert into tbpreweddingprofile (image, description) value (?,?);",
    [filename, description],
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { image: filename, description: description });
      }
    }
  );
};

PreWeddingProfile.getAllImages = function (result) {
  sql.query(
    "select * from tbpreweddingprofile order by id desc limit 1;",
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { images: res });
      }
    }
  );
};

module.exports = PreWeddingProfile;
