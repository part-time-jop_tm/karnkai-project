var sql = require("../config/connectdb");

// Images object constructor
var Download = function (img) {
  this.image = img.image;
  this.description = img.description;
};

Download.insertImage = function (filename, description, result) {
  sql.query(
    "insert into tbdownload (image, description) values (?,?)",
    [filename, description],
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { image: filename, description: description });
      }
    }
  );
};

Download.getAllImages = function (result) {
  sql.query("select * from tbdownload order by status desc", function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { images: res });
    }
  });
};

Download.getImageID = function (id, result) {
  sql.query("select * from tbdownload where id= ?", id, function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { image: res });
    }
  });
};

Download.DeleteImage = function (id, result) {
  sql.query("delete from tbdownload where id= ?", id, function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { imageId: id });
    }
  });
};

Download.UpdateImage = function (id, filename, description, result) {
  sql.query(
    "update tbdownload set image = ?, description = ? where id =" + id,
    [filename, description],
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, image: filename, description: description });
      } 
    }
  );
};

Download.DisabledImage = function (id, result) {
  sql.query(
    "UPDATE tbdownload SET status = 0 WHERE id =" + id, 
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, status: 'disabled' });
      } 
    }
  );
};

Download.EnabledImage = function (id, result) {
  sql.query(
    "UPDATE tbdownload SET status = 1 WHERE id =" + id, 
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, status: 'enabled' });
      } 
    }
  );
};

module.exports = Download;
