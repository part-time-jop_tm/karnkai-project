var sql = require("../config/connectdb");
var bcrypt = require("bcrypt");
var jwt = require("jsonwebtoken");
const config = require("../config/config");
// const sqlQuery = require("../config/sqlQuery");
const nimberRount = 10;
const salt = bcrypt.genSaltSync(nimberRount);

//Task object constructor
var Users = function (users) {
  this.id = users.id;
  this.username = users.username;
  this.email = users.email;
  this.password = bcrypt.hashSync(users.password, salt);
  this.role = users.role;
  this.status = users.status;
};

Users.getAllUsers = function (result) {
  sql.query("select * from users order by id desc", function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { users: res });
    }
  });
};

Users.getUserID = function (id, result) {
  sql.query("SELECT * FROM users WHERE id = ?;", id, function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { user: res });
    }
  });
};

Users.insertUser = function (newUser, result) {
  try {
    sql.query(
      "INSERT INTO users (email,username,password,role,status) VALUES (?,?,?,?,?)",
      [
        newUser.email,
        newUser.username,
        newUser.password,
        newUser.role,
        newUser.status,
      ],
      function (err, res) {
        if (err) {
          result(err);
        } else {
          result(null, { user: newUser });
        }
      }
    );
  } catch (error) {
    result(error);
  }
};

Users.deleteUser = function (id, result) {
  sql.query("DELETE FROM users WHERE id = ?", id, function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { DeleteUserId: id });
    }
  });
};

// Users.updateUser = function (id, newUser, result) {
//   // sql.query("UPDATE users set ? where id= " + id, newUser, function (err, res) {
//   // sql.query("call pcd_userUpdate(?,?,?,?)", [id, newUser.email, newUser.username, newUser.password], function (err, res) {
//   sql.query(
//     sqlQuery.update_user,
//     [id, newUser.email, newUser.username, newUser.password],
//     function (err, res) {
//       if (err) {
//         result(null, err);
//       } else {
//         result(null, {
//           success: true,
//           message: "Update data successfully",
//           userID: id,
//           users: [newUser],
//         });
//       }
//     }
//   );
// };

Users.loginUser = function (data, result) {
  try {
    sql.query(
      "SELECT id, role, username, status FROM users WHERE username = '" +
        [data.username] +
        "';",
      function (err, res) {
        if (err) {
          result(err);
        } else {
          if (res[0].id > 0) {
            if (res[0].status === "lock") {
              result(null,{ user: res[0].status});
            } else {
              var token = jwt.sign({ data: res[0].id }, config.jwtsecret, {
                expiresIn: config.time,
              });
              result(null, { token: token, user: res[0] });
            }
          }
        }
      }
    );
  } catch (error) {
    result(error);
  }
};

Users.lockUser = function (id, result) {
  sql.query(
    "UPDATE users set status = 'lock' where id = ?;",
    id,
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, "lock userId: " + id);
      }
    }
  );
};

Users.activeUser = function (id, result) {
  sql.query(
    "UPDATE users set status = 'active' where id = ?;",
    id,
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, "active userId: " + id);
      }
    }
  );
};

Users.getLogs = function (reqDate, result) {
  sql.query(
    "SELECT * FROM `tblogs` WHERE DATE(datetime) between '" +
      reqDate.startDate +
      "' and '" +
      reqDate.toDate +
      "'; ",
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { logs: res });
      }
    }
  );
};

Users.changePassword = function (id, password, result) {
  sql.query(
    "UPDATE users SET password = ? WHERE id= " + id,
    [password],
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { changePasswordId: id });
      }
    }
  );
};

module.exports = Users;
