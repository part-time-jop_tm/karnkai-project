var nodemailer = require("nodemailer");
const config = require("../config/config");

//Task object constructor
var Mail = function (mail) {
  this.id = mail.id;
};

Mail.sendMail = function (body, result) {
  let transport = nodemailer.createTransport({
    service: "Gmail",
    auth: {
      user: config.gmail,
      pass: config.password_gmail,
    },
  });

  const message = {
    from: body.from,
    to: body.to,
    subject: body.subject,
    html: `<p><b>${body.title}</b> sent you now!</p>`,
  };

  transport.sendMail(message, function (error, info) {
    if (error) {
      result(error);
    } else {
      result(null, { gmail: info });
    }
  });
};

module.exports = Mail;
