var sql = require("../config/connectdb");

// Images object constructor
var Calendar = function (img) {
  this.image = img.image;
  this.description = img.description;
};

Calendar.insertImage = function (filename, description, result) {
  sql.query(
    "insert into tbcalendar (image, description) values (?,?)",
    [filename, description],
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { image: filename, description: description });
      }
    }
  );
};

Calendar.getAllImages = function (result) {
  sql.query("select * from tbcalendar order by status desc", function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { images: res });
    }
  });
};

Calendar.getImageID = function (id, result) {
  sql.query("select * from tbcalendar where id= ?", id, function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { image: res });
    }
  });
};

Calendar.DeleteImage = function (id, result) {
  sql.query("delete from tbcalendar where id= ?", id, function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { imageId: id });
    }
  });
};

Calendar.UpdateImage = function (id, filename, description, result) {
  sql.query(
    "update tbcalendar set image = ?, description = ? where id =" + id,
    [filename, description],
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, image: filename, description: description });
      } 
    }
  );
};

Calendar.DisabledImage = function (id, result) {
  sql.query(
    "update tbcalendar set status = 0 where id =" + id, 
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, status: 'disabled' });
      } 
    }
  );
};

Calendar.EnabledImage = function (id, result) {
  sql.query(
    "update tbcalendar set status = 1 where id =" + id, 
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, status: 'enabled' });
      } 
    }
  );
};

module.exports = Calendar;
