var sql = require("../config/connectdb");

// Images object constructor
var PhotoBook = function (img) {
  this.image = img.image;
  this.description = img.description;
};

PhotoBook.insertImage = function (filename, description, result) {
  sql.query(
    "insert into tbphotobook (image, description) values (?,?)",
    [filename, description],
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { image: filename, description: description });
      }
    }
  );
};

PhotoBook.getAllImages = function (result) {
  sql.query("select * from tbphotobook order by status desc", function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { images: res });
    }
  });
};

PhotoBook.getImageID = function (id, result) {
  sql.query("select * from tbphotobook where id= ?", id, function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { image: res });
    }
  });
};

PhotoBook.DeleteImage = function (id, result) {
  sql.query("delete from tbphotobook where id= ?", id, function (err, res) {
    if (err) {
      result(err);
    } else {
      result(null, { imageId: id });
    }
  });
};

PhotoBook.UpdateImage = function (id, filename, description, result) {
  sql.query(
    "update tbphotobook set image = ?, description = ? where id =" + id,
    [filename, description],
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, image: filename, description: description });
      } 
    }
  );
};

PhotoBook.DisabledImage = function (id, result) {
  sql.query(
    "update tbphotobook set status = 0 where id =" + id, 
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, status: 'disabled' });
      } 
    }
  );
};

PhotoBook.EnabledImage = function (id, result) {
  sql.query(
    "update tbphotobook set status = 1 where id =" + id, 
    function (err, res) {
      if (err) {
        result(err);
      } else {
        result(null, { imageId: id, status: 'enabled' });
      } 
    }
  );
};

module.exports = PhotoBook;
