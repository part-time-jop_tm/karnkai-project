var axios = require("axios").default;

//Task object constructor
var Instagram = function (ig) {
  this.id = ig.id;
};

Instagram.getPicture = function (result) { 
  var options = {
    method: "GET",
    url: "https://instagram28.p.rapidapi.com/medias",
    params: { user_id: "1782272103", batch_size: "9" },
    headers: {
      "x-rapidapi-host": "instagram28.p.rapidapi.com",
      "x-rapidapi-key": "b4e00b9c1dmshbb5444e448b5241p110ee4jsn659debe27177",
    },
  };
  axios
    .request(options)
    .then(function (response) {
      result(null, { images: response.data.data.user.edge_owner_to_timeline_media.edges });
    })
    .catch(function (error) {
      result(error);
    });
};

module.exports = Instagram;
