var createError = require("http-errors");
var bodyParser = require("body-parser");
var compress = require("compression");
var express = require("express");
var methodOverride = require("method-override");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var helmet = require("helmet");
var cors = require("cors");
var passport = require("../middleware/passport");
var config = require("./config");

var apiRouter = require("../routes/api/index");
var indexRouter = require("../routes/index");
var usersRouter = require("../routes/users");

var app = express();

if (config.env === "development") {
  app.use(logger("dev"));
}

// download file
app.use("/api/upload/download", express.static("upload/download")); 
app.use("/api/upload/bag", express.static("upload/bag")); 
app.use("/api/upload/canvas", express.static("upload/canvas")); 
app.use("/api/upload/photobook", express.static("upload/photobook")); 
app.use("/api/upload/calendar", express.static("upload/calendar")); 
app.use("/api/upload/2021", express.static("upload/2021")); 
app.use("/api/upload/2022", express.static("upload/2022")); 
app.use("/api/upload/workshop", express.static("upload/workshop")); 
app.use("/api/upload/logotypedesign", express.static("upload/logotypedesign"));  
app.use("/api/upload/adverse", express.static("upload/adverse"));  
app.use("/api/upload/event", express.static("upload/event"));  
app.use("/api/upload/eventprofile", express.static("upload/eventprofile"));  
app.use("/api/upload/product", express.static("upload/product"));  
app.use("/api/upload/workshopprofile", express.static("upload/workshopprofile"));  
app.use("/api/upload/preweddingprofile", express.static("upload/preweddingprofile"));  

// view engine setup
// app.set("views", path.join(__dirname, "../views"));
// app.set("view engine", "pug");

app.use(logger("dev"));
app.use(express.json());
app.enable("trust proxy");
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(bodyParser.json({ limit: "50mb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));

app.use(cookieParser());
app.use(compress());
app.use(methodOverride());

// secure apps by setting various HTTP headers
app.use(helmet());

// enable CORS - Cross Origin Resource Sharing
app.use(cors());
app.use(bodyParser.json());
app.use(passport.initialize());

app.use("/karnkai/api/service", apiRouter);
app.use("/", indexRouter);
app.use("/users", usersRouter);

app.use(
  "/javascripts",
  express.static(path.join(__dirname, "../node_modules/jquery/dist"))
);
app.use(
  "/javascripts",
  express.static(path.join(__dirname, "../node_modules/bootstrap/dist/js"))
);
app.use(
  "/stylesheets",
  express.static(path.join(__dirname, "../node_modules/bootstrap/dist/css"))
);
// app.use("/", express.static(path.join(__dirname, "../public")));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // customize Joi validation errors
  if (err.isJoi) {
    err.message = err.details.map((e) => e.message).join("; ");
    err.status = 400;
  }

  res.status(err.status || 500).json({
    message: err.message,
  });
  next(err);
});

module.exports = app;
