const dotenv = require("dotenv");

// read in the .env file
dotenv.config();

// capture the environment variables the application needs

const {
  //FOR USER
  GETUSER,
  GETUSERID,
  INSERTUSER,
  UPDATEUSER,
  DELETEUSER,
  LOGIN,
  //FOR IMAGE
  GETIMAGE,
  GETIMAGEID,
  INSERTIMAGE,
  UPDATEIMAGE,
  DELETEIMAGE,
  //FOR CUSTOMER
  GETCUSTOMER,
  GETCUSTOMERID,
  INSERTCUSTOMER,
  UPDATECUSTOMER,
  DELETECUSTOMER,
} = process.env;

module.exports = {
  //FOR USER
  get_user: GETUSER,
  get_userID: GETUSERID,
  insert_user: INSERTUSER,
  update_user: UPDATEUSER,
  delete_user: DELETEUSER,
  login: LOGIN,
  //FOR IMAGE
  get_image: GETIMAGE,
  get_imageID: GETIMAGEID,
  insert_image: INSERTIMAGE,
  update_image: UPDATEIMAGE,
  delete_image: DELETEIMAGE,
  //FOR CUSTOMER
  get_customer: GETCUSTOMER,
  get_customerID: GETCUSTOMERID,
  insert_customer: INSERTCUSTOMER,
  update_customer: UPDATECUSTOMER,
  delete_customer: DELETECUSTOMER
};
